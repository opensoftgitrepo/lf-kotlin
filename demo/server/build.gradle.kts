plugins {
    id("java")
    kotlin("jvm")
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    id("war")
}

java.sourceCompatibility = JavaVersion.VERSION_11

dependencies {
    implementation(project(":lf-kotlin"))
    implementation(project(":demo:common"))

    implementation("org.springframework.boot:spring-boot-starter-web")
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    providedRuntime("org.springframework.boot:spring-boot-starter-tomcat")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    implementation(
        "com.fasterxml.jackson.module:jackson-module-kotlin:${property("jacksonModuleKotlinVersion")}"
    )
}

tasks.test { useJUnitPlatform() }

/** Build the client and copy the build output into the server as static resources. */
val copyClientResources by
    tasks.registering(Copy::class) {
        from(project(":demo:client").tasks.named("npmRunBuild"))
        into("$buildDir/resources/main/static")
    }

tasks.processResources { dependsOn(copyClientResources) }

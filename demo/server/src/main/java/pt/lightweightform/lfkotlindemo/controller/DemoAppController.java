package pt.lightweightform.lfkotlindemo.controller;

import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import pt.lightweightform.lfkotlin.Issue;
import pt.lightweightform.lfkotlin.IssuesMaps;
import pt.lightweightform.lfkotlindemo.DemoApp;
import pt.lightweightform.lfkotlindemo.service.DemoAppValidator;

/** Controller to provide the demo app. */
@Controller
public class DemoAppController {
    final Logger logger = LoggerFactory.getLogger(DemoAppController.class);
    final DemoAppValidator demoAppValidator;

    @Autowired
    public DemoAppController(DemoAppValidator demoAppValidator) {
        this.demoAppValidator = demoAppValidator;
    }

    @GetMapping(value = {"/", "/{:[^.]+}/**"})
    public String index() {
        return "forward:index.html";
    }

    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> submitDemoApp(
            HttpServletRequest request, @RequestBody DemoApp demoApp) {
        Map<String, List<Issue>> issuesMap =
                demoAppValidator.validate(demoApp, request.getRemoteAddr());

        if (IssuesMaps.hasNoErrors(issuesMap)) {
            logger.info("Accepted form: {}", demoApp);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } else {
            logger.info("Rejected form: {}, due to issues: {}", demoApp, issuesMap);
            return new ResponseEntity<>(issuesMap, HttpStatus.BAD_REQUEST);
        }
    }
}

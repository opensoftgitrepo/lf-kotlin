package pt.lightweightform.lfkotlindemo.service;

import static pt.lightweightform.lfkotlindemo.DemoAppKt.demoAppSchema;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import pt.lightweightform.lfkotlin.*;
import pt.lightweightform.lfkotlindemo.DemoApp;

@Service
public class DemoAppValidator {
    Validator<DemoApp> demoAppValidator =
            new Validator<>(
                    demoAppSchema,
                    Map.of(
                            "/demoForm/stringField", new StringCannotBeYyyWhenLongGt9999(),
                            "/demoForm/ipAddress", new IpAddressMatchesRequestIp(),
                            "/demoForm/tableField/?/stringCell",
                                    new TableRowStringCellCannotBeZzz()));

    /** Validate the demo app value using the IP address of the request as extra context. */
    public Map<String, List<Issue>> validate(DemoApp demoApp, String ipAddress) {
        return demoAppValidator.validationIssues(demoApp, ipAddress);
    }

    /**
     * Validates that the string isn't `yyy` (case doesn't matter) when the long field is greater
     * than `9999`.
     */
    private static class StringCannotBeYyyWhenLongGt9999 implements SyncValidation<String> {
        @NotNull
        @Override
        public Iterable<Issue> validate(@NotNull Context ctx, String value) {
            List<Issue> issues = new LinkedList<>();
            long longField = ctx.get("../longField");
            if (value.equalsIgnoreCase("yyy") && longField > 9999) {
                issues.add(new Issue("CANNOT_BE_YYY"));
            }
            return issues;
        }
    }

    /**
     * Validates that `ipAddress` matches the request's IP address, accessible via the extra context
     * of the validation.
     */
    private static class IpAddressMatchesRequestIp implements SyncValidation<String> {
        @NotNull
        @Override
        public Iterable<Issue> validate(@NotNull Context ctx, String value) {
            List<Issue> issues = new LinkedList<>();
            String ipAddress = (String) ctx.getExternalContext();
            if (!value.equals(ipAddress)) {
                issues.add(
                        new Issue(
                                "IP_ADDRESS_DOES_NOT_MATCH",
                                false,
                                JsonObjects.objectOf(
                                        new Pair<>("expected", ctx.getExternalContext()))));
            }
            return issues;
        }
    }

    /** Validates that the string cell of a table row isn't `zzz` (case doesn't matter). */
    private static class TableRowStringCellCannotBeZzz implements SyncValidation<String> {
        @NotNull
        @Override
        public Iterable<Issue> validate(@NotNull Context ctx, String value) {
            List<Issue> issues = new LinkedList<>();
            if (value.equalsIgnoreCase("zzz")) {
                issues.add(new Issue("CANNOT_BE_ZZZ"));
            }
            return issues;
        }
    }
}

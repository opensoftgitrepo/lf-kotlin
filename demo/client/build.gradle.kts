import com.github.gradle.node.npm.task.NpmTask

plugins {
    id("com.github.node-gradle.node")
    base
}

tasks.npmInstall { dependsOn(":lf-kotlin:assemble", ":demo:common:assemble") }

val npmStart by
    tasks.registering(NpmTask::class) {
        dependsOn(tasks.npmInstall)
        npmCommand.set(listOf("start"))
    }

val npmRunBuild by
    tasks.registering(NpmTask::class) {
        dependsOn(tasks.npmInstall)
        if (properties["prod"] == "true") {
            npmCommand.set(listOf("run", "build", "--", "--prod"))
        } else {
            npmCommand.set(listOf("run", "build"))
        }

        inputs.dir("src")
        inputs.dir("node_modules")
        inputs.file("angular.json")
        inputs.file("package.json")
        inputs.file(".browserslistrc")
        inputs.file("tsconfig.json")
        inputs.file("tsconfig.app.json")
        outputs.dir("dist")
    }

val npmTest by
    tasks.registering(NpmTask::class) {
        dependsOn(tasks.npmInstall)
        npmCommand.set(listOf("test", "--", "--watch", "false", "--browsers", "ChromeHeadless"))
    }

val npmRunLint by
    tasks.registering(NpmTask::class) {
        dependsOn(tasks.npmInstall)
        npmCommand.set(listOf("run", "lint"))
    }

tasks.assemble { dependsOn(npmRunBuild) }

tasks.check { dependsOn(npmRunLint /*, npmTest*/) }

import { Component, ViewChild, OnInit, isDevMode } from '@angular/core';
import {
  ActionsMenuAction,
  AppComponent as LfAppComponent,
} from '@lightweightform/bootstrap-theme';
import { LfStorage, LfFileStorage, LfUnloadAlert } from '@lightweightform/core';
import { computed, observable } from 'mobx';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  actions: ActionsMenuAction[] = [
    {
      id: 'save',
      style: 'outline-secondary',
      icon: 'save',
      callback: () => this.save(),
    },
    {
      id: 'load',
      style: 'outline-secondary',
      icon: 'folder-open',
      isDisabled: () => !this.lfFileStorage.loadIsSupported,
      callback: () => this.load(),
    },
    {
      id: 'validate',
      style: 'outline-danger',
      icon: 'check-square-o',
      callback: () => this.lfApp.validate(),
    },
    {
      id: 'submit',
      style: 'outline-success',
      icon: 'send',
      callback: () => this.submit(),
    },
  ];

  @ViewChild(LfAppComponent) lfApp: LfAppComponent;
  @observable sending: boolean;

  constructor(
    public lfStorage: LfStorage,
    public lfFileStorage: LfFileStorage,
    public lfUnloadAlert: LfUnloadAlert
  ) {}

  ngOnInit(): void {
    if (!isDevMode()) {
      this.lfUnloadAlert.enable();
    }
  }

  async save(): Promise<void> {
    try {
      const dateStr = new Date().toISOString().replace(/[T:.]/g, '-');
      const fileName = `lf-kotlin-demo-app-${dateStr}`;
      await this.lfFileStorage.saveToFile('/', fileName);
      this.lfStorage.setPristine();
    } catch (err) {
      console.error('Error saving file:', err);
    }
  }

  async load(): Promise<void> {
    try {
      if (await this.lfFileStorage.loadFromFile('/')) {
        this.lfStorage.setTouched('/', true);
      }
    } catch (err) {
      console.error('Error loading file:', err);
    }
  }

  // Allow submission of invalid values to test server response
  async submit(): Promise<void> {
    const appJSON = JSON.stringify(this.lfStorage.getAsJS());
    this.sending = true;
    const res = await fetch('/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: appJSON,
    });
    if (res.ok) {
      alert('Value submitted successfully.');
    } else if (res.status === 400) {
      const issues = await res.json();
      console.error('Form contains errors:', issues);
      alert(`Form contains errors: ${JSON.stringify(issues, null, 2)}`);
    } else {
      alert(`Error submitting form: ${res.status} (${res.statusText})`);
    }
    this.sending = false;
  }
}

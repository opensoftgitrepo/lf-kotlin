import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LfRouter, LF_ROUTER_BASE_PATH, LfRoutes } from '@lightweightform/core';

import { DemoFormComponent } from './demo-form/demo-form.component';

const routes: LfRoutes = [
  { path: '', redirectTo: 'demoForm', pathMatch: 'full' },
  { path: 'demoForm', component: DemoFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [LfRouter, { provide: LF_ROUTER_BASE_PATH, useValue: '/' }],
})
export class AppRoutingModule {}

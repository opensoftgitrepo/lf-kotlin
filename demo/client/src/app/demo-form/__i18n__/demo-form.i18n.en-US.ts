/**
 * Demo form i18n object for the en-US locale.
 */
export const demoFormI18nEnUS: Record<string, any> = {
  '/demoForm': {
    label: 'Demo form',
  },
  '/demoForm/ipAddress': {
    label: 'IP address',
    code: 'Ip',
    helpMessage:
      'A server-side validation will check that this IP address matches the IP address that made the request.',
  },
  '/demoForm/booleanField': {
    label: 'Boolean field',
    code: 'Bo',
    validations: {
      CANNOT_BE_FALSE: 'Boolean field must be ticked.',
    },
  },
  '/demoForm/arrayEnumStringField': {
    label: 'Array of enum strings field',
    code: 'Ar',
    options: [
      { value: 'ValA', label: 'ValA' },
      { value: 'ValB', label: 'ValB' },
      { value: 'ValC', label: 'ValC' },
    ],
  },
  '/demoForm/dateField': {
    label: 'Date field',
    code: 'Da',
    validations: {
      CANNOT_BE_A_SUNDAY: 'Date must not be a Sunday.',
    },
  },
  '/demoForm/dateRangeField': {
    label: 'Date-range field',
    code: 'Dr',
    validations: {
      CANNOT_BE_MORE_THAN_2_WEEKS: 'Date-range cannot be larger than 2 weeks.',
    },
  },
  '/demoForm/byteField': {
    label: 'Byte field',
    code: 'By',
    validations: {
      CANNOT_BE_POWER_OF_2: 'Number cannot be a power of 2.',
    },
  },
  '/demoForm/shortField': {
    label: 'Short field',
    code: 'Sh',
  },
  '/demoForm/intField': {
    label: 'Int field',
    code: 'In',
  },
  '/demoForm/longField': {
    label: 'Long field',
    code: 'Lo',
  },
  '/demoForm/floatField': {
    label: 'Float field',
    code: 'Fl',
    validations: {
      MUST_BE_GREATER_THAN_LONG:
        'Number must be greater than the number in [long field](lf://../longField).',
    },
  },
  '/demoForm/doubleField': {
    label: 'Double field',
    code: 'Do',
    validations: {
      CANNOT_BE_66: 'Number cannot be 66.',
    },
  },
  '/demoForm/sumField': {
    label: 'Sum field',
    code: 'Su',
  },
  '/demoForm/enumNameField': {
    label: 'Enum name field',
    code: 'En',
    options: [
      { value: 'ValA', label: 'ValA' },
      { value: 'ValB', label: 'ValB' },
      { value: 'ValC', label: 'ValC' },
    ],
    validations: {
      CANNOT_BE_VAL_B_WHEN_DOUBLE_BETWEEN_10_AND_80:
        'Field cannot be `ValB` when the [double field](lf://../doubleField) is between 10 and 80.',
    },
  },
  '/demoForm/enumOrdinalField': {
    label: 'Enum ordinal field',
    code: 'Eo',
    options: [
      { value: null, label: '' },
      { value: 0, label: 'ValA' },
      { value: 1, label: 'ValB' },
      { value: 2, label: 'ValC' },
    ],
    validations: {
      CANNOT_BE_VAL_A_WHEN_DOUBLE_BETWEEN_30_AND_70:
        'Field cannot be `ValA` when the [double field](lf://../doubleField) is between 30 and 70.',
    },
  },
  '/demoForm/stringField': {
    label: 'String field',
    code: 'St',
    validations: {
      CANNOT_START_WITH_X_WHEN_BOOLEAN_IS_TRUE:
        'Text must not start with "x" or "X" when the [boolean field](lf://../booleanField) is ticked.',
      CANNOT_BE_YYY:
        'Text cannot be "YYY" (case-insensitive) when the [long field](lf://../longField) is over 9999.',
    },
  },
  '/demoForm/emailField': {
    label: 'Email field',
    code: 'Em',
    validations: {
      LF_INVALID_EMAIL: 'Email is invalid.',
    },
  },
  '/demoForm/tableField': {
    label: 'Table field',
    code: 'Ta',
    validations: {
      LF_REPEATED_ELEMENTS: (_, { indices: [index1, index2] }) =>
        `Rows cannot contain the same text in the string cell. Conflicting rows: ${
          index1 + 1
        } and ${index2 + 1}.`,
    },
    columnLabels: {
      index: '#',
      stringCell: 'String cell',
      doubleCell: 'Double cell',
    },
  },
  '/demoForm/tableField/?/stringCell': {
    label: 'String cell',
    validations: {
      CANNOT_BE_ZZZ: 'Test cannot be "ZZZ" (case-insensitive).',
    },
  },
  '/demoForm/tableSumField': {
    label: 'Table sum field',
    code: 'Ts',
  },
  '/demoForm/computedArrayField': {
    label: 'Computed array field',
    code: 'Af',
    columnLabels: {
      index: '#',
      stringCell: 'String cell',
      doubleCell: 'Double cell',
    },
  },
};

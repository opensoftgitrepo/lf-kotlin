import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LfBootstrapThemeModule } from '@lightweightform/bootstrap-theme';
import {
  LfStorage,
  LfFileStorage,
  LfI18n,
  LfUnloadAlert,
  LfSerializer,
  LfJsonSerializer,
  LF_APP_SCHEMA,
  LF_APP_I18N,
} from '@lightweightform/core';
import { AppComponent } from './app.component';
import { appSchema } from './app.schema';
import { appI18n } from './app.i18n';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, LfBootstrapThemeModule],
      declarations: [AppComponent],
      providers: [
        LfStorage,
        LfFileStorage,
        LfI18n,
        LfUnloadAlert,
        { provide: LfSerializer, useClass: LfJsonSerializer },
        { provide: LF_APP_SCHEMA, useValue: appSchema },
        { provide: LF_APP_I18N, useValue: appI18n },
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});

import lfKotlinDemoCommon from '@lightweightform-demo/lf-kotlin-demo-common';

/**
 * Application schema.
 */
export const appSchema =
  lfKotlinDemoCommon.pt.lightweightform.lfkotlindemo.demoAppSchema;

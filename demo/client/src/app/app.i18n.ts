import { I18N_EN_US } from '@lightweightform/bootstrap-theme';
import { LfI18n } from '@lightweightform/core';

import { appI18nEnUS } from './__i18n__/app.i18n.en-US';
import { demoFormI18nEnUS } from './demo-form/__i18n__/demo-form.i18n.en-US';

/**
 * App i18n object.
 */
export const appI18n: Record<string, any> = {
  'en-US': LfI18n.mergeTranslations(I18N_EN_US, appI18nEnUS, demoFormI18nEnUS),
};

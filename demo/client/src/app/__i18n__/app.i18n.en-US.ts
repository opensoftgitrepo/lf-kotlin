/**
 * App i18n object for the en-US locale.
 */
export const appI18nEnUS: Record<string, any> = {
  '*': {
    actionsMenu: {
      save: 'Save',
      load: 'Load',
      validate: 'Validate',
      submit: 'Submit',
    },
  },
  '/': {
    label: 'LF-Kotlin Demo',
  },
};

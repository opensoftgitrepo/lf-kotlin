@file:OptIn(ExperimentalJsExport::class)
@file:Suppress("NON_EXPORTABLE_TYPE")

package pt.lightweightform.lfkotlindemo

import kotlin.experimental.and
import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport
import kotlin.jvm.JvmField
import kotlinx.datetime.Clock
import kotlinx.datetime.DayOfWeek
import kotlinx.datetime.Instant
import kotlinx.datetime.TimeZone
import kotlinx.datetime.atStartOfDayIn
import kotlinx.datetime.daysUntil
import kotlinx.datetime.toLocalDateTime
import kotlinx.datetime.todayAt
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.Context
import pt.lightweightform.lfkotlin.Issue
import pt.lightweightform.lfkotlin.LfDate
import pt.lightweightform.lfkotlin.LfDateRange
import pt.lightweightform.lfkotlin.LfLong
import pt.lightweightform.lfkotlin.SyncBound
import pt.lightweightform.lfkotlin.SyncIsRequired
import pt.lightweightform.lfkotlin.SyncValidation
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.computedvalues.SumOfDoubles
import pt.lightweightform.lfkotlin.schemas.arraySchema
import pt.lightweightform.lfkotlin.schemas.booleanSchema
import pt.lightweightform.lfkotlin.schemas.classSchema
import pt.lightweightform.lfkotlin.schemas.emailSchema
import pt.lightweightform.lfkotlin.schemas.enumNameSchema
import pt.lightweightform.lfkotlin.schemas.nullableByteSchema
import pt.lightweightform.lfkotlin.schemas.nullableDateRangeSchema
import pt.lightweightform.lfkotlin.schemas.nullableDateSchema
import pt.lightweightform.lfkotlin.schemas.nullableDoubleSchema
import pt.lightweightform.lfkotlin.schemas.nullableEnumOrdinalSchema
import pt.lightweightform.lfkotlin.schemas.nullableFloatSchema
import pt.lightweightform.lfkotlin.schemas.nullableIntSchema
import pt.lightweightform.lfkotlin.schemas.nullableLongSchema
import pt.lightweightform.lfkotlin.schemas.nullableShortSchema
import pt.lightweightform.lfkotlin.schemas.stringSchema
import pt.lightweightform.lfkotlin.schemas.tableSchema
import pt.lightweightform.lfkotlin.toEpochMilliseconds
import pt.lightweightform.lfkotlin.toLfDate
import pt.lightweightform.lfkotlin.toLfLong
import pt.lightweightform.lfkotlin.toLong
import pt.lightweightform.lfkotlin.validations.Unique
import pt.lightweightform.lfkotlin.validations.UniqueBy

// Data ============================================================================================

data class DemoApp(var demoForm: DemoForm)

data class DemoForm(
    var ipAddress: String,
    var booleanField: Boolean,
    var arrayEnumStringField: Array<String>,
    var dateField: LfDate?,
    var dateRangeField: LfDateRange?,
    var byteField: Byte?,
    var shortField: Short?,
    var intField: Int?,
    var longField: LfLong?,
    var floatField: Float?,
    var doubleField: Double?,
    var sumField: Double?,
    var enumNameField: String,
    var enumOrdinalField: Int?,
    var stringField: String,
    var emailField: String,
    var tableField: Array<DemoTableRow>,
    var tableSumField: Double?,
    var computedArrayField: Array<DemoTableRow>
)

data class DemoTableRow(var stringCell: String, var doubleCell: Double?)

enum class DemoEnum {
    ValA,
    ValB,
    ValC
}

// Schema ==========================================================================================

// Lisbon timezone
val timeZone = TimeZone.of("Europe/Lisbon")

@JsExport
@JvmField
val demoAppSchema =
    classSchema<DemoApp> {
        DemoApp::demoForm {
            classSchema {
                DemoForm::ipAddress { stringSchema(minLength = 1) }
                DemoForm::booleanField {
                    booleanSchema(
                        validations = listOf(CannotBeFalse),
                        initialState = mapOf("someState" to 10),
                        extra = mapOf("extraSchemaProp" to "yes")
                    )
                }
                DemoForm::arrayEnumStringField {
                    arraySchema(
                        enumNameSchema<DemoEnum>(),
                        minSize = 1,
                        maxSize = 2,
                        validations = listOf(Unique())
                    )
                }
                DemoForm::dateField {
                    nullableDateSchema(
                        isRequired = true,
                        minDate =
                            Clock.System.todayAt(timeZone)
                                .atStartOfDayIn(timeZone)
                                .toEpochMilliseconds()
                                .toLfDate(),
                        validations = listOf(CannotBeASunday)
                    )
                }
                DemoForm::dateRangeField {
                    nullableDateRangeSchema(
                        isRequired = true,
                        computedMinDate = BoundToDateFieldsDate,
                        validations = listOf(CannotBeMoreThan2Weeks)
                    )
                }
                DemoForm::byteField {
                    nullableByteSchema(
                        initialValue = 127,
                        computedMin = Bound100WhenBooleanIsTrue,
                        isRequired = true,
                        validations = listOf(CannotBePowerOf2)
                    )
                }
                DemoForm::shortField {
                    nullableShortSchema(computedIsRequired = IsRequiredWhenDateIsToday, min = 18)
                }
                DemoForm::intField {
                    nullableIntSchema(computedIsRequired = IsRequiredWhenShortIsNotNull)
                }
                DemoForm::longField {
                    nullableLongSchema(initialValue = 9999.toLfLong(), isRequired = true)
                }
                DemoForm::floatField {
                    nullableFloatSchema(
                        validations = listOf(MustBeGreaterThanLong),
                        isRequired = true
                    )
                }
                DemoForm::doubleField {
                    nullableDoubleSchema(
                        initialValue = 20.0,
                        isRequired = true,
                        min = 0.0,
                        max = 100.0,
                        validations = listOf(CannotBe66)
                    )
                }
                DemoForm::sumField {
                    nullableDoubleSchema(computedValue = SumOfNumericFields, isClientOnly = false)
                }
                DemoForm::enumNameField {
                    enumNameSchema<DemoEnum>(
                        validations = listOf(CannotBeValBWhenDoubleBetween10And80)
                    )
                }
                DemoForm::enumOrdinalField {
                    nullableEnumOrdinalSchema<DemoEnum>(
                        validations = listOf(CannotBeValAWhenDoubleBetween30And70)
                    )
                }
                DemoForm::stringField {
                    stringSchema(
                        minLength = 2,
                        maxLength = 20,
                        validations =
                            listOf(
                                CannotStartWithXWhenBooleanIsTrue,
                                StringCannotBeYyyWhenLongGt9999
                            )
                    )
                }
                DemoForm::emailField { emailSchema(isRequired = true, maxLength = 100) }
                DemoForm::tableField {
                    tableSchema(
                        demoTableRowSchema(true),
                        minSize = 1,
                        maxSize = 30,
                        validations =
                            listOf(
                                UniqueBy { row ->
                                    if (row.stringCell.isEmpty()) null else row.stringCell
                                }
                            )
                    )
                }
                DemoForm::tableSumField {
                    nullableDoubleSchema(
                        computedValue = SumOfDoubles<DemoTableRow>("tableField") { it.doubleCell },
                        isClientOnly = false
                    )
                }
                DemoForm::computedArrayField {
                    arraySchema(
                        demoTableRowSchema(false),
                        computedValue = ComputedArray,
                        isClientOnly = false
                    )
                }
            }
        }
    }

fun demoTableRowSchema(withValidations: Boolean) =
    classSchema<DemoTableRow> {
        DemoTableRow::stringCell {
            if (withValidations) stringSchema(minLength = 1, maxLength = 25) else stringSchema()
        }
        DemoTableRow::doubleCell {
            if (withValidations) nullableDoubleSchema(max = 999.0) else nullableDoubleSchema()
        }
    }

// Computed values =================================================================================

object SumOfNumericFields : ComputedValue<Double?> {
    override fun Context.compute(): Double? {
        val byte: Byte? = get("byteField")
        val short: Short? = get("shortField")
        val int: Int? = get("intField")
        val long: LfLong? = get("longField")
        val float: Float? = get("floatField")
        val double: Double? = get("doubleField")

        if (byte == null &&
                short == null &&
                int == null &&
                long == null &&
                float == null &&
                double == null
        ) {
            return null
        }
        return (byte ?: 0).toDouble() +
            (short ?: 0).toDouble() +
            (int ?: 0).toDouble() +
            (long?.toLong() ?: 0).toDouble() +
            (float ?: 0).toDouble() +
            (double ?: 0.0)
    }
}

object ComputedArray : ComputedValue<Array<DemoTableRow>> {
    override fun Context.compute(): Array<DemoTableRow> =
        get<Array<DemoTableRow>>("tableField")
            .map { row -> DemoTableRow("\\_${row.stringCell}_/", row.doubleCell?.let { it * 2 }) }
            .toTypedArray()
}

// Validations =====================================================================================

object Bound100WhenBooleanIsTrue : SyncBound<Byte> {
    override fun Context.bound(): Byte? {
        val booleanField = get<Boolean>("booleanField")
        return if (booleanField) 100 else null
    }
}

object BoundToDateFieldsDate : SyncBound<LfDate> {
    override fun Context.bound(): LfDate? {
        return get("dateField")
    }
}

object CannotBeFalse : SyncValidation<Boolean> {
    override fun Context.validate(value: Boolean): Iterable<Issue> = buildList {
        if (!value) {
            add(Issue("CANNOT_BE_FALSE"))
        }
    }
}

object CannotBeASunday : SyncValidation<LfDate> {
    override fun Context.validate(value: LfDate): Iterable<Issue> = buildList {
        val instant = Instant.fromEpochMilliseconds(value.toEpochMilliseconds())
        if (instant.toLocalDateTime(timeZone).dayOfWeek == DayOfWeek.SUNDAY) {
            add(Issue("CANNOT_BE_A_SUNDAY"))
        }
    }
}

object CannotBeMoreThan2Weeks : SyncValidation<LfDateRange> {
    override fun Context.validate(value: LfDateRange): Iterable<Issue> = buildList {
        val start = Instant.fromEpochMilliseconds(value[0].toEpochMilliseconds())
        val end = Instant.fromEpochMilliseconds(value[1].toEpochMilliseconds())
        if (start.daysUntil(end, timeZone) > 14) {
            add(Issue("CANNOT_BE_MORE_THAN_2_WEEKS"))
        }
    }
}

object CannotBePowerOf2 : SyncValidation<Byte> {
    override fun Context.validate(value: Byte): Iterable<Issue> = buildList {
        if ((value != 0.toByte()) && ((value and ((value - 1).toByte())) == 0.toByte())) {
            add(Issue("CANNOT_BE_POWER_OF_2"))
        }
    }
}

object IsRequiredWhenDateIsToday : SyncIsRequired {
    override fun Context.isRequired(): Boolean {
        val date: LfDate? = get("dateField")
        return date != null &&
            Clock.System.todayAt(timeZone) ==
                Instant.fromEpochMilliseconds(date.toEpochMilliseconds())
                    .toLocalDateTime(timeZone)
                    .date
    }
}

object IsRequiredWhenShortIsNotNull : SyncIsRequired {
    override fun Context.isRequired(): Boolean {
        val short: Short? = get("shortField")
        return short != null
    }
}

object MustBeGreaterThanLong : SyncValidation<Float> {
    override fun Context.validate(value: Float): Iterable<Issue> = buildList {
        val long: LfLong? = get("../longField")
        if (long != null && value <= long.toLong()) {
            add(Issue("MUST_BE_GREATER_THAN_LONG"))
        }
    }
}

object CannotBe66 : SyncValidation<Double> {
    override fun Context.validate(value: Double): Iterable<Issue> = buildList {
        if (value == 66.0) {
            add(Issue("CANNOT_BE_66"))
        }
    }
}

object CannotBeValBWhenDoubleBetween10And80 : SyncValidation<String> {
    override fun Context.validate(value: String): Iterable<Issue> = buildList {
        val double: Double? = get("../doubleField")
        val enumValue = enumValueOf<DemoEnum>(value)
        if (enumValue == DemoEnum.ValB && double != null && double >= 10 && double <= 80) {
            add(Issue("CANNOT_BE_VAL_B_WHEN_DOUBLE_BETWEEN_10_AND_80"))
        }
    }
}

object CannotBeValAWhenDoubleBetween30And70 : SyncValidation<Int> {
    override fun Context.validate(value: Int): Iterable<Issue> = buildList {
        val double: Double? = get("../doubleField")
        val enumValue = enumValues<DemoEnum>()[value]
        if (enumValue == DemoEnum.ValA && double != null && double >= 30 && double <= 70) {
            add(Issue("CANNOT_BE_VAL_A_WHEN_DOUBLE_BETWEEN_30_AND_70"))
        }
    }
}

object CannotStartWithXWhenBooleanIsTrue : SyncValidation<String> {
    override fun Context.validate(value: String): Iterable<Issue> = buildList {
        val boolean: Boolean = get("../booleanField")
        if (boolean && value.startsWith("x", ignoreCase = true)) {
            add(Issue("CANNOT_START_WITH_X_WHEN_BOOLEAN_IS_TRUE"))
        }
    }
}

expect object StringCannotBeYyyWhenLongGt9999 : Validation<String>

package pt.lightweightform.lfkotlindemo

// Support `js-joda` timezones
@JsModule("@js-joda/timezone") @JsNonModule public external object JsJodaTimeZoneModule

private val jsJodaTz = JsJodaTimeZoneModule

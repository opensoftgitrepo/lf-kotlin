package pt.lightweightform.lfkotlindemo

import kotlin.js.Promise
import kotlinx.browser.window
import pt.lightweightform.lfkotlin.AsyncValidation
import pt.lightweightform.lfkotlin.Context
import pt.lightweightform.lfkotlin.Issue
import pt.lightweightform.lfkotlin.LfLong
import pt.lightweightform.lfkotlin.Validation

actual object StringCannotBeYyyWhenLongGt9999 : Validation<String>, AsyncValidation<String> {
    override fun Context.validate(value: String): Promise<Iterable<Issue>> = Promise { res, _ ->
        // Get dependency before waiting so that Mobx properly depends on it
        val longField: LfLong? = get("../longField")
        if (longField == null) {
            res(listOf())
        } else {
            Promise<Unit> { tRes, _ -> window.setTimeout(tRes, 3000) }.then {
                res(
                    if (value.equals("yyy", true) && longField.toLong() > 9999)
                        listOf(Issue("CANNOT_BE_YYY"))
                    else emptyList()
                )
            }
        }
    }
}

package pt.lightweightform.lfkotlindemo

import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.validations.NoOpValidation

actual object StringCannotBeYyyWhenLongGt9999 : Validation<String>, NoOpValidation<String>()

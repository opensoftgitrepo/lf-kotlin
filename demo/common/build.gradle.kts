import org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpack

plugins { kotlin("multiplatform") }

kotlin {
    jvm {
        compilations.all { kotlinOptions.jvmTarget = "11" }
        testRuns["test"].executionTask.configure { useJUnit() }
        withJava()
    }
    js(IR) {
        moduleName = "lf-kotlin-demo-common"
        browser {
            binaries.executable()
            testTask { useMocha() }
        }
    }

    sourceSets {
        // Opt-in to `RequiresOptIn`
        all { languageSettings.optIn("kotlin.RequiresOptIn") }

        val commonMain by getting {
            dependencies {
                implementation(project(":lf-kotlin"))
                implementation(
                    "org.jetbrains.kotlinx:kotlinx-datetime:${property("kotlinxDatetimeVersion")}"
                )
            }
        }
        val commonTest by getting { dependencies { implementation(kotlin("test")) } }
        val jvmMain by getting
        val jvmTest by getting
        val jsMain by getting {
            dependencies {
                implementation(npm("@js-joda/timezone", "${property("jsJodaTimezoneVersion")}"))
            }
        }
        val jsTest by getting
    }
}

/**
 * Prepare the npm package.
 *
 * This copies the (expanded) content of the `npm` directory to the Webpack destination directory
 * where the JS code has been built.
 */
val webpackTask = tasks.getByName<KotlinWebpack>("jsBrowserProductionWebpack")
val prepareNpmPackage by
    tasks.registering(Copy::class) {
        dependsOn(webpackTask)
        from(file("$projectDir/npm"))
        expand(project.properties)
        into(file(webpackTask.destinationDirectory))
    }

tasks.assemble { dependsOn(prepareNpmPackage) }

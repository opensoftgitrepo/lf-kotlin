/*
 * Expected project properties:
 * - prod: Boolean
 * - sonatypeUsername: String
 * - sonatypePassword: String
 */

// Global properties
extra.let {
    it["licenseName"] = "Apache-2.0"
    it["authorName"] = "Opensoft"
    it["authorUrl"] = "https://www.opensoft.pt/en"
    it["gitRemotePath"] = "bitbucket.org/opensoftgitrepo/lf-kotlin"
    it["gitRemoteUrl"] = "https://${it["gitRemotePath"]}"
}

plugins {
    kotlin("multiplatform") apply false
    id("com.github.node-gradle.node")
    id("com.diffplug.spotless")
    id("io.github.gradle-nexus.publish-plugin")
    id("maven-publish")
}

allprojects { repositories { mavenCentral() } }

/** Node configuration. */
node { npmInstallCommand.set(if (findProperty("prod") == "true") "ci" else "install") }

/** Code formatting configuration. */
spotless {
    val gitignorePatterns =
        file(".gitignore")
            .readLines()
            .filter { line -> !line.startsWith('#') && line.isNotEmpty() }
            .map { line -> "**/$line" }

    format("misc") {
        target(".editorconfig", ".gitignore", "gradle.properties")
        trimTrailingWhitespace()
        endWithNewline()
    }

    java {
        target(
            fileTree(rootDir) {
                include("**/*.java")
                exclude(gitignorePatterns)
            }
        )
        googleJavaFormat("${property("googleJavaFormatVersion")}").aosp().reflowLongStrings()
    }

    kotlin {
        target(
            fileTree(rootDir) {
                include("**/*.kt", "**/*.kts")
                exclude(gitignorePatterns)
            }
        )
        ktfmt("${property("ktfmtVersion")}").kotlinlangStyle()
    }

    format("prettier") {
        target(
            fileTree(rootDir) {
                include("**/*.js", "**/*.ts", "**/*.json", "**/*.html", "**/*.scss", "**/*.md")
                exclude(gitignorePatterns + "**/*.d.ts")
            }
        )
        prettier("${property("prettierVersion")}").configFile(".prettierrc")
    }
}

/** Nexus publish configuration. */
nexusPublishing { repositories { sonatype() } }

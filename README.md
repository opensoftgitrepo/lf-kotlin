# LF-Kotlin

Kotlin integration for [Lightweightform](https://lightweightform.io) (or LF for
short): an Angular-based open source library that simplifies the development of
complex web forms.

LF-Kotlin allows representing Lightweightform schemas and their respective
validations in Kotlin and provides a `Validator` class to validate forms on a
JVM backend. These schemas, when compiled to JavaScript, can then be used by LF.
This enables developers to write validations once which run on both the
server-side (JVM) and client-side (JS) of an application.

## Usage

Add the dependency to the `build.gradle.kts` of your project:

```kotlin
dependencies {
    implementation("pt.lightweightform:lf-kotlin:$version")
}
```

Create your schema in common code with its respective validations:

```kotlin
data class AppValue(var email: String, var amount: Double?)

val appSchema = classSchema<AppValue> {
    AppValue::email {
        stringSchema(minLength = 1, validations = listOf(IsValidEmail))
    }
    AppValue::amount {
        nullableDoubleSchema(min = 0.0, max = 99999999.0)
    }
}

object IsValidEmail : SyncValidation<String> {
    override fun Context.validate(value: String): Iterable<Issue> = buildList {
        if (!isValidEmail(value)) {
            add(Issue("invalidEmail"))
        }
    }
}
```

On the server-side, use a `Validator` to validate values of the schema:

```kotlin
val validator = Validator(appSchema)

validator.isValid(AppValue("abc", 2.0)) // false
validator.isValid(AppValue("john@doe.com", 2000.0)) // true
```

On the client-side, provide `appSchema` as the schema to Lightweightform.

## Demo

The [demo folder](./demo) provides an example using `Lf-Kotlin` in common,
server, and client code.

## License

LF-Kotlin is released under the [Apache License 2.0](./LICENSE).

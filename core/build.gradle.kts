// Project settings
project.description = "Kotlin integration for Lightweightform"

plugins {
    kotlin("multiplatform")
    id("com.github.node-gradle.node")
    id("org.jetbrains.dokka")
    id("signing")
    id("maven-publish")
}

kotlin {
    explicitApi()

    jvm {
        compilations.all { kotlinOptions.jvmTarget = "1.8" }
        testRuns["test"].executionTask.configure { useJUnit() }
    }
    js(BOTH) { browser { testTask { useMocha() } } }

    sourceSets {
        // Opt-in to `RequiresOptIn`
        all { languageSettings.optIn("kotlin.RequiresOptIn") }

        val commonMain by getting { dependencies { implementation(kotlin("reflect")) } }
        val commonTest by getting { dependencies { implementation(kotlin("test")) } }
        val jvmMain by getting
        val jvmTest by getting
        val jsMain by getting
        val jsTest by getting
    }
}

/** Configure documentation. */
tasks.dokkaHtml.configure {
    outputDirectory.set(buildDir.resolve("dokka"))

    dokkaSourceSets {
        configureEach { skipEmptyPackages.set(true) }
        for (sourceSet in kotlin.sourceSets.names.filter { name -> name.endsWith("Main") }) {
            named(sourceSet) {
                sourceLink {
                    val sourcePath = "src/$sourceSet/kotlin"
                    localDirectory.set(file(sourcePath))
                    remoteUrl.set(
                        uri(
                                "${rootProject.ext["gitRemoteUrl"]}/src/master/${project.name}/$sourcePath"
                            )
                            .toURL()
                    )
                    remoteLineSuffix.set("#lines-")
                }
            }
        }
    }
}

val dokkaHtmlJar by
    tasks.registering(Jar::class) {
        from(tasks.dokkaHtml)
        dependsOn(tasks.dokkaHtml)
        archiveClassifier.set("javadoc")
    }

/** Configure publish. */
publishing {
    publications.withType<MavenPublication>().configureEach {
        artifact(dokkaHtmlJar.get())

        pom {
            name.set(project.name)
            description.set(project.description)
            url.set("${rootProject.ext["gitRemoteUrl"]}")

            licenses {
                license {
                    name.set("${rootProject.ext["licenseName"]}")
                    url.set("${rootProject.ext["gitRemoteUrl"]}/src/master/LICENSE")
                }
            }
            scm {
                url.set("${rootProject.ext["gitRemoteUrl"]}")
                connection.set("scm:git:git://${rootProject.ext["gitRemotePath"]}.git")
            }
            developers {
                developer {
                    name.set("${rootProject.ext["authorName"]}")
                    url.set("${rootProject.ext["authorUrl"]}")
                }
            }
        }
    }
}

// Sign published packages.
signing {
    setRequired(provider { gradle.taskGraph.hasTask("publish") })
    sign(publishing.publications)
}

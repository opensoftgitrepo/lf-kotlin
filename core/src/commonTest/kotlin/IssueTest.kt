import kotlin.test.Test
import kotlin.test.assertEquals
import pt.lightweightform.lfkotlin.Issue

class IssueTest {
    @Test
    fun testToString() {
        val issueFull = Issue("X", isWarning = true, data = "DATA")
        assertEquals("Issue(code=X, isWarning=true, data=DATA)", issueFull.toString())

        val issueWarn = Issue("X", isWarning = true)
        assertEquals("Issue(code=X, isWarning=true)", issueWarn.toString())

        val issueData = Issue("X", data = "DATA")
        assertEquals("Issue(code=X, data=DATA)", issueData.toString())

        val issueCode = Issue("X")
        assertEquals("Issue(code=X)", issueCode.toString())
    }
}

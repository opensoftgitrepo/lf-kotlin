import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertTrue
import pt.lightweightform.lfkotlin.LF_LONG_MAX_VALUE
import pt.lightweightform.lfkotlin.LF_LONG_MIN_VALUE
import pt.lightweightform.lfkotlin.compareTo
import pt.lightweightform.lfkotlin.toLfLong
import pt.lightweightform.lfkotlin.toLong

class LfLongTest {
    @Test
    fun testConversions() {
        val long = 90L
        assertEquals(long, long.toLfLong().toLong())

        val maxValue = LF_LONG_MAX_VALUE.toLong()
        val minValue = LF_LONG_MIN_VALUE.toLong()
        assertEquals(maxValue, maxValue.toLfLong().toLong())
        assertEquals(minValue, minValue.toLfLong().toLong())

        val tooBig = maxValue + 1
        val tooSmall = minValue - 1
        assertFails { tooBig.toLfLong() }
        assertFails { tooSmall.toLfLong() }
    }

    @Test
    fun testComparisons() {
        val value = 10.toLfLong()
        assertTrue(value > 5.toLfLong())
        assertTrue(value > 5.toByte())
        assertTrue(value > 5.toShort())
        assertTrue(value > 5)
        assertTrue(value > 5f)
        assertTrue(value > 5.0)
        assertTrue(5.toLfLong() < value)
        assertTrue(5.toByte() < value)
        assertTrue(5.toShort() < value)
        assertTrue(5 < value)
        assertTrue(5f < value)
        assertTrue(5.0 < value)
        assertTrue(10.1 > value)
    }
}

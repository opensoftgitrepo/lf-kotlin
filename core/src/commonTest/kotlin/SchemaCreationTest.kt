import kotlin.test.Test
import kotlin.test.assertTrue
import pt.lightweightform.lfkotlin.exampleValueSchema

class SchemaCreationTest {
    // Simply test that creating a schema doesn't result in a runtime error in all platforms
    @Test
    fun testSchemaCreation() {
        val schema = exampleValueSchema
        @Suppress("SENSELESS_COMPARISON") assertTrue(schema != null)
    }
}

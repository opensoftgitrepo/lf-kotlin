package pt.lightweightform.lfkotlin

import pt.lightweightform.lfkotlin.computedvalues.SumOfLongs
import pt.lightweightform.lfkotlin.schemas.arraySchema
import pt.lightweightform.lfkotlin.schemas.booleanSchema
import pt.lightweightform.lfkotlin.schemas.byteSchema
import pt.lightweightform.lfkotlin.schemas.classSchema
import pt.lightweightform.lfkotlin.schemas.dateRangeSchema
import pt.lightweightform.lfkotlin.schemas.dateSchema
import pt.lightweightform.lfkotlin.schemas.doubleSchema
import pt.lightweightform.lfkotlin.schemas.enumNameSchema
import pt.lightweightform.lfkotlin.schemas.enumOrdinalSchema
import pt.lightweightform.lfkotlin.schemas.floatSchema
import pt.lightweightform.lfkotlin.schemas.intSchema
import pt.lightweightform.lfkotlin.schemas.longSchema
import pt.lightweightform.lfkotlin.schemas.nullableArraySchema
import pt.lightweightform.lfkotlin.schemas.nullableBooleanSchema
import pt.lightweightform.lfkotlin.schemas.nullableByteSchema
import pt.lightweightform.lfkotlin.schemas.nullableDateRangeSchema
import pt.lightweightform.lfkotlin.schemas.nullableDateSchema
import pt.lightweightform.lfkotlin.schemas.nullableDoubleSchema
import pt.lightweightform.lfkotlin.schemas.nullableEnumNameSchema
import pt.lightweightform.lfkotlin.schemas.nullableEnumOrdinalSchema
import pt.lightweightform.lfkotlin.schemas.nullableFloatSchema
import pt.lightweightform.lfkotlin.schemas.nullableIntSchema
import pt.lightweightform.lfkotlin.schemas.nullableLongSchema
import pt.lightweightform.lfkotlin.schemas.nullableShortSchema
import pt.lightweightform.lfkotlin.schemas.nullableStringSchema
import pt.lightweightform.lfkotlin.schemas.nullableTableSchema
import pt.lightweightform.lfkotlin.schemas.nullableTupleSchema
import pt.lightweightform.lfkotlin.schemas.shortSchema
import pt.lightweightform.lfkotlin.schemas.stringSchema
import pt.lightweightform.lfkotlin.schemas.tableSchema
import pt.lightweightform.lfkotlin.schemas.tupleSchema
import pt.lightweightform.lfkotlin.validations.UniqueBy

// Data ============================================================================================

data class ExampleValue(
    var arrayOfStrings: Array<String> = arrayOf(),
    var nullableArrayOfStrings: Array<String>? = null,
    var boolean: Boolean = false,
    var nullableBoolean: Boolean? = null,
    var byte: Byte = 0,
    var nullableByte: Byte? = null,
    var dateRange: LfDateRange = arrayOf(0L.toLfDate(), 100L.toLfDate()),
    var nullableDateRange: LfDateRange? = null,
    var date: LfDate = 0L.toLfDate(),
    var nullableDate: LfDate? = null,
    var double: Double = 0.0,
    var nullableDouble: Double? = null,
    var enumName: String = "V1",
    var nullableEnumName: String? = null,
    var enumOrdinal: Int = 0,
    var nullableEnumOrdinal: Int? = null,
    var float: Float = 0f,
    var nullableFloat: Float? = null,
    var int: Int = 0,
    var nullableInt: Int? = null,
    var long: LfLong = 0.toLfLong(),
    var nullableLong: LfLong? = null,
    var short: Short = 0,
    var nullableShort: Short? = null,
    var string: String = "",
    var nullableString: String? = null,
    var table: Array<ExampleTableRow> = arrayOf(),
    var tableSum: LfLong = 0.toLfLong(),
    var nullableTable: Array<ExampleTableRow>? = null,
    var tupleStringInt: Array<Any?> = arrayOf("", 0),
    var nullableTupleStringInt: Array<Any?>? = null
)

enum class ExampleEnum {
    V1,
    V2,
    V3
}

data class ExampleTableRow(var stringCell: String = "", var intCell: Int = 0)

// Schema ==========================================================================================

val exampleTableRowSchema =
    classSchema<ExampleTableRow> {
        ExampleTableRow::stringCell { stringSchema() }
        ExampleTableRow::intCell { intSchema() }
    }

@Suppress("UNCHECKED_CAST")
val exampleValueSchema =
    classSchema<ExampleValue> {
        ExampleValue::arrayOfStrings { arraySchema(stringSchema(), minSize = 1, maxSize = 3) }
        ExampleValue::nullableArrayOfStrings {
            nullableArraySchema(stringSchema(), isRequired = true)
        }
        ExampleValue::boolean { booleanSchema(allowedValues = listOf(true)) }
        ExampleValue::nullableBoolean { nullableBooleanSchema() }
        ExampleValue::byte { byteSchema(min = 10, max = 44) }
        ExampleValue::nullableByte { nullableByteSchema() }
        ExampleValue::dateRange { dateRangeSchema() }
        ExampleValue::nullableDateRange { nullableDateRangeSchema(minDate = 10L.toLfDate()) }
        ExampleValue::date { dateSchema(maxDate = 15L.toLfDate()) }
        ExampleValue::nullableDate { nullableDateSchema() }
        ExampleValue::double { doubleSchema(max = 99999.0) }
        ExampleValue::nullableDouble { nullableDoubleSchema() }
        ExampleValue::enumName { enumNameSchema<ExampleEnum>() }
        ExampleValue::nullableEnumName { nullableEnumNameSchema<ExampleEnum>() }
        ExampleValue::enumOrdinal { enumOrdinalSchema<ExampleEnum>() }
        ExampleValue::nullableEnumOrdinal { nullableEnumOrdinalSchema<ExampleEnum>() }
        ExampleValue::float { floatSchema(validations = listOf(MustBeOdd as Validation<Float>)) }
        ExampleValue::nullableFloat { nullableFloatSchema() }
        ExampleValue::int { intSchema(computedMin = Bound10WhenBooleanIsTrue) }
        ExampleValue::nullableInt { nullableIntSchema(computedMax = Bound10WhenBooleanIsTrue) }
        ExampleValue::long { longSchema(validations = listOf(MustBeOdd as Validation<LfLong>)) }
        ExampleValue::nullableLong { nullableLongSchema() }
        ExampleValue::short {
            shortSchema(validations = listOf(ShouldBeEvenWhenNullableBooleanIsFalse))
        }
        ExampleValue::nullableShort { nullableShortSchema() }
        ExampleValue::string { stringSchema(minLength = 3) }
        ExampleValue::nullableString { nullableStringSchema() }
        ExampleValue::table {
            tableSchema(
                exampleTableRowSchema,
                maxSize = 1,
                validations = listOf(UniqueBy { row -> row.stringCell })
            )
        }
        ExampleValue::tableSum {
            longSchema(
                computedValue = SumOfLongs<ExampleTableRow>("table") { it.intCell.toLfLong() },
                isClientOnly = false
            )
        }
        ExampleValue::nullableTable { nullableTableSchema(exampleTableRowSchema) }
        ExampleValue::tupleStringInt {
            tupleSchema(
                listOf(
                    stringSchema(maxLength = 18),
                    intSchema(validations = listOf(ShouldBeEvenWhenNullableBooleanIsFalse))
                ) as
                    List<Schema<Any?>>
            )
        }
        ExampleValue::nullableTupleStringInt {
            nullableTupleSchema(listOf(stringSchema(), intSchema()) as List<Schema<Any?>>)
        }
    }

// Validations =====================================================================================

object Bound10WhenBooleanIsTrue : SyncBound<Int> {
    override fun Context.bound(): Int? {
        val boolean: Boolean = get("/boolean")
        return if (boolean) 10 else null
    }
}

object MustBeOdd : SyncValidation<Number> {
    override fun Context.validate(value: Number): Iterable<Issue> = buildList {
        if (value.toDouble() % 2 == 0.0) {
            add(Issue("NUMBER_MUST_BE_ODD"))
        }
    }
}

object ShouldBeEvenWhenNullableBooleanIsFalse : SyncValidation<Number> {
    override fun Context.validate(value: Number): Iterable<Issue> = buildList {
        val nullableBoolean: Boolean? = get("/nullableBoolean")

        if (nullableBoolean == false && value.toDouble() % 2 != 0.0) {
            add(Issue("NUMBER_MUST_BE_EVEN", isWarning = true))
        }
    }
}

package pt.lightweightform.lfkotlin

/** [LfLong] is represented as a "number" in JS. */
public actual typealias LfLong = Double

public actual fun Long.toLfLongImpl(): LfLong = this.toDouble()

public actual fun LfLong.toLongImpl(): Long = this.toLong()

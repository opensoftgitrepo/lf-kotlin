package pt.lightweightform.lfkotlin.internal

/**
 * JavaScript's `Proxy` class (not supported by IE, for IE support with the legacy IE compiler,
 * check the `legacy-ie11` branch).
 */
internal external class Proxy(target: dynamic, handler: dynamic)

/** JavaScript's `Reflect` object (needs to be polyfilled for IE support). */
internal external val Reflect: dynamic

/** Type of a [Proxy] `get` handler. */
internal typealias ProxyGetHandler = (target: dynamic, prop: String, receiver: dynamic) -> dynamic

/** Returns a [Proxy] of [target] that proxies its `get` requests to the [getHandler]. */
internal fun proxyGet(target: dynamic, getHandler: ProxyGetHandler): dynamic {
    val handler = js("{}")
    handler.get = getHandler
    return Proxy(target, handler)
}

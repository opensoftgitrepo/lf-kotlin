package pt.lightweightform.lfkotlin.internal

import kotlin.reflect.KClass

/** JavaScript's `Object` object. */
private external val Object: dynamic

/** Creates a Kotlin class instance given a [KClass] and the constructor arguments. */
internal fun <T : Any> constructFromKClass(kClass: KClass<T>, vararg arguments: Any?): T {
    // XXX: Work around not having access to `kClass.constructors` or to the `new` keyword
    val obj = Object.create(kClass.js.asDynamic().prototype)
    obj.constructor.apply(obj, arguments)
    return obj as T
}

package pt.lightweightform.lfkotlin

public open class JsonObject

public actual fun objectOf(vararg pairs: Pair<String, Any?>): Any {
    val obj = object : JsonObject() {}.asDynamic()
    for ((key, value) in pairs) {
        when (value) {
            null, is Boolean, is String, is JsonObject -> obj[key] = value
            is Number -> obj[key] = if (value is Long) value.toDouble() else value
            is List<*> -> obj[key] = jsonArrayOf(value)
            else -> error("Invalid object entry value: $value")
        }
    }
    return obj as JsonObject
}

private fun jsonArrayOf(value: List<*>): Array<*> {
    val list = mutableListOf<Any?>()
    for (el in value) {
        when (el) {
            null, is Boolean, is String, is JsonObject -> list += el
            is Number -> list += if (el is Long) el.toDouble() else el
            is List<*> -> list.add(jsonArrayOf(el))
            else -> error("Invalid array element: $el")
        }
    }
    return list.toTypedArray()
}

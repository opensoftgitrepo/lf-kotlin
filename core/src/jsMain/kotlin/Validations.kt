package pt.lightweightform.lfkotlin

import kotlin.js.Promise

/** Asynchronous schema validation. */
public interface AsyncValidation<in T> : Validation<T> {
    /**
     * Asynchronously validates a value within a context by returning an iterable of found issues.
     */
    public fun Context.validate(value: T): Promise<Iterable<Issue>>
}

/** Asynchronous computed "is required" validation. */
public interface AsyncIsRequired : IsRequired {
    /** Asynchronously verifies whether a value is required within a context. */
    public fun Context.isRequired(): Promise<Boolean>
}

/** Asynchronous computed "allowed values" validation. */
public interface AsyncAllowedValues<out T> : AllowedValues<T> {
    /** Asynchronously returns the allowed values that a schema is allowed to contain. */
    public fun Context.allowedValues(): Promise<List<T>?>
}

/** Asynchronous computed "bound" used for validation. */
public interface AsyncBound<out T> : Bound<T> {
    /** Asynchronously returns a bound that should be respected. */
    public fun Context.bound(): Promise<T?>
}

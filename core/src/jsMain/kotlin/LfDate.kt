package pt.lightweightform.lfkotlin

import kotlin.js.Date as JsDate

/** [LfDate] is represented as a [Date][JsDate] in JS. */
public actual typealias LfDate = JsDate

public actual fun Long.toLfDate(): LfDate = JsDate(this.toDouble())

public actual fun LfDate.toEpochMilliseconds(): Long = this.getTime().toLong()

@Suppress("EXTENSION_SHADOWED_BY_MEMBER")
public actual fun LfDate.toISOString(): String = this.toISOString()

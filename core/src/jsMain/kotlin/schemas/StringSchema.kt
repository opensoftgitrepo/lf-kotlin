package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema
import pt.lightweightform.lfkotlin.internal.toStorageBound

@Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
internal actual fun stringSchemaImpl(
    isNullable: Boolean,
    initialValue: String?,
    computedInitialValue: InitialValue<String?>?,
    computedValue: ComputedValue<String?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<String>?,
    computedAllowedValues: AllowedValues<String>?,
    disallowedValueCode: String?,
    minLength: Int?,
    computedMinLength: Bound<Int>?,
    minLengthCode: String?,
    maxLength: Int?,
    computedMaxLength: Bound<Int>?,
    maxLengthCode: String?,
    validations: List<Validation<String>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<String?> {
    val schema = js("{}") as Schema<String?>

    addCommonPropsToSchema(
        schema,
        "string",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

    schema.asDynamic().minLength =
        when {
            minLength != null -> minLength
            computedMinLength != null -> toStorageBound(computedMinLength)
            else -> undefined
        }
    schema.asDynamic().minLengthCode = minLengthCode ?: undefined
    schema.asDynamic().maxLength =
        when {
            maxLength != null -> maxLength
            computedMaxLength != null -> toStorageBound(computedMaxLength)
            else -> undefined
        }
    schema.asDynamic().maxLengthCode = maxLengthCode ?: undefined

    return schema
}

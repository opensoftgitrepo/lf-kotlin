package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.LF_LONG_MAX_VALUE
import pt.lightweightform.lfkotlin.LF_LONG_MIN_VALUE
import pt.lightweightform.lfkotlin.LfLong
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema
import pt.lightweightform.lfkotlin.internal.toStorageBound

@Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
internal actual fun longSchemaImpl(
    isNullable: Boolean,
    initialValue: LfLong?,
    computedInitialValue: InitialValue<LfLong?>?,
    computedValue: ComputedValue<LfLong?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<LfLong>?,
    computedAllowedValues: AllowedValues<LfLong>?,
    disallowedValueCode: String?,
    min: LfLong?,
    computedMin: Bound<LfLong>?,
    minCode: String?,
    max: LfLong?,
    computedMax: Bound<LfLong>?,
    maxCode: String?,
    validations: List<Validation<LfLong>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<LfLong?> {
    val schema = js("{}") as Schema<LfLong?>

    schema.asDynamic().isInteger = true

    addCommonPropsToSchema(
        schema,
        "number",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

    schema.asDynamic().min =
        when {
            min != null -> min
            computedMin != null -> toStorageBound(computedMin, LF_LONG_MIN_VALUE)
            else -> LF_LONG_MIN_VALUE
        }
    schema.asDynamic().minCode = minCode ?: undefined
    schema.asDynamic().max =
        when {
            max != null -> max
            computedMax != null -> toStorageBound(computedMax, LF_LONG_MAX_VALUE)
            else -> LF_LONG_MAX_VALUE
        }
    schema.asDynamic().maxCode = maxCode ?: undefined

    return schema
}

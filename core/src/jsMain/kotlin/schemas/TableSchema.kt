package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema
import pt.lightweightform.lfkotlin.internal.toStorageBound

@Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
internal actual fun <T : Any> tableSchemaImpl(
    rowsSchema: ClassSchema<T>,
    isNullable: Boolean,
    initialValue: Array<T>?,
    computedInitialValue: InitialValue<Array<T>?>?,
    computedValue: ComputedValue<Array<T>?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Array<T>>?,
    computedAllowedValues: AllowedValues<Array<T>>?,
    disallowedValueCode: String?,
    minSize: Int?,
    computedMinSize: Bound<Int>?,
    minSizeCode: String?,
    maxSize: Int?,
    computedMaxSize: Bound<Int>?,
    maxSizeCode: String?,
    validations: List<Validation<Array<T>>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Array<T>?> {
    val schema = js("{}") as Schema<Array<T>?>

    schema.asDynamic().rowsSchema = rowsSchema

    addCommonPropsToSchema(
        schema,
        "table",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

    schema.asDynamic().minSize =
        when {
            minSize != null -> minSize
            computedMinSize != null -> toStorageBound(computedMinSize)
            else -> undefined
        }
    schema.asDynamic().minSizeCode = minSizeCode ?: undefined
    schema.asDynamic().maxSize =
        when {
            maxSize != null -> maxSize
            computedMaxSize != null -> toStorageBound(computedMaxSize)
            else -> undefined
        }
    schema.asDynamic().maxSizeCode = maxSizeCode ?: undefined

    return schema
}

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema

@Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
internal actual fun tupleSchemaImpl(
    elementsSchemas: List<Schema<Any?>>,
    isNullable: Boolean,
    initialValue: Array<Any?>?,
    computedInitialValue: InitialValue<Array<Any?>?>?,
    computedValue: ComputedValue<Array<Any?>?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Array<Any?>>?,
    computedAllowedValues: AllowedValues<Array<Any?>>?,
    disallowedValueCode: String?,
    validations: List<Validation<Array<Any?>>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Array<Any?>?> {
    val schema = js("{}") as Schema<Array<Any?>?>

    schema.asDynamic().elementsSchemas = elementsSchemas.toTypedArray()

    return addCommonPropsToSchema(
        schema,
        "tuple",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )
}

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema

@Suppress("UNCHECKED_CAST", "UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
internal actual fun booleanSchemaImpl(
    isNullable: Boolean,
    initialValue: Boolean?,
    computedInitialValue: InitialValue<Boolean?>?,
    computedValue: ComputedValue<Boolean?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Boolean>?,
    computedAllowedValues: AllowedValues<Boolean>?,
    disallowedValueCode: String?,
    validations: List<Validation<Boolean>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Boolean?> =
    addCommonPropsToSchema(
        js("{}") as Schema<Boolean?>,
        "boolean",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

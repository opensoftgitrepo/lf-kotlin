package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.LfDate
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema
import pt.lightweightform.lfkotlin.internal.toStorageBound

@Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
internal actual fun dateSchemaImpl(
    isNullable: Boolean,
    initialValue: LfDate?,
    computedInitialValue: InitialValue<LfDate?>?,
    computedValue: ComputedValue<LfDate?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<LfDate>?,
    computedAllowedValues: AllowedValues<LfDate>?,
    disallowedValueCode: String?,
    minDate: LfDate?,
    computedMinDate: Bound<LfDate>?,
    minDateCode: String?,
    maxDate: LfDate?,
    computedMaxDate: Bound<LfDate>?,
    maxDateCode: String?,
    validations: List<Validation<LfDate>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<LfDate?> {
    val schema = js("{}") as Schema<LfDate?>

    addCommonPropsToSchema(
        schema,
        "date",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

    schema.asDynamic().minDate =
        when {
            minDate != null -> minDate
            computedMinDate != null -> toStorageBound(computedMinDate)
            else -> undefined
        }
    schema.asDynamic().minDateCode = minDateCode ?: undefined
    schema.asDynamic().maxDate =
        when {
            maxDate != null -> maxDate
            computedMaxDate != null -> toStorageBound(computedMaxDate)
            else -> undefined
        }
    schema.asDynamic().maxDateCode = maxDateCode ?: undefined

    return schema
}

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema
import pt.lightweightform.lfkotlin.internal.toStorageBound

@Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
internal actual fun intSchemaImpl(
    isNullable: Boolean,
    initialValue: Int?,
    computedInitialValue: InitialValue<Int?>?,
    computedValue: ComputedValue<Int?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Int>?,
    computedAllowedValues: AllowedValues<Int>?,
    disallowedValueCode: String?,
    min: Int?,
    computedMin: Bound<Int>?,
    minCode: String?,
    max: Int?,
    computedMax: Bound<Int>?,
    maxCode: String?,
    validations: List<Validation<Int>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Int?> {
    val schema = js("{}") as Schema<Int?>

    schema.asDynamic().isInteger = true

    addCommonPropsToSchema(
        schema,
        "number",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

    schema.asDynamic().min =
        when {
            min != null -> min
            computedMin != null -> toStorageBound(computedMin, Int.MIN_VALUE)
            else -> Int.MIN_VALUE
        }
    schema.asDynamic().minCode = minCode ?: undefined
    schema.asDynamic().max =
        when {
            max != null -> max
            computedMax != null -> toStorageBound(computedMax, Int.MAX_VALUE)
            else -> Int.MAX_VALUE
        }
    schema.asDynamic().maxCode = maxCode ?: undefined

    return schema
}

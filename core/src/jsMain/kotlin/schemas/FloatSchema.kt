package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema
import pt.lightweightform.lfkotlin.internal.toStorageBound

@Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
internal actual fun floatSchemaImpl(
    isNullable: Boolean,
    representsInteger: Boolean,
    initialValue: Float?,
    computedInitialValue: InitialValue<Float?>?,
    computedValue: ComputedValue<Float?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Float>?,
    computedAllowedValues: AllowedValues<Float>?,
    disallowedValueCode: String?,
    min: Float?,
    computedMin: Bound<Float>?,
    minCode: String?,
    max: Float?,
    computedMax: Bound<Float>?,
    maxCode: String?,
    validations: List<Validation<Float>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Float?> {
    val schema = js("{}") as Schema<Float?>

    schema.asDynamic().isInteger = representsInteger

    addCommonPropsToSchema(
        schema,
        "number",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

    schema.asDynamic().min =
        when {
            min != null -> min
            computedMin != null -> toStorageBound(computedMin)
            else -> undefined
        }
    schema.asDynamic().minCode = minCode ?: undefined
    schema.asDynamic().max =
        when {
            max != null -> max
            computedMax != null -> toStorageBound(computedMax)
            else -> undefined
        }
    schema.asDynamic().maxCode = maxCode ?: undefined

    return schema
}

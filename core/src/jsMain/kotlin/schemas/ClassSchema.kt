package pt.lightweightform.lfkotlin.schemas

import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty1
import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema

public actual external interface ClassSchema<T> : Schema<T> {
    public actual var kClass: KClass<Any>
    public actual var childInfoByName: Map<String, ChildInfo<T>>
    public actual var constructorFunction: ConstructorFunction<T>?
    public var fieldsSchemas: dynamic
}

@Suppress("UNCHECKED_CAST", "UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
public actual fun <T : Any> classSchemaImpl(
    kClass: KClass<T>,
    childrenSchemas: Map<KMutableProperty1<T, *>, Schema<*>>,
    constructorFunction: ConstructorFunction<T>?,
    isNullable: Boolean,
    initialValue: T?,
    computedInitialValue: InitialValue<T?>?,
    computedValue: ComputedValue<T?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<T>?,
    computedAllowedValues: AllowedValues<T>?,
    disallowedValueCode: String?,
    validations: List<Validation<T>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): ClassSchema<T?> {
    val schema = js("{}") as ClassSchema<T?>

    schema.kClass = kClass as KClass<Any>
    schema.constructorFunction = constructorFunction
    val childInfoByName = mutableMapOf<String, ChildInfo<T>>()
    for ((prop, childSchema) in childrenSchemas) {
        childInfoByName[prop.name] = ChildInfo(prop, childSchema)
    }
    schema.childInfoByName = childInfoByName as Map<String, ChildInfo<T?>>
    schema.fieldsSchemas = js("{}")
    for ((prop, childSchema) in childrenSchemas) {
        schema.fieldsSchemas[prop.name] = childSchema
    }

    return addCommonPropsToSchema(
        schema,
        "record",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )
}

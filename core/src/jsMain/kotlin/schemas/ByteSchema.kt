package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema
import pt.lightweightform.lfkotlin.internal.toStorageBound

@Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
internal actual fun byteSchemaImpl(
    isNullable: Boolean,
    initialValue: Byte?,
    computedInitialValue: InitialValue<Byte?>?,
    computedValue: ComputedValue<Byte?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Byte>?,
    computedAllowedValues: AllowedValues<Byte>?,
    disallowedValueCode: String?,
    min: Byte?,
    computedMin: Bound<Byte>?,
    minCode: String?,
    max: Byte?,
    computedMax: Bound<Byte>?,
    maxCode: String?,
    validations: List<Validation<Byte>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Byte?> {
    val schema = js("{}") as Schema<Byte?>

    schema.asDynamic().isInteger = true

    addCommonPropsToSchema(
        schema,
        "number",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

    schema.asDynamic().min =
        when {
            min != null -> min
            computedMin != null -> toStorageBound(computedMin, Byte.MIN_VALUE)
            else -> Byte.MIN_VALUE
        }
    schema.asDynamic().minCode = minCode ?: undefined
    schema.asDynamic().max =
        when {
            max != null -> max
            computedMax != null -> toStorageBound(computedMax, Byte.MAX_VALUE)
            else -> Byte.MAX_VALUE
        }
    schema.asDynamic().maxCode = maxCode ?: undefined

    return schema
}

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.internal.addCommonPropsToSchema
import pt.lightweightform.lfkotlin.internal.toStorageBound

@Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
internal actual fun shortSchemaImpl(
    isNullable: Boolean,
    initialValue: Short?,
    computedInitialValue: InitialValue<Short?>?,
    computedValue: ComputedValue<Short?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Short>?,
    computedAllowedValues: AllowedValues<Short>?,
    disallowedValueCode: String?,
    min: Short?,
    computedMin: Bound<Short>?,
    minCode: String?,
    max: Short?,
    computedMax: Bound<Short>?,
    maxCode: String?,
    validations: List<Validation<Short>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Short?> {
    val schema = js("{}") as Schema<Short?>

    schema.asDynamic().isInteger = true

    addCommonPropsToSchema(
        schema,
        "number",
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

    schema.asDynamic().min =
        when {
            min != null -> min
            computedMin != null -> toStorageBound(computedMin, Short.MIN_VALUE)
            else -> Short.MIN_VALUE
        }
    schema.asDynamic().minCode = minCode ?: undefined
    schema.asDynamic().max =
        when {
            max != null -> max
            computedMax != null -> toStorageBound(computedMax, Short.MAX_VALUE)
            else -> Short.MAX_VALUE
        }
    schema.asDynamic().maxCode = maxCode ?: undefined

    return schema
}

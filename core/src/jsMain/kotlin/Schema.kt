package pt.lightweightform.lfkotlin

public actual external interface Schema<T> {
    public var type: String
    public var isNullable: Boolean?
    public var initialValue: Any?
    public var computedValue: StorageContextFn?
    public var isClientOnly: Boolean?
    public var isRequired: Any?
    public var isRequiredCode: String?
    public var allowedValues: Any?
    public var disallowedValueCode: String?
    public var validate: Array<StorageContextFn>?
    public var initialState: Any?
}

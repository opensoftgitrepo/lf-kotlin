package pt.lightweightform.lfkotlin

import kotlin.js.Promise

/** Asynchronous computed "state property". */
public interface AsyncStateProperty<out T> : StateProperty<T> {
    /** Asynchronously returns the value of a state property. */
    public fun Context.property(): Promise<T>
}

package pt.lightweightform.lfkotlin.util

import kotlin.js.Promise
import pt.lightweightform.lfkotlin.AsyncBound
import pt.lightweightform.lfkotlin.AsyncIsRequired
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.Context
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.SyncBound
import pt.lightweightform.lfkotlin.SyncIsRequired

public actual fun <T> isRequiredToBound(isRequired: IsRequired, boundValue: T): Bound<T> =
    when (isRequired) {
        is SyncIsRequired -> SyncIsRequiredBound(isRequired, boundValue)
        is AsyncIsRequired -> AsyncIsRequiredBound(isRequired, boundValue)
        else -> error("Unsupported `IsRequired` implementation")
    }

private class SyncIsRequiredBound<T>(
    private val isRequired: SyncIsRequired,
    private val boundValue: T
) : SyncBound<T> {
    override fun Context.bound(): T? = isRequired.run { if (isRequired()) boundValue else null }
}

private class AsyncIsRequiredBound<T>(
    private val isRequired: AsyncIsRequired,
    private val boundValue: T
) : AsyncBound<T> {
    override fun Context.bound(): Promise<T?> =
        isRequired.run { isRequired().then { if (it) boundValue else null } }
}

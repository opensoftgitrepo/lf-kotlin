package pt.lightweightform.lfkotlin.util

import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.IsRequired

/**
 * Helper function that creates a [Bound] validation from an [IsRequired] validation. If a field is
 * required according to [isRequired], then the returned bound will be of [boundValue].
 */
public expect fun <T> isRequiredToBound(isRequired: IsRequired, boundValue: T): Bound<T>

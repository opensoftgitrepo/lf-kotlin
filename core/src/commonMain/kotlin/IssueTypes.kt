@file:JvmName("IssueTypes")

package pt.lightweightform.lfkotlin

import kotlin.jvm.JvmName

/** Issue type of issues emitted when a value that is required is `null`. */
public const val IS_REQUIRED_TYPE: String = "isRequired"

/** Issue type of issues emitted when a value isn't one of the defined `allowedValues`. */
public const val DISALLOWED_VALUE_TYPE: String = "disallowedValue"

/** Issue type representing that the date is out of bounds. */
public const val DATE_OUT_OF_BOUNDS_TYPE: String = "dateOutOfBounds"

/** Issue type representing that the size is out of bounds. */
public const val SIZE_OUT_OF_BOUNDS_TYPE: String = "sizeOutOfBounds"

/** Issue type representing that the number is out of bounds. */
public const val NUMBER_OUT_OF_BOUNDS_TYPE: String = "numberOutOfBounds"

/** Issue type representing that the string's length is out of bounds. */
public const val LENGTH_OUT_OF_BOUNDS_TYPE: String = "lengthOutOfBounds"

/** Issue type used to represent that a received value does not match the computed value. */
public const val MISMATCHED_COMPUTED_TYPE: String = "mismatchedComputed"

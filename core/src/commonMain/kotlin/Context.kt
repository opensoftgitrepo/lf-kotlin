package pt.lightweightform.lfkotlin

/** Context available for use in validations, computed values, and state properties. */
public expect class Context {
    /** Path associated with the context. */
    public val currentPath: Path

    /** Obtains the context at the provided path (relative to `currentPath`). */
    public fun relativeContext(relativePath: Path): Context

    /** Gets the value at the provided path (relative to `currentPath`). */
    public fun <T> get(relativePath: Path = "."): T

    /** Gets the state property `prop` at the provided path (relative to `currentPath`). */
    public fun <T> getStateProperty(relativePath: Path = ".", prop: String): T
}

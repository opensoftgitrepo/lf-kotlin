package pt.lightweightform.lfkotlin

/** Generic schema of values of type [T]. */
public expect interface Schema<T>

package pt.lightweightform.lfkotlin

public expect fun objectOf(vararg pairs: Pair<String, Any?>): Any

package pt.lightweightform.lfkotlin

/** Computed initial value. */
public interface InitialValue<out T> {
    /** Compute the initial value within a context. */
    public fun Context.initialValue(): T
}

@file:JvmName("LfDate")

package pt.lightweightform.lfkotlin

import kotlin.jvm.JvmName

/**
 * A common date which is represented by a JS `Date` on the JS side and as an `Instant` on the JVM.
 * Use `toEpochMilliseconds()` or `toISOString()` to use this value from common code.
 */
public expect class LfDate

/** Date-range represented as an array of [LfDate]. */
public typealias LfDateRange = Array<LfDate>

/** Builds as an LF date from the number of milliseconds since the Unix/POSIX epoch. */
public expect fun Long.toLfDate(): LfDate

/** Converts an LF date to the number of milliseconds since the Unix/POSIX epoch. */
public expect fun LfDate.toEpochMilliseconds(): Long

/** Converts an LF date into a string formatted as an ISO-8601 instant. */
public expect fun LfDate.toISOString(): String

/** Converts an LF date to the number of milliseconds since the Unix/POSIX epoch. */
public operator fun LfDate.unaryPlus(): Long = this.toEpochMilliseconds()

/** `LfDate` comparator. */
public operator fun LfDate.compareTo(date: LfDate): Int = (+this).compareTo(+date)

/** Adds two `LfDate`s together. */
public operator fun LfDate.plus(date: LfDate): LfDate = (+this + +date).toLfDate()

/** Subtracts an `LfDate` from this `LfDate`. */
public operator fun LfDate.minus(date: LfDate): LfDate = (+this - +date).toLfDate()

package pt.lightweightform.lfkotlin.computedvalues

import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.Context
import pt.lightweightform.lfkotlin.LfLong
import pt.lightweightform.lfkotlin.Path
import pt.lightweightform.lfkotlin.toLfLong
import pt.lightweightform.lfkotlin.toLong

/**
 * Computed value with the sum of all integer values produced by the [selector] function applied to
 * each element in the array at [dependencyPath].
 */
public open class SumOfInts<T>(
    private val dependencyPath: Path,
    private val selector: (T) -> Int?
) : ComputedValue<Int> {
    override fun Context.compute(): Int = get<Array<T>>(dependencyPath).sumOf { selector(it) ?: 0 }
}

/**
 * Computed value with the sum of all long values produced by the [selector] function applied to
 * each element in the array at [dependencyPath].
 */
public open class SumOfLongs<T>(
    private val dependencyPath: Path,
    private val selector: (T) -> LfLong?
) : ComputedValue<LfLong> {
    override fun Context.compute(): LfLong =
        (get<Array<T>>(dependencyPath).sumOf { selector(it)?.toLong() ?: 0 }).toLfLong()
}

/**
 * Computed value with the sum of all double values produced by the [selector] function applied to
 * each element in the array at [dependencyPath].
 */
public open class SumOfDoubles<T>(
    private val dependencyPath: Path,
    private val selector: (T) -> Double?
) : ComputedValue<Double> {
    override fun Context.compute(): Double =
        get<Array<T>>(dependencyPath).sumOf { selector(it) ?: 0.0 }
}

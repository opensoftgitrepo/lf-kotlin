package pt.lightweightform.lfkotlin

/** Schema validation. */
public interface Validation<in T>

/** Synchronous schema validation. */
public interface SyncValidation<in T> : Validation<T> {
    /**
     * Synchronously validates a value within a context by returning an iterable of found issues.
     */
    public fun Context.validate(value: T): Iterable<Issue>
}

/** Computed "is required" validation. */
public interface IsRequired

/** Synchronous computed "is required" validation. */
public interface SyncIsRequired : IsRequired {
    /** Synchronously verifies whether a value is required within a context. */
    public fun Context.isRequired(): Boolean
}

/** Computed "allowed values" used for validation. */
public interface AllowedValues<out T>

/** Synchronous computed "allowed values" validation. */
public interface SyncAllowedValues<out T> : AllowedValues<T> {
    /** Synchronously returns the allowed values that a schema is allowed to contain. */
    public fun Context.allowedValues(): List<T>?
}

/**
 * Computed "bound" used for validation (`min`, `max`, `minDate`, `maxDate`, `minLength`,
 * `maxLength`, `minSize`, or `maxSize`).
 */
public interface Bound<out T>

/** Synchronous computed "bound" used for validation. */
public interface SyncBound<out T> : Bound<T> {
    /** Synchronously returns a bound that should be respected. */
    public fun Context.bound(): T?
}

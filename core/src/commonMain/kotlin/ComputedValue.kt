package pt.lightweightform.lfkotlin

/** Computed value. */
public interface ComputedValue<out T> {
    /** Compute the computed value within a context. */
    public fun Context.compute(): T
}

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun <T : Any> tableSchemaImpl(
    rowsSchema: ClassSchema<T>,
    isNullable: Boolean,
    initialValue: Array<T>?,
    computedInitialValue: InitialValue<Array<T>?>?,
    computedValue: ComputedValue<Array<T>?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Array<T>>?,
    computedAllowedValues: AllowedValues<Array<T>>?,
    disallowedValueCode: String?,
    minSize: Int?,
    computedMinSize: Bound<Int>?,
    minSizeCode: String?,
    maxSize: Int?,
    computedMaxSize: Bound<Int>?,
    maxSizeCode: String?,
    validations: List<Validation<Array<T>>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Array<T>?>

/** Creates a table schema. Maps to a schema of type "table" in LF. */
@Suppress("UNCHECKED_CAST")
public fun <T : Any> tableSchema(
    rowsSchema: ClassSchema<T>,
    initialValue: Array<T>? = null,
    computedInitialValue: InitialValue<Array<T>>? = null,
    computedValue: ComputedValue<Array<T>>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<Array<T>>? = null,
    computedAllowedValues: AllowedValues<Array<T>>? = null,
    disallowedValueCode: String? = null,
    minSize: Int? = null,
    computedMinSize: Bound<Int>? = null,
    minSizeCode: String? = null,
    maxSize: Int? = null,
    computedMaxSize: Bound<Int>? = null,
    maxSizeCode: String? = null,
    validations: List<Validation<Array<T>>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Array<T>> =
    tableSchemaImpl(
        rowsSchema,
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<Array<T>?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        minSize,
        computedMinSize,
        minSizeCode,
        maxSize,
        computedMaxSize,
        maxSizeCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Array<T>>

/**
 * Creates a nullable table schema. Maps to a schema of type "table" with `isNullable` set to `true`
 * in LF.
 */
public fun <T : Any> nullableTableSchema(
    rowsSchema: ClassSchema<T>,
    initialValue: Array<T>? = null,
    computedInitialValue: InitialValue<Array<T>?>? = null,
    computedValue: ComputedValue<Array<T>?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<Array<T>>? = null,
    computedAllowedValues: AllowedValues<Array<T>>? = null,
    disallowedValueCode: String? = null,
    minSize: Int? = null,
    computedMinSize: Bound<Int>? = null,
    minSizeCode: String? = null,
    maxSize: Int? = null,
    computedMaxSize: Bound<Int>? = null,
    maxSizeCode: String? = null,
    validations: List<Validation<Array<T>>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Array<T>?> =
    tableSchemaImpl(
        rowsSchema,
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        minSize,
        computedMinSize,
        minSizeCode,
        maxSize,
        computedMaxSize,
        maxSizeCode,
        validations,
        initialState,
        extra
    )

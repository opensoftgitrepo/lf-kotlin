package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun tupleSchemaImpl(
    elementsSchemas: List<Schema<Any?>>,
    isNullable: Boolean,
    initialValue: Array<Any?>?,
    computedInitialValue: InitialValue<Array<Any?>?>?,
    computedValue: ComputedValue<Array<Any?>?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Array<Any?>>?,
    computedAllowedValues: AllowedValues<Array<Any?>>?,
    disallowedValueCode: String?,
    validations: List<Validation<Array<Any?>>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Array<Any?>?>

/** Creates a tuple schema. Maps to a schema of type "tuple" in LF. */
@Suppress("UNCHECKED_CAST")
public fun tupleSchema(
    elementsSchemas: List<Schema<Any?>>,
    initialValue: Array<Any?>? = null,
    computedInitialValue: InitialValue<Array<Any?>>? = null,
    computedValue: ComputedValue<Array<Any?>>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<Array<Any?>>? = null,
    computedAllowedValues: AllowedValues<Array<Any?>>? = null,
    disallowedValueCode: String? = null,
    validations: List<Validation<Array<Any?>>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Array<Any?>> =
    tupleSchemaImpl(
        elementsSchemas,
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<Array<Any?>?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Array<Any?>>

/**
 * Creates a nullable tuple schema. Maps to a schema of type "tuple" with `isNullable` set to `true`
 * in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun nullableTupleSchema(
    elementsSchemas: List<Schema<Any?>>,
    initialValue: Array<Any?>? = null,
    computedInitialValue: InitialValue<Array<Any?>?>? = null,
    computedValue: ComputedValue<Array<Any?>?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<Array<Any?>>? = null,
    computedAllowedValues: AllowedValues<Array<Any?>>? = null,
    disallowedValueCode: String? = null,
    validations: List<Validation<Array<Any?>>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Array<Any?>?> =
    tupleSchemaImpl(
        elementsSchemas,
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

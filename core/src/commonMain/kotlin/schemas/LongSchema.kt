package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.LfLong
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun longSchemaImpl(
    isNullable: Boolean,
    initialValue: LfLong?,
    computedInitialValue: InitialValue<LfLong?>?,
    computedValue: ComputedValue<LfLong?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<LfLong>?,
    computedAllowedValues: AllowedValues<LfLong>?,
    disallowedValueCode: String?,
    min: LfLong?,
    computedMin: Bound<LfLong>?,
    minCode: String?,
    max: LfLong?,
    computedMax: Bound<LfLong>?,
    maxCode: String?,
    validations: List<Validation<LfLong>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<LfLong?>

/**
 * Creates a long schema. Maps to a schema of type "number" with `isInteger` set to `true` and
 * appropriate min/max bounds in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun longSchema(
    initialValue: LfLong? = null,
    computedInitialValue: InitialValue<LfLong>? = null,
    computedValue: ComputedValue<LfLong>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<LfLong>? = null,
    computedAllowedValues: AllowedValues<LfLong>? = null,
    disallowedValueCode: String? = null,
    min: LfLong? = null,
    computedMin: Bound<LfLong>? = null,
    minCode: String? = null,
    max: LfLong? = null,
    computedMax: Bound<LfLong>? = null,
    maxCode: String? = null,
    validations: List<Validation<LfLong>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<LfLong> =
    longSchemaImpl(
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<LfLong?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<LfLong>

/**
 * Creates a nullable long schema. Maps to a schema of type "number" with `isNullable` set to
 * `true`, `isInteger` set to `true` and appropriate min/max bounds in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun nullableLongSchema(
    initialValue: LfLong? = null,
    computedInitialValue: InitialValue<LfLong?>? = null,
    computedValue: ComputedValue<LfLong?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<LfLong>? = null,
    computedAllowedValues: AllowedValues<LfLong>? = null,
    disallowedValueCode: String? = null,
    min: LfLong? = null,
    computedMin: Bound<LfLong>? = null,
    minCode: String? = null,
    max: LfLong? = null,
    computedMax: Bound<LfLong>? = null,
    maxCode: String? = null,
    validations: List<Validation<LfLong>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<LfLong?> =
    longSchemaImpl(
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    )

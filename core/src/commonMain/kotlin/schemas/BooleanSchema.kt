package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun booleanSchemaImpl(
    isNullable: Boolean,
    initialValue: Boolean?,
    computedInitialValue: InitialValue<Boolean?>?,
    computedValue: ComputedValue<Boolean?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Boolean>?,
    computedAllowedValues: AllowedValues<Boolean>?,
    disallowedValueCode: String?,
    validations: List<Validation<Boolean>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Boolean?>

/** Creates a boolean schema. Maps to a schema of type "boolean" in LF. */
@Suppress("UNCHECKED_CAST")
public fun booleanSchema(
    initialValue: Boolean? = null,
    computedInitialValue: InitialValue<Boolean>? = null,
    computedValue: ComputedValue<Boolean>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<Boolean>? = null,
    computedAllowedValues: AllowedValues<Boolean>? = null,
    disallowedValueCode: String? = null,
    validations: List<Validation<Boolean>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Boolean> =
    booleanSchemaImpl(
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<Boolean?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Boolean>

/**
 * Creates a nullable boolean schema. Maps to a schema of type "boolean" with `isNullable` set to
 * `true` in LF.
 */
public fun nullableBooleanSchema(
    initialValue: Boolean? = null,
    computedInitialValue: InitialValue<Boolean?>? = null,
    computedValue: ComputedValue<Boolean?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<Boolean>? = null,
    computedAllowedValues: AllowedValues<Boolean>? = null,
    disallowedValueCode: String? = null,
    validations: List<Validation<Boolean>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Boolean?> =
    booleanSchemaImpl(
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )

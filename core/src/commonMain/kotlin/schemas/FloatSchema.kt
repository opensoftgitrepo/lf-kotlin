package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun floatSchemaImpl(
    isNullable: Boolean,
    representsInteger: Boolean,
    initialValue: Float?,
    computedInitialValue: InitialValue<Float?>?,
    computedValue: ComputedValue<Float?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Float>?,
    computedAllowedValues: AllowedValues<Float>?,
    disallowedValueCode: String?,
    min: Float?,
    computedMin: Bound<Float>?,
    minCode: String?,
    max: Float?,
    computedMax: Bound<Float>?,
    maxCode: String?,
    validations: List<Validation<Float>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Float?>

/** Creates a float schema. Maps to a schema of type "number" in LF. */
@Suppress("UNCHECKED_CAST")
public fun floatSchema(
    representsInteger: Boolean = false,
    initialValue: Float? = null,
    computedInitialValue: InitialValue<Float>? = null,
    computedValue: ComputedValue<Float>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<Float>? = null,
    computedAllowedValues: AllowedValues<Float>? = null,
    disallowedValueCode: String? = null,
    min: Float? = null,
    computedMin: Bound<Float>? = null,
    minCode: String? = null,
    max: Float? = null,
    computedMax: Bound<Float>? = null,
    maxCode: String? = null,
    validations: List<Validation<Float>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Float> =
    floatSchemaImpl(
        false,
        representsInteger,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<Float?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Float>

/**
 * Creates a nullable float schema. Maps to a schema of type "number" with `isNullable` set to
 * `true` in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun nullableFloatSchema(
    representsInteger: Boolean = false,
    initialValue: Float? = null,
    computedInitialValue: InitialValue<Float?>? = null,
    computedValue: ComputedValue<Float?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<Float>? = null,
    computedAllowedValues: AllowedValues<Float>? = null,
    disallowedValueCode: String? = null,
    min: Float? = null,
    computedMin: Bound<Float>? = null,
    minCode: String? = null,
    max: Float? = null,
    computedMax: Bound<Float>? = null,
    maxCode: String? = null,
    validations: List<Validation<Float>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Float?> =
    floatSchemaImpl(
        true,
        representsInteger,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    )

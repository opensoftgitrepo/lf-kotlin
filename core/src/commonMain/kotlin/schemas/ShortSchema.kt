package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun shortSchemaImpl(
    isNullable: Boolean,
    initialValue: Short?,
    computedInitialValue: InitialValue<Short?>?,
    computedValue: ComputedValue<Short?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Short>?,
    computedAllowedValues: AllowedValues<Short>?,
    disallowedValueCode: String?,
    min: Short?,
    computedMin: Bound<Short>?,
    minCode: String?,
    max: Short?,
    computedMax: Bound<Short>?,
    maxCode: String?,
    validations: List<Validation<Short>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Short?>

/**
 * Creates a short schema. Maps to a schema of type "number" with `isInteger` set to `true` and
 * appropriate min/max bounds in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun shortSchema(
    initialValue: Short? = null,
    computedInitialValue: InitialValue<Short>? = null,
    computedValue: ComputedValue<Short>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<Short>? = null,
    computedAllowedValues: AllowedValues<Short>? = null,
    disallowedValueCode: String? = null,
    min: Short? = null,
    computedMin: Bound<Short>? = null,
    minCode: String? = null,
    max: Short? = null,
    computedMax: Bound<Short>? = null,
    maxCode: String? = null,
    validations: List<Validation<Short>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Short> =
    shortSchemaImpl(
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<Short?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Short>

/**
 * Creates a nullable short schema. Maps to a schema of type "number" with `isNullable` set to
 * `true`, `isInteger` set to `true` and appropriate min/max bounds in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun nullableShortSchema(
    initialValue: Short? = null,
    computedInitialValue: InitialValue<Short?>? = null,
    computedValue: ComputedValue<Short?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<Short>? = null,
    computedAllowedValues: AllowedValues<Short>? = null,
    disallowedValueCode: String? = null,
    min: Short? = null,
    computedMin: Bound<Short>? = null,
    minCode: String? = null,
    max: Short? = null,
    computedMax: Bound<Short>? = null,
    maxCode: String? = null,
    validations: List<Validation<Short>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Short?> =
    shortSchemaImpl(
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    )

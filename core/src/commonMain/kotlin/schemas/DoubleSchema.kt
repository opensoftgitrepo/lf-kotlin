package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun doubleSchemaImpl(
    isNullable: Boolean,
    representsInteger: Boolean,
    initialValue: Double?,
    computedInitialValue: InitialValue<Double?>?,
    computedValue: ComputedValue<Double?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Double>?,
    computedAllowedValues: AllowedValues<Double>?,
    disallowedValueCode: String?,
    min: Double?,
    computedMin: Bound<Double>?,
    minCode: String?,
    max: Double?,
    computedMax: Bound<Double>?,
    maxCode: String?,
    validations: List<Validation<Double>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Double?>

/** Creates a double schema. Maps to a schema of type "number" in LF. */
@Suppress("UNCHECKED_CAST")
public fun doubleSchema(
    representsInteger: Boolean = false,
    initialValue: Double? = null,
    computedInitialValue: InitialValue<Double>? = null,
    computedValue: ComputedValue<Double>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<Double>? = null,
    computedAllowedValues: AllowedValues<Double>? = null,
    disallowedValueCode: String? = null,
    min: Double? = null,
    computedMin: Bound<Double>? = null,
    minCode: String? = null,
    max: Double? = null,
    computedMax: Bound<Double>? = null,
    maxCode: String? = null,
    validations: List<Validation<Double>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Double> =
    doubleSchemaImpl(
        false,
        representsInteger,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<Double?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Double>

/**
 * Creates a nullable double schema. Maps to a schema of type "number" with `isNullable` set to
 * `true` in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun nullableDoubleSchema(
    representsInteger: Boolean = false,
    initialValue: Double? = null,
    computedInitialValue: InitialValue<Double?>? = null,
    computedValue: ComputedValue<Double?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<Double>? = null,
    computedAllowedValues: AllowedValues<Double>? = null,
    disallowedValueCode: String? = null,
    min: Double? = null,
    computedMin: Bound<Double>? = null,
    minCode: String? = null,
    max: Double? = null,
    computedMax: Bound<Double>? = null,
    maxCode: String? = null,
    validations: List<Validation<Double>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Double?> =
    doubleSchemaImpl(
        true,
        representsInteger,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    )

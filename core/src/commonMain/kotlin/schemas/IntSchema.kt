package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun intSchemaImpl(
    isNullable: Boolean,
    initialValue: Int?,
    computedInitialValue: InitialValue<Int?>?,
    computedValue: ComputedValue<Int?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Int>?,
    computedAllowedValues: AllowedValues<Int>?,
    disallowedValueCode: String?,
    min: Int?,
    computedMin: Bound<Int>?,
    minCode: String?,
    max: Int?,
    computedMax: Bound<Int>?,
    maxCode: String?,
    validations: List<Validation<Int>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Int?>

/**
 * Creates a int schema. Maps to a schema of type "number" with `isInteger` set to `true` and
 * appropriate min/max bounds in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun intSchema(
    initialValue: Int? = null,
    computedInitialValue: InitialValue<Int>? = null,
    computedValue: ComputedValue<Int>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<Int>? = null,
    computedAllowedValues: AllowedValues<Int>? = null,
    disallowedValueCode: String? = null,
    min: Int? = null,
    computedMin: Bound<Int>? = null,
    minCode: String? = null,
    max: Int? = null,
    computedMax: Bound<Int>? = null,
    maxCode: String? = null,
    validations: List<Validation<Int>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Int> =
    intSchemaImpl(
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<Int?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Int>

/**
 * Creates a nullable int schema. Maps to a schema of type "number" with `isNullable` set to `true`,
 * `isInteger` set to `true` and appropriate min/max bounds in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun nullableIntSchema(
    initialValue: Int? = null,
    computedInitialValue: InitialValue<Int?>? = null,
    computedValue: ComputedValue<Int?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<Int>? = null,
    computedAllowedValues: AllowedValues<Int>? = null,
    disallowedValueCode: String? = null,
    min: Int? = null,
    computedMin: Bound<Int>? = null,
    minCode: String? = null,
    max: Int? = null,
    computedMax: Bound<Int>? = null,
    maxCode: String? = null,
    validations: List<Validation<Int>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Int?> =
    intSchemaImpl(
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    )

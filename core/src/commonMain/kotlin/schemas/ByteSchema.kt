package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun byteSchemaImpl(
    isNullable: Boolean,
    initialValue: Byte?,
    computedInitialValue: InitialValue<Byte?>?,
    computedValue: ComputedValue<Byte?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Byte>?,
    computedAllowedValues: AllowedValues<Byte>?,
    disallowedValueCode: String?,
    min: Byte?,
    computedMin: Bound<Byte>?,
    minCode: String?,
    max: Byte?,
    computedMax: Bound<Byte>?,
    maxCode: String?,
    validations: List<Validation<Byte>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Byte?>

/**
 * Creates a byte schema. Maps to a schema of type "number" with `isInteger` set to `true` and
 * appropriate min/max bounds in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun byteSchema(
    initialValue: Byte? = null,
    computedInitialValue: InitialValue<Byte>? = null,
    computedValue: ComputedValue<Byte>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<Byte>? = null,
    computedAllowedValues: AllowedValues<Byte>? = null,
    disallowedValueCode: String? = null,
    min: Byte? = null,
    computedMin: Bound<Byte>? = null,
    minCode: String? = null,
    max: Byte? = null,
    computedMax: Bound<Byte>? = null,
    maxCode: String? = null,
    validations: List<Validation<Byte>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Byte> =
    byteSchemaImpl(
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<Byte?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Byte>

/**
 * Creates a nullable byte schema. Maps to a schema of type "number" with `isNullable` set to
 * `true`, `isInteger` set to `true` and appropriate min/max bounds in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun nullableByteSchema(
    initialValue: Byte? = null,
    computedInitialValue: InitialValue<Byte?>? = null,
    computedValue: ComputedValue<Byte?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<Byte>? = null,
    computedAllowedValues: AllowedValues<Byte>? = null,
    disallowedValueCode: String? = null,
    min: Byte? = null,
    computedMin: Bound<Byte>? = null,
    minCode: String? = null,
    max: Byte? = null,
    computedMax: Bound<Byte>? = null,
    maxCode: String? = null,
    validations: List<Validation<Byte>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Byte?> =
    byteSchemaImpl(
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    )

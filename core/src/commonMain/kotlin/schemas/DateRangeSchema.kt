package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.Context
import pt.lightweightform.lfkotlin.DATE_OUT_OF_BOUNDS_CODE
import pt.lightweightform.lfkotlin.DATE_OUT_OF_BOUNDS_TYPE
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Issue
import pt.lightweightform.lfkotlin.LfDate
import pt.lightweightform.lfkotlin.LfDateRange
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.StateProperty
import pt.lightweightform.lfkotlin.SyncBound
import pt.lightweightform.lfkotlin.SyncStateProperty
import pt.lightweightform.lfkotlin.SyncValidation
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.compareTo
import pt.lightweightform.lfkotlin.objectOf
import pt.lightweightform.lfkotlin.toISOString

/** Default issue code emitted when a date-range is invalid. */
public const val INVALID_DATE_RANGE_CODE: String = "LF_INVALID_DATE_RANGE"
/** Issue type used to represent that a date-range is invalid. */
public const val INVALID_DATE_RANGE_TYPE: String = "invalidDateRange"

@Suppress("UNCHECKED_CAST")
internal fun dateRangeSchemaImpl(
    isNullable: Boolean,
    initialValue: LfDateRange?,
    computedInitialValue: InitialValue<LfDateRange?>?,
    computedValue: ComputedValue<LfDateRange?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    skipValidations: Boolean,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<LfDateRange>?,
    computedAllowedValues: AllowedValues<LfDateRange>?,
    disallowedValueCode: String?,
    invalidDateRangeCode: String?,
    minDate: LfDate?,
    computedMinDate: Bound<LfDate>?,
    minDateCode: String?,
    maxDate: LfDate?,
    computedMaxDate: Bound<LfDate>?,
    maxDateCode: String?,
    validations: List<Validation<LfDateRange>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<LfDateRange?> {
    val state = HashMap(initialState ?: mapOf())

    // Skip adding validations when requested (because e.g. the date-range is
    // computed)
    var dateRangeValidations = validations
    if (!skipValidations) {
        // Set `minDate` and `maxDate` as data properties in the date-range.
        if (minDate != null) {
            state["minDate"] = minDate
        } else if (computedMinDate != null) {
            state["minDate"] = statePropertyFromDateBound(computedMinDate)
        }
        if (maxDate != null) {
            state["maxDate"] = maxDate
        } else if (computedMaxDate != null) {
            state["maxDate"] = statePropertyFromDateBound(computedMaxDate)
        }

        // Add date-range validation
        dateRangeValidations =
            listOf(DateRangeValidation(invalidDateRangeCode, minDateCode, maxDateCode)) +
                (dateRangeValidations ?: emptyList())
    }

    return tupleSchemaImpl(
        listOf(dateSchema(), dateSchema()) as List<Schema<Any?>>,
        isNullable,
        initialValue?.also {
            if (it.size != 2) {
                throw IllegalArgumentException("Initial value must be an array with 2 elements.")
            }
        } as
            Array<Any?>?,
        computedInitialValue as InitialValue<Array<Any?>?>?,
        computedValue as ComputedValue<Array<Any?>?>?,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues as List<Array<Any?>>?,
        computedAllowedValues as AllowedValues<Array<Any?>>?,
        disallowedValueCode,
        dateRangeValidations as List<Validation<Array<Any?>>>?,
        state,
        extra
    ) as
        Schema<LfDateRange?>
}

/**
 * Builds a date state property from a date bound. This state property is used by LF to query the
 * date bound of an LF date-range component and is used by the date bound validations.
 */
private fun statePropertyFromDateBound(computedDate: Bound<LfDate>): StateProperty<LfDate?> =
    when (computedDate) {
        is SyncBound<LfDate> ->
            object : SyncStateProperty<LfDate?> {
                override fun Context.property(): LfDate? =
                    computedDate.run { relativeContext("..").bound() }
            }
        else -> error("Unsupported `Bound` implementation")
    }

/** Date-range validation. */
private class DateRangeValidation(
    private val invalidDateRangeCode: String?,
    private val minDateCode: String?,
    private val maxDateCode: String?
) : SyncValidation<LfDateRange> {
    override fun Context.validate(value: LfDateRange): Iterable<Issue> = buildList {
        if (value[0] > value[1]) {
            add(
                Issue(
                    invalidDateRangeCode ?: INVALID_DATE_RANGE_CODE,
                    data = objectOf("type" to INVALID_DATE_RANGE_TYPE)
                )
            )
            return@buildList
        }

        val minDate: LfDate? = getStateProperty(".", "minDate")
        val maxDate: LfDate? = getStateProperty(".", "maxDate")
        val code =
            if (minDate != null && value[0] < minDate) minDateCode ?: DATE_OUT_OF_BOUNDS_CODE
            else if (maxDate != null && value[1] > maxDate) maxDateCode ?: DATE_OUT_OF_BOUNDS_CODE
            else null
        if (code != null) {
            add(
                Issue(
                    code,
                    data =
                        buildMap<String, Any?> {
                            put("type", DATE_OUT_OF_BOUNDS_TYPE)
                            put(
                                "value",
                                value.joinToString(separator = " - ") { date -> date.toISOString() }
                            )
                            if (minDate != null) put("minDate", minDate.toISOString())
                            if (maxDate != null) put("maxDate", maxDate.toISOString())
                        }
                )
            )
        }
    }
}

/**
 * Creates a date-range schema. Maps to a schema of type "tuple" with two inner schemas of type
 * "date" in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun dateRangeSchema(
    initialValue: LfDateRange? = null,
    computedInitialValue: InitialValue<LfDateRange>? = null,
    computedValue: ComputedValue<LfDateRange>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    skipValidations: Boolean = false,
    allowedValues: List<LfDateRange>? = null,
    computedAllowedValues: AllowedValues<LfDateRange>? = null,
    disallowedValueCode: String? = null,
    invalidDateRangeCode: String? = null,
    minDate: LfDate? = null,
    computedMinDate: Bound<LfDate>? = null,
    minDateCode: String? = null,
    maxDate: LfDate? = null,
    computedMaxDate: Bound<LfDate>? = null,
    maxDateCode: String? = null,
    validations: List<Validation<LfDateRange>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<LfDateRange> =
    dateRangeSchemaImpl(
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<LfDateRange?>?,
        mismatchedComputedCode,
        isClientOnly,
        skipValidations,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        invalidDateRangeCode,
        minDate,
        computedMinDate,
        minDateCode,
        maxDate,
        computedMaxDate,
        maxDateCode,
        validations,
        initialState,
        extra
    ) as
        Schema<LfDateRange>

/**
 * Creates a nullable date-range schema. Maps to a schema of type "tuple" with `isNullable` set to
 * `true` and two inner schemas of type "date" in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun nullableDateRangeSchema(
    initialValue: LfDateRange? = null,
    computedInitialValue: InitialValue<LfDateRange?>? = null,
    computedValue: ComputedValue<LfDateRange?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    skipValidations: Boolean = false,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<LfDateRange>? = null,
    computedAllowedValues: AllowedValues<LfDateRange>? = null,
    disallowedValueCode: String? = null,
    invalidDateRangeCode: String? = null,
    minDate: LfDate? = null,
    computedMinDate: Bound<LfDate>? = null,
    minDateCode: String? = null,
    maxDate: LfDate? = null,
    computedMaxDate: Bound<LfDate>? = null,
    maxDateCode: String? = null,
    validations: List<Validation<LfDateRange>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<LfDateRange?> =
    dateRangeSchemaImpl(
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        skipValidations,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        invalidDateRangeCode,
        minDate,
        computedMinDate,
        minDateCode,
        maxDate,
        computedMaxDate,
        maxDateCode,
        validations,
        initialState,
        extra
    )

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

/**
 * Creates a schema from an enum's value names. Maps to a schema of type "string" in LF where the
 * allowed values are the names of the enum's values.
 *
 * The [initialValue] defaults to the first enum value's name.
 */
public inline fun <reified T : Enum<T>> enumNameSchema(
    initialValue: String? = null,
    computedInitialValue: InitialValue<String>? = null,
    computedValue: ComputedValue<String>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    disallowedValueCode: String? = null,
    validations: List<Validation<String>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<String> =
    stringSchema(
        initialValue =
            if (initialValue == null && computedInitialValue == null) enumValues<T>().first().name
            else initialValue,
        computedInitialValue = computedInitialValue,
        computedValue = computedValue,
        mismatchedComputedCode = mismatchedComputedCode,
        isClientOnly = isClientOnly,
        allowedValues = enumValues<T>().map { value -> value.name },
        disallowedValueCode = disallowedValueCode,
        validations = validations,
        initialState = initialState,
        extra = extra
    )

/**
 * Creates a nullable schema from an enum's value names. Maps to a schema of type "string" with
 * `isNullable` set to `true` in LF where the allowed values are the names of the enum's values.
 */
public inline fun <reified T : Enum<T>> nullableEnumNameSchema(
    initialValue: String? = null,
    computedInitialValue: InitialValue<String?>? = null,
    computedValue: ComputedValue<String?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    disallowedValueCode: String? = null,
    validations: List<Validation<String>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<String?> =
    nullableStringSchema(
        initialValue = initialValue,
        computedInitialValue = computedInitialValue,
        computedValue = computedValue,
        mismatchedComputedCode = mismatchedComputedCode,
        isClientOnly = isClientOnly,
        isRequired = isRequired,
        computedIsRequired = computedIsRequired,
        isRequiredCode = isRequiredCode,
        allowedValues = enumValues<T>().map { value -> value.name },
        disallowedValueCode = disallowedValueCode,
        validations = validations,
        initialState = initialState,
        extra = extra
    )

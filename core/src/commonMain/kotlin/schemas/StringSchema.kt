package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun stringSchemaImpl(
    isNullable: Boolean,
    initialValue: String?,
    computedInitialValue: InitialValue<String?>? = null,
    computedValue: ComputedValue<String?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<String>?,
    computedAllowedValues: AllowedValues<String>?,
    disallowedValueCode: String?,
    minLength: Int?,
    computedMinLength: Bound<Int>?,
    minLengthCode: String?,
    maxLength: Int?,
    computedMaxLength: Bound<Int>?,
    maxLengthCode: String?,
    validations: List<Validation<String>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<String?>

/** Creates a string schema. Maps to a schema of type "string" in LF. */
@Suppress("UNCHECKED_CAST")
public fun stringSchema(
    initialValue: String? = null,
    computedInitialValue: InitialValue<String>? = null,
    computedValue: ComputedValue<String>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<String>? = null,
    computedAllowedValues: AllowedValues<String>? = null,
    disallowedValueCode: String? = null,
    minLength: Int? = null,
    computedMinLength: Bound<Int>? = null,
    minLengthCode: String? = null,
    maxLength: Int? = null,
    computedMaxLength: Bound<Int>? = null,
    maxLengthCode: String? = null,
    validations: List<Validation<String>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<String> =
    stringSchemaImpl(
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<String?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        minLength,
        computedMinLength,
        minLengthCode,
        maxLength,
        computedMaxLength,
        maxLengthCode,
        validations,
        initialState,
        extra
    ) as
        Schema<String>

/**
 * Creates a nullable string schema. Maps to a schema of type "string" with `isNullable` set to
 * `true` in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun nullableStringSchema(
    initialValue: String? = null,
    computedInitialValue: InitialValue<String?>? = null,
    computedValue: ComputedValue<String?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<String>? = null,
    computedAllowedValues: AllowedValues<String>? = null,
    disallowedValueCode: String? = null,
    minLength: Int? = null,
    computedMinLength: Bound<Int>? = null,
    minLengthCode: String? = null,
    maxLength: Int? = null,
    computedMaxLength: Bound<Int>? = null,
    maxLengthCode: String? = null,
    validations: List<Validation<String>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<String?> =
    stringSchemaImpl(
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        minLength,
        computedMinLength,
        minLengthCode,
        maxLength,
        computedMaxLength,
        maxLengthCode,
        validations,
        initialState,
        extra
    )

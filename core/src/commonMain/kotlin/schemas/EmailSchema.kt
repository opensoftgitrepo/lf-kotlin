@file:OptIn(ExperimentalJsExport::class, ExperimentalStdlibApi::class)

package pt.lightweightform.lfkotlin.schemas

import kotlin.js.ExperimentalJsExport
import kotlin.js.JsExport
import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.Context
import pt.lightweightform.lfkotlin.IS_REQUIRED_CODE
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Issue
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.SyncValidation
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.objectOf
import pt.lightweightform.lfkotlin.util.isRequiredToBound

/** Default issue code emitted when an email is invalid. */
@JsExport public val INVALID_EMAIL_CODE: String = "LF_INVALID_EMAIL"

/** Issue type representing an invalid email. */
@JsExport public val INVALID_EMAIL_TYPE: String = "invalidEmail"

/**
 * W3C HTML5 spec email validation regex:
 * https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address
 */
public val EMAIL_REGEX: Regex =
    Regex(
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?" +
            "(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    )

/**
 * Creates an email schema on top of a string schema.
 *
 * Contains a validation that checks whether the provided email is valid according to [emailRegex]
 * which defaults to [EMAIL_REGEX].
 *
 * Set [invalidEmailCode] to specify the issue code emitted for invalid emails.
 */
public fun emailSchema(
    initialValue: String? = null,
    computedInitialValue: InitialValue<String>? = null,
    computedValue: ComputedValue<String>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    skipValidations: Boolean = false,
    allowedValues: List<String>? = null,
    computedAllowedValues: AllowedValues<String>? = null,
    disallowedValueCode: String? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    maxLength: Int? = null,
    computedMaxLength: Bound<Int>? = null,
    maxLengthCode: String? = null,
    emailRegex: Regex? = null,
    invalidEmailCode: String? = null,
    validations: List<Validation<String>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<String> =
    stringSchema(
        initialValue = initialValue,
        computedInitialValue = computedInitialValue,
        computedValue = computedValue,
        mismatchedComputedCode = mismatchedComputedCode,
        isClientOnly = isClientOnly,
        allowedValues = allowedValues,
        computedAllowedValues = computedAllowedValues,
        disallowedValueCode = disallowedValueCode,
        minLength = if (isRequired == true) 1 else null,
        computedMinLength =
            if (computedIsRequired != null) isRequiredToBound(computedIsRequired, 1) else null,
        minLengthCode = isRequiredCode ?: IS_REQUIRED_CODE,
        maxLength = maxLength,
        computedMaxLength = computedMaxLength,
        maxLengthCode = maxLengthCode,
        validations =
            (if (skipValidations) emptyList()
            else listOf(EmailValidation(invalidEmailCode, emailRegex))) +
                (validations ?: emptyList()),
        initialState = initialState,
        extra = extra
    )

/**
 * Email validation that checks whether the email is valid according to [emailRegex] (when it isn't
 * empty).
 */
public class EmailValidation(
    private val invalidEmailCode: String? = null,
    emailRegex: Regex? = null,
) : SyncValidation<String> {
    private val emailRegex: Regex = emailRegex ?: EMAIL_REGEX

    override fun Context.validate(value: String): Iterable<Issue> = buildList {
        if (value.isEmpty()) {
            return@buildList
        }

        if (!emailRegex.matches(value)) {
            add(
                Issue(
                    invalidEmailCode ?: INVALID_EMAIL_CODE,
                    data = objectOf("type" to INVALID_EMAIL_TYPE)
                )
            )
        }
    }
}

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.LfDate
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

internal expect fun dateSchemaImpl(
    isNullable: Boolean,
    initialValue: LfDate?,
    computedInitialValue: InitialValue<LfDate?>?,
    computedValue: ComputedValue<LfDate?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<LfDate>?,
    computedAllowedValues: AllowedValues<LfDate>?,
    disallowedValueCode: String?,
    minDate: LfDate?,
    computedMinDate: Bound<LfDate>?,
    minDateCode: String?,
    maxDate: LfDate?,
    computedMaxDate: Bound<LfDate>?,
    maxDateCode: String?,
    validations: List<Validation<LfDate>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<LfDate?>

/** Creates a date schema. Maps to a schema of type "date" in LF. */
@Suppress("UNCHECKED_CAST")
public fun dateSchema(
    initialValue: LfDate? = null,
    computedInitialValue: InitialValue<LfDate>? = null,
    computedValue: ComputedValue<LfDate>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<LfDate>? = null,
    computedAllowedValues: AllowedValues<LfDate>? = null,
    disallowedValueCode: String? = null,
    minDate: LfDate? = null,
    computedMinDate: Bound<LfDate>? = null,
    minDateCode: String? = null,
    maxDate: LfDate? = null,
    computedMaxDate: Bound<LfDate>? = null,
    maxDateCode: String? = null,
    validations: List<Validation<LfDate>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<LfDate> =
    dateSchemaImpl(
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<LfDate?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        minDate,
        computedMinDate,
        minDateCode,
        maxDate,
        computedMaxDate,
        maxDateCode,
        validations,
        initialState,
        extra
    ) as
        Schema<LfDate>

/**
 * Creates a nullable date schema. Maps to a schema of type "date" with `isNullable` set to `true`
 * in LF.
 */
@Suppress("UNCHECKED_CAST")
public fun nullableDateSchema(
    initialValue: LfDate? = null,
    computedInitialValue: InitialValue<LfDate?>? = null,
    computedValue: ComputedValue<LfDate?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<LfDate>? = null,
    computedAllowedValues: AllowedValues<LfDate>? = null,
    disallowedValueCode: String? = null,
    minDate: LfDate? = null,
    computedMinDate: Bound<LfDate>? = null,
    minDateCode: String? = null,
    maxDate: LfDate? = null,
    computedMaxDate: Bound<LfDate>? = null,
    maxDateCode: String? = null,
    validations: List<Validation<LfDate>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<LfDate?> =
    dateSchemaImpl(
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        minDate,
        computedMinDate,
        minDateCode,
        maxDate,
        computedMaxDate,
        maxDateCode,
        validations,
        initialState,
        extra
    )

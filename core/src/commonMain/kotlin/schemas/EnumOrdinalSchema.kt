package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

/**
 * Creates a schema from an enum's value ordinals. Maps to a schema of type "number" with
 * `isInteger` set to `true` in LF where the allowed values are the ordinals of the enum's values.
 *
 * The [initialValue] defaults to the first enum value's ordinal.
 */
public inline fun <reified T : Enum<T>> enumOrdinalSchema(
    initialValue: Int? = null,
    computedInitialValue: InitialValue<Int>? = null,
    computedValue: ComputedValue<Int>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    disallowedValueCode: String? = null,
    validations: List<Validation<Int>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Int> =
    intSchema(
        initialValue =
            if (initialValue == null && computedInitialValue == null)
                enumValues<T>().first().ordinal
            else initialValue,
        computedInitialValue = computedInitialValue,
        computedValue = computedValue,
        mismatchedComputedCode = mismatchedComputedCode,
        isClientOnly = isClientOnly,
        allowedValues = enumValues<T>().map { value -> value.ordinal },
        disallowedValueCode = disallowedValueCode,
        validations = validations,
        initialState = initialState,
        extra = extra
    )

/**
 * Creates a nullable schema from an enum's value ordinals. Maps to a schema of type "number" with
 * `isInteger` set to `true`, and isNullable` set to `true` in LF where the allowed values are the
 * ordinals of the enum's values.
 */
public inline fun <reified T : Enum<T>> nullableEnumOrdinalSchema(
    initialValue: Int? = null,
    computedInitialValue: InitialValue<Int?>? = null,
    computedValue: ComputedValue<Int?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    disallowedValueCode: String? = null,
    validations: List<Validation<Int>>? = null,
    initialState: Map<String, Any?>? = null,
    extra: Map<String, Any?>? = null
): Schema<Int?> =
    nullableIntSchema(
        initialValue = initialValue,
        computedInitialValue = computedInitialValue,
        computedValue = computedValue,
        mismatchedComputedCode = mismatchedComputedCode,
        isClientOnly = isClientOnly,
        isRequired = isRequired,
        computedIsRequired = computedIsRequired,
        isRequiredCode = isRequiredCode,
        allowedValues = enumValues<T>().map { value -> value.ordinal },
        disallowedValueCode = disallowedValueCode,
        validations = validations,
        initialState = initialState,
        extra = extra
    )

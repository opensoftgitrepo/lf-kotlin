@file:OptIn(ExperimentalTypeInference::class)

package pt.lightweightform.lfkotlin.schemas

import kotlin.experimental.ExperimentalTypeInference
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty1
import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

/** Function responsible for creating an instance of type `T` from a map of arguments. */
public typealias ConstructorFunction<T> = (arguments: Map<String, Any?>) -> T

/**
 * Object holding information about a child of the class schema (class property used to access the
 * child + child schema).
 */
public class ChildInfo<T>(public val prop: KMutableProperty1<T, *>, public val schema: Schema<*>)

/** Interface representing a schema for a class of type [T]. */
public expect interface ClassSchema<T> : Schema<T> {
    public var kClass: KClass<Any>
    public var childInfoByName: Map<String, ChildInfo<T>>
    public var constructorFunction: ConstructorFunction<T>?
}

/**
 * Builder of a class schema. Use [classSchema] or [nullableClassSchema] to build a class schema.
 */
public class ClassSchemaBuilder<T : Any> {
    public val childrenSchemas: MutableMap<KMutableProperty1<T, *>, Schema<*>> = mutableMapOf()

    /** Declare that the child schema of property [property] is [schema]. */
    public fun <C> childSchema(property: KMutableProperty1<T, C>, schema: Schema<C>) {
        childrenSchemas[property] = schema
    }

    /**
     * Declare that the child schema of the receiving property is the one returned by
     * [schemaBuilder].
     */
    public operator fun <C> KMutableProperty1<T, C>.invoke(schemaBuilder: () -> Schema<C>) {
        childSchema(this, schemaBuilder())
    }
}

public expect fun <T : Any> classSchemaImpl(
    kClass: KClass<T>,
    childrenSchemas: Map<KMutableProperty1<T, *>, Schema<*>>,
    constructorFunction: ConstructorFunction<T>?,
    isNullable: Boolean,
    initialValue: T?,
    computedInitialValue: InitialValue<T?>?,
    computedValue: ComputedValue<T?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<T>?,
    computedAllowedValues: AllowedValues<T>?,
    disallowedValueCode: String?,
    validations: List<Validation<T>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): ClassSchema<T?>

/**
 * Creates a schema for a class of type [T]. Maps to a schema of type "record" in LF.
 *
 * Example defining a `PersonSchema` for a class `Person` with `name` and `married` properties:
 * ```kotlin
 * data class Person(var name: String, var married: Boolean)
 *
 * val personSchema = classSchema<Person> {
 *     Person::name { stringSchema() }
 *     Person::married { booleanSchema() }
 * }
 * ```
 *
 * When running validations and computed values, LF-Kotlin converts LF "records" (which are simple
 * JS objects) into proper instances of the Kotlin class being represented by the schema. By
 * default, this conversion occurs by attempting to call a constructor of the class with the value
 * of each child as an argument, in the order they were defined in the schema. I.e., in the example
 * above, LF-Kotlin will attempt to create a `Person` from a JS object `{name: "Pat", married:
 * true}` by calling `Person("Pat", true)`.
 *
 * If the class' constructor has its parameters in a different order or requires other arguments,
 * then the user must provide a [constructorFunction] function, instructing how to properly
 * construct the class.
 */
public inline fun <reified T : Any> classSchema(
    noinline constructorFunction: ConstructorFunction<T>? = null,
    initialValue: T? = null,
    computedInitialValue: InitialValue<T>? = null,
    computedValue: ComputedValue<T>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    allowedValues: List<T>? = null,
    computedAllowedValues: AllowedValues<T>? = null,
    disallowedValueCode: String? = null,
    validations: List<Validation<T>>? = null,
    extra: Map<String, Any?>? = null,
    initialState: Map<String, Any?>? = null,
    @BuilderInference classSchemaBuilder: ClassSchemaBuilder<T>.() -> Unit,
): ClassSchema<T> {
    val builder = ClassSchemaBuilder<T>()
    builder.classSchemaBuilder()
    @Suppress("UNCHECKED_CAST")
    return classSchemaImpl(
        T::class,
        builder.childrenSchemas,
        constructorFunction,
        false,
        initialValue,
        computedInitialValue,
        computedValue as ComputedValue<T?>?,
        mismatchedComputedCode,
        isClientOnly,
        null,
        null,
        null,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    ) as
        ClassSchema<T>
}

/**
 * Creates a nullable schema for a class of type [T]. Maps to a schema of type "record" with
 * `isNullable` set to `true` in LF.
 *
 * Example defining a `PersonSchema` for an optional class `Person` with `name` and `married`
 * properties:
 * ```kotlin
 * data class Person(var name: String, var married: Boolean)
 *
 * val personSchema = nullableClassSchema<Person> {
 *     Person::name { stringSchema() }
 *     Person::married { booleanSchema() }
 * }
 * ```
 *
 * When running validations and computed values, LF-Kotlin converts LF "records" (which are simple
 * JS objects) into proper instances of the Kotlin class being represented by the schema. By
 * default, this conversion occurs by attempting to call a constructor of the class with the value
 * of each child as an argument, in the order they were defined in the schema. I.e., in the example
 * above, LF-Kotlin will attempt to create a `Person` from a JS object `{name: "Pat", married:
 * true}` by calling `Person("Pat", true)`.
 *
 * If the class' constructor has its parameters in a different order or requires other arguments,
 * then the user must provide a [constructorFunction] function, instructing how to properly
 * construct the class.
 */
public inline fun <reified T : Any> nullableClassSchema(
    noinline constructorFunction: ConstructorFunction<T>? = null,
    initialValue: T? = null,
    computedInitialValue: InitialValue<T?>? = null,
    computedValue: ComputedValue<T?>? = null,
    mismatchedComputedCode: String? = null,
    isClientOnly: Boolean? = null,
    isRequired: Boolean? = null,
    computedIsRequired: IsRequired? = null,
    isRequiredCode: String? = null,
    allowedValues: List<T>? = null,
    computedAllowedValues: AllowedValues<T>? = null,
    disallowedValueCode: String? = null,
    validations: List<Validation<T>>? = null,
    extra: Map<String, Any?>? = null,
    initialState: Map<String, Any?>? = null,
    @BuilderInference classSchemaBuilder: ClassSchemaBuilder<T>.() -> Unit,
): ClassSchema<T?> {
    val builder = ClassSchemaBuilder<T>()
    builder.classSchemaBuilder()
    return classSchemaImpl(
        T::class,
        builder.childrenSchemas,
        constructorFunction,
        true,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    )
}

package pt.lightweightform.lfkotlin

import kotlin.js.JsName
import kotlin.jvm.JvmOverloads

/** Validation issue. */
@JsName("Issue")
public data class Issue
@JvmOverloads
constructor(
    /** Code of the issue. Must uniquely identify an issue withing the issues of a certain value. */
    @JsName("code") public val code: String,
    /**
     * Whether this issue represents a warning (`false` by default, meaning that the issue
     * represents an error).
     */
    @JsName("isWarning") public val isWarning: Boolean = false,
    /**
     * Additional issue data. Useful, for example, when displaying internationalised messages about
     * the issue.
     */
    @JsName("data") public val data: Any? = null
) {
    override fun toString(): String = buildString {
        append("Issue(code=$code")
        if (isWarning) {
            append(", isWarning=true")
        }
        if (data != null) {
            append(", data=$data")
        }
        append(")")
    }
}

package pt.lightweightform.lfkotlin.validations

import pt.lightweightform.lfkotlin.Context
import pt.lightweightform.lfkotlin.Issue
import pt.lightweightform.lfkotlin.SyncValidation

/**
 * No-op validation.
 *
 * Useful in scenarios where a certain validation should only run in a specific platform. The actual
 * implementation of said validation in other platforms can extend this class.
 */
public open class NoOpValidation<T> : SyncValidation<T> {
    override fun Context.validate(value: T): Collection<Issue> = emptyList()
}

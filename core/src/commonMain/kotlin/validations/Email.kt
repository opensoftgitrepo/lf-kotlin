package pt.lightweightform.lfkotlin.validations

/**
 * W3C HTML5 spec email validation regex:
 * https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address
 */
public val EMAIL_REGEX: Regex =
    Regex(
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?" +
            "(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    )

/** Checks whether an email is valid. */
public fun emailIsValid(email: String): Boolean = EMAIL_REGEX.matches(email)

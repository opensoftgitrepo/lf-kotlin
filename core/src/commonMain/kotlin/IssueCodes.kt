@file:JvmName("IssueCodes")

package pt.lightweightform.lfkotlin

import kotlin.jvm.JvmName

/** Default issue code emitted when a value that is required is `null`. */
public const val IS_REQUIRED_CODE: String = "LF_IS_REQUIRED"

/** Default issue code emitted when a value isn't one of the defined `allowedValues`. */
public const val DISALLOWED_VALUE_CODE: String = "LF_DISALLOWED_VALUE"

/** Default issue code representing that the date is out of bounds. */
public const val DATE_OUT_OF_BOUNDS_CODE: String = "LF_DATE_OUT_OF_BOUNDS"

/** Default issue code representing that the size is out of bounds. */
public const val SIZE_OUT_OF_BOUNDS_CODE: String = "LF_SIZE_OUT_OF_BOUNDS"

/** Default issue code representing that the number is out of bounds. */
public const val NUMBER_OUT_OF_BOUNDS_CODE: String = "LF_NUMBER_OUT_OF_BOUNDS"

/** Default issue code representing that the string's length is out of bounds. */
public const val LENGTH_OUT_OF_BOUNDS_CODE: String = "LF_LENGTH_OUT_OF_BOUNDS"

/** Default issue code used to represent that a received value does not match the computed value. */
public const val MISMATCHED_COMPUTED_CODE: String = "LF_MISMATCHED_COMPUTED"

package pt.lightweightform.lfkotlin

/** Computed "state property". */
public interface StateProperty<out T>

/** Synchronous computed "state property". */
public interface SyncStateProperty<out T> : StateProperty<T> {
    /** Synchronously returns the value of a state property. */
    public fun Context.property(): T
}

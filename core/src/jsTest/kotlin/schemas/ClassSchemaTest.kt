package pt.lightweightform.lfkotlin.schemas

import kotlin.test.Test
import kotlin.test.assertEquals

class ClassSchemaTest {
    data class TestClass(var x: Int)

    private val schema =
        classSchema(
            initialValue = TestClass(10),
            isClientOnly = true,
            allowedValues = listOf(TestClass(20)),
            disallowedValueCode = "X",
            initialState = mapOf("A" to "B"),
            extra = mapOf("C" to "D")
        ) { TestClass::x { intSchema() } }

    @Test
    fun testPropertiesAreAccessible() {
        val dynSchema = schema.asDynamic()
        assertEquals("record", dynSchema.type)
        assertEquals("number", dynSchema.fieldsSchemas.x.type)
        assertEquals(10, dynSchema.initialValue.x)
        assertEquals(true, dynSchema.isClientOnly)
        assertEquals(20, dynSchema.allowedValues[0].x)
        assertEquals("X", dynSchema.disallowedValueCode)
        assertEquals("B", dynSchema.initialState.A)
        assertEquals("D", dynSchema.C)
    }
}

package pt.lightweightform.lfkotlin.schemas

import kotlin.test.Test
import kotlin.test.assertEquals

class BooleanSchemaTest {
    private val schema =
        booleanSchema(
            initialValue = true,
            isClientOnly = true,
            allowedValues = listOf(true),
            disallowedValueCode = "X",
            initialState = mapOf("A" to "B"),
            extra = mapOf("C" to "D")
        )

    @Test
    fun testPropertiesAreAccessible() {
        val dynSchema = schema.asDynamic()
        assertEquals("boolean", dynSchema.type)
        assertEquals(true, dynSchema.initialValue)
        assertEquals(true, dynSchema.isClientOnly)
        assertEquals(true, dynSchema.allowedValues[0])
        assertEquals("X", dynSchema.disallowedValueCode)
        assertEquals("B", dynSchema.initialState.A)
        assertEquals("D", dynSchema.C)
    }
}

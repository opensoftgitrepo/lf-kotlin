package pt.lightweightform.lfkotlin.schemas

import kotlin.test.Test
import kotlin.test.assertEquals

class StringSchemaTest {
    private val schema =
        stringSchema(
            initialValue = "a",
            isClientOnly = true,
            allowedValues = listOf("b"),
            disallowedValueCode = "X",
            minLength = 2,
            minLengthCode = "M",
            maxLength = 4,
            maxLengthCode = "N",
            initialState = mapOf("A" to "B"),
            extra = mapOf("C" to "D")
        )

    @Test
    fun testPropertiesAreAccessible() {
        val dynSchema = schema.asDynamic()
        assertEquals("string", dynSchema.type)
        assertEquals("a", dynSchema.initialValue)
        assertEquals(true, dynSchema.isClientOnly)
        assertEquals("b", dynSchema.allowedValues[0])
        assertEquals("X", dynSchema.disallowedValueCode)
        assertEquals(2, dynSchema.minLength)
        assertEquals("M", dynSchema.minLengthCode)
        assertEquals(4, dynSchema.maxLength)
        assertEquals("N", dynSchema.maxLengthCode)
        assertEquals("B", dynSchema.initialState.A)
        assertEquals("D", dynSchema.C)
    }
}

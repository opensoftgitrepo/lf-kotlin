package pt.lightweightform.lfkotlin.schemas

import kotlin.test.Test
import kotlin.test.assertEquals

class FloatSchemaTest {
    private val schema =
        floatSchema(
            representsInteger = true,
            initialValue = 1f,
            isClientOnly = true,
            allowedValues = listOf(5f),
            disallowedValueCode = "X",
            min = 2f,
            minCode = "M",
            max = 4f,
            maxCode = "N",
            initialState = mapOf("A" to "B"),
            extra = mapOf("C" to "D")
        )

    @Test
    fun testPropertiesAreAccessible() {
        val dynSchema = schema.asDynamic()
        assertEquals("number", dynSchema.type)
        assertEquals(true, dynSchema.isInteger)
        assertEquals(1f, dynSchema.initialValue)
        assertEquals(true, dynSchema.isClientOnly)
        assertEquals(5f, dynSchema.allowedValues[0])
        assertEquals("X", dynSchema.disallowedValueCode)
        assertEquals(2f, dynSchema.min)
        assertEquals("M", dynSchema.minCode)
        assertEquals(4f, dynSchema.max)
        assertEquals("N", dynSchema.maxCode)
        assertEquals("B", dynSchema.initialState.A)
        assertEquals("D", dynSchema.C)
    }
}

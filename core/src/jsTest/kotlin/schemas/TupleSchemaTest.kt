package pt.lightweightform.lfkotlin.schemas

import kotlin.test.Test
import kotlin.test.assertEquals
import pt.lightweightform.lfkotlin.Schema

class TupleSchemaTest {
    @Suppress("UNCHECKED_CAST")
    private val schema =
        tupleSchema(
            listOf(intSchema()) as List<Schema<Any?>>,
            initialValue = arrayOf(1),
            isClientOnly = true,
            allowedValues = listOf(arrayOf(5)),
            disallowedValueCode = "X",
            initialState = mapOf("A" to "B"),
            extra = mapOf("C" to "D")
        )

    @Test
    fun testPropertiesAreAccessible() {
        val dynSchema = schema.asDynamic()
        assertEquals("tuple", dynSchema.type)
        assertEquals("number", dynSchema.elementsSchemas[0].type)
        assertEquals(1, dynSchema.initialValue[0])
        assertEquals(true, dynSchema.isClientOnly)
        assertEquals(5, dynSchema.allowedValues[0][0])
        assertEquals("X", dynSchema.disallowedValueCode)
        assertEquals("B", dynSchema.initialState.A)
        assertEquals("D", dynSchema.C)
    }
}

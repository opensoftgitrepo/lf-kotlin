package pt.lightweightform.lfkotlin.schemas

import kotlin.test.Test
import kotlin.test.assertEquals

class ArraySchemaTest {
    private val schema =
        arraySchema(
            intSchema(),
            initialValue = arrayOf(1),
            isClientOnly = true,
            allowedValues = listOf(arrayOf(5)),
            disallowedValueCode = "X",
            minSize = 2,
            minSizeCode = "M",
            maxSize = 4,
            maxSizeCode = "N",
            initialState = mapOf("A" to "B"),
            extra = mapOf("C" to "D")
        )

    @Test
    fun testPropertiesAreAccessible() {
        val dynSchema = schema.asDynamic()
        assertEquals("list", dynSchema.type)
        assertEquals("number", dynSchema.elementsSchema.type)
        assertEquals(1, dynSchema.initialValue[0])
        assertEquals(true, dynSchema.isClientOnly)
        assertEquals(5, dynSchema.allowedValues[0][0])
        assertEquals("X", dynSchema.disallowedValueCode)
        assertEquals(2, dynSchema.minSize)
        assertEquals("M", dynSchema.minSizeCode)
        assertEquals(4, dynSchema.maxSize)
        assertEquals("N", dynSchema.maxSizeCode)
        assertEquals("B", dynSchema.initialState.A)
        assertEquals("D", dynSchema.C)
    }
}

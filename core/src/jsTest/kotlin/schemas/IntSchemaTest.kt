package pt.lightweightform.lfkotlin.schemas

import kotlin.test.Test
import kotlin.test.assertEquals

class IntSchemaTest {
    private val schema =
        intSchema(
            initialValue = 1,
            isClientOnly = true,
            allowedValues = listOf(5),
            disallowedValueCode = "X",
            min = 2,
            minCode = "M",
            max = 4,
            maxCode = "N",
            initialState = mapOf("A" to "B"),
            extra = mapOf("C" to "D")
        )

    @Test
    fun testPropertiesAreAccessible() {
        val dynSchema = schema.asDynamic()
        assertEquals("number", dynSchema.type)
        assertEquals(true, dynSchema.isInteger)
        assertEquals(1, dynSchema.initialValue)
        assertEquals(true, dynSchema.isClientOnly)
        assertEquals(5, dynSchema.allowedValues[0])
        assertEquals("X", dynSchema.disallowedValueCode)
        assertEquals(2, dynSchema.min)
        assertEquals("M", dynSchema.minCode)
        assertEquals(4, dynSchema.max)
        assertEquals("N", dynSchema.maxCode)
        assertEquals("B", dynSchema.initialState.A)
        assertEquals("D", dynSchema.C)
    }
}

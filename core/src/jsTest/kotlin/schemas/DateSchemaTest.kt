package pt.lightweightform.lfkotlin.schemas

import kotlin.js.Date
import kotlin.test.Test
import kotlin.test.assertEquals

class DateSchemaTest {
    private val schema =
        dateSchema(
            initialValue = Date(1),
            isClientOnly = true,
            allowedValues = listOf(Date(5)),
            disallowedValueCode = "X",
            minDate = Date(2),
            minDateCode = "M",
            maxDate = Date(4),
            maxDateCode = "N",
            initialState = mapOf("A" to "B"),
            extra = mapOf("C" to "D")
        )

    @Test
    fun testPropertiesAreAccessible() {
        val dynSchema = schema.asDynamic()
        assertEquals("date", dynSchema.type)
        assertEquals(1, dynSchema.initialValue.valueOf())
        assertEquals(true, dynSchema.isClientOnly)
        assertEquals(5, dynSchema.allowedValues[0].valueOf())
        assertEquals("X", dynSchema.disallowedValueCode)
        assertEquals(2, dynSchema.minDate.valueOf())
        assertEquals("M", dynSchema.minDateCode)
        assertEquals(4, dynSchema.maxDate.valueOf())
        assertEquals("N", dynSchema.maxDateCode)
        assertEquals("B", dynSchema.initialState.A)
        assertEquals("D", dynSchema.C)
    }
}

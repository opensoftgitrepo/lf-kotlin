package pt.lightweightform.lfkotlin

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class IssuesMapTest {
    @Test
    fun testHasIssues() {
        assertFalse { emptyIssuesMap().hasIssues() }
        assertTrue { emptyIssuesMap().hasNoIssues() }
        assertFalse { mapOf("/" to emptyList<Issue>()).hasIssues() }
        assertTrue { mapOf("/" to emptyList<Issue>()).hasNoIssues() }
        assertTrue { mapOf("/" to listOf(Issue("X"))).hasIssues() }
        assertFalse { mapOf("/" to listOf(Issue("X"))).hasNoIssues() }
    }

    @Test
    fun testHasErrors() {
        assertFalse { emptyIssuesMap().hasErrors() }
        assertTrue { emptyIssuesMap().hasNoErrors() }
        assertFalse { mapOf("/" to emptyList<Issue>()).hasErrors() }
        assertTrue { mapOf("/" to emptyList<Issue>()).hasNoErrors() }
        assertTrue { mapOf("/" to listOf(Issue("X"))).hasErrors() }
        assertFalse { mapOf("/" to listOf(Issue("X"))).hasNoErrors() }
        assertFalse { mapOf("/" to listOf(Issue("X", isWarning = true))).hasErrors() }
        assertTrue { mapOf("/" to listOf(Issue("X", isWarning = true))).hasNoErrors() }
    }

    @Test
    fun testHasWarnings() {
        assertFalse { emptyIssuesMap().hasWarnings() }
        assertTrue { emptyIssuesMap().hasNoWarnings() }
        assertFalse { mapOf("/" to emptyList<Issue>()).hasWarnings() }
        assertTrue { mapOf("/" to emptyList<Issue>()).hasNoWarnings() }
        assertFalse { mapOf("/" to listOf(Issue("X"))).hasWarnings() }
        assertTrue { mapOf("/" to listOf(Issue("X"))).hasNoWarnings() }
        assertTrue { mapOf("/" to listOf(Issue("X", isWarning = true))).hasWarnings() }
        assertFalse { mapOf("/" to listOf(Issue("X", isWarning = true))).hasNoWarnings() }
    }

    @Test
    fun testEqual() {
        assertTrue { emptyIssuesMap().equal(emptyIssuesMap()) }
        assertTrue { emptyIssuesMap().equal(mapOf("/" to emptyList())) }
        assertTrue { mapOf("/" to emptyList<Issue>()).equal(emptyIssuesMap()) }
        assertFalse { mapOf("/" to listOf(Issue("X"))).equal(emptyIssuesMap()) }
        assertFalse { emptyIssuesMap().equal(mapOf("/" to listOf(Issue("X")))) }
        assertTrue { mapOf("/" to listOf(Issue("X"))).equal(mapOf("/" to listOf(Issue("X")))) }
        assertFalse { mapOf("/" to listOf(Issue("X"))).equal(mapOf("/" to listOf(Issue("Y")))) }
        assertFalse { mapOf("/x" to listOf(Issue("X"))).equal(mapOf("/y" to listOf(Issue("X")))) }
        assertTrue {
            mapOf("/" to listOf(Issue("X"), Issue("Y")))
                .equal(mapOf("/" to listOf(Issue("Y"), Issue("X"))))
        }
        assertFalse {
            mapOf("/" to listOf(Issue("X"), Issue("Y"), Issue("Z")))
                .equal(mapOf("/" to listOf(Issue("Y"), Issue("X"))))
        }
        assertFalse {
            mapOf("/" to listOf(Issue("X"), Issue("Y")))
                .equal(mapOf("/" to listOf(Issue("Z"), Issue("Y"), Issue("X"))))
        }
    }

    @Test
    fun testContains() {
        assertTrue { emptyIssuesMap().contains(emptyIssuesMap()) }
        assertTrue { emptyIssuesMap().contains(mapOf("/" to emptyList())) }
        assertTrue { mapOf("/" to emptyList<Issue>()).contains(emptyIssuesMap()) }
        assertTrue { mapOf("/" to listOf(Issue("X"))).contains(emptyIssuesMap()) }
        assertFalse { emptyIssuesMap().contains(mapOf("/" to listOf(Issue("X")))) }
        assertTrue { mapOf("/" to listOf(Issue("X"))).contains(mapOf("/" to listOf(Issue("X")))) }
        assertFalse { mapOf("/" to listOf(Issue("X"))).contains(mapOf("/" to listOf(Issue("Y")))) }
        assertFalse {
            mapOf("/x" to listOf(Issue("X"))).contains(mapOf("/y" to listOf(Issue("X"))))
        }
        assertTrue {
            mapOf("/" to listOf(Issue("X"), Issue("Y")))
                .contains(mapOf("/" to listOf(Issue("Y"), Issue("X"))))
        }
        assertTrue {
            mapOf("/" to listOf(Issue("X"), Issue("Y"), Issue("Z")))
                .contains(mapOf("/" to listOf(Issue("Y"), Issue("X"))))
        }
        assertFalse {
            mapOf("/" to listOf(Issue("X"), Issue("Y")))
                .contains(mapOf("/" to listOf(Issue("Z"), Issue("Y"), Issue("X"))))
        }
    }

    @Test
    fun testMinus() {
        assertEquals(emptyIssuesMap(), emptyIssuesMap().minus(emptyIssuesMap()))
        assertEquals(emptyIssuesMap(), emptyIssuesMap().minus(mapOf("/" to emptyList())))
        assertEquals(emptyIssuesMap(), mapOf("/" to emptyList<Issue>()).minus(emptyIssuesMap()))
        assertEquals(
            mapOf("/" to listOf(Issue("X"))),
            mapOf("/" to listOf(Issue("X"))).minus(emptyIssuesMap())
        )
        assertEquals(emptyIssuesMap(), emptyIssuesMap().minus(mapOf("/" to listOf(Issue("X")))))
        assertEquals(
            emptyIssuesMap(),
            mapOf("/" to listOf(Issue("X"))).minus(mapOf("/" to listOf(Issue("X"))))
        )
        assertEquals(
            mapOf("/" to listOf(Issue("X"))),
            mapOf("/" to listOf(Issue("X"))).minus(mapOf("/" to listOf(Issue("Y"))))
        )
        assertEquals(
            mapOf("/x" to listOf(Issue("X"))),
            mapOf("/x" to listOf(Issue("X"))).minus(mapOf("/y" to listOf(Issue("X"))))
        )
        assertEquals(
            emptyIssuesMap(),
            mapOf("/" to listOf(Issue("X"), Issue("Y")))
                .minus(mapOf("/" to listOf(Issue("Y"), Issue("X"))))
        )
        assertEquals(
            mapOf("/" to listOf(Issue("Z"))),
            mapOf("/" to listOf(Issue("X"), Issue("Y"), Issue("Z")))
                .minus(mapOf("/" to listOf(Issue("Y"), Issue("X"))))
        )
        assertEquals(
            emptyIssuesMap(),
            mapOf("/" to listOf(Issue("X"), Issue("Y")))
                .minus(mapOf("/" to listOf(Issue("Z"), Issue("Y"), Issue("X"))))
        )
    }
}

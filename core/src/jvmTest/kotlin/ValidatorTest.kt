package pt.lightweightform.lfkotlin

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import pt.lightweightform.lfkotlin.schemas.INVALID_DATE_RANGE_CODE
import pt.lightweightform.lfkotlin.schemas.INVALID_DATE_RANGE_TYPE
import pt.lightweightform.lfkotlin.test.assertMatchingIssues
import pt.lightweightform.lfkotlin.validations.REPEATED_ELEMENTS_CODE
import pt.lightweightform.lfkotlin.validations.REPEATED_ELEMENTS_TYPE

class ValidatorTest {
    @Test
    fun testHasPath() {
        val validator = Validator(exampleValueSchema)
        assertTrue(validator.hasPath("/"))
        assertTrue(validator.hasPath("/string"))
        assertFalse(validator.hasPath("/x"))
        assertFalse(validator.hasPath("/string/x"))
        assertTrue(validator.hasPath("/table/0"))
        assertTrue(validator.hasPath("/table/?"))
        assertTrue(validator.hasPath("/nullableTable/?"))
        assertTrue(validator.hasPath("/arrayOfStrings/?"))
        assertTrue(validator.hasPath("/nullableArrayOfStrings/?"))
        assertTrue(validator.hasPath("/dateRange/1"))
        assertFalse(validator.hasPath("/dateRange/2"))
        assertFalse(validator.hasPath("/table/x"))
        assertTrue(validator.hasPath("/table/1003232"))
        assertFalse(validator.hasPath("/table/${Int.MAX_VALUE - 1}"))
        assertTrue(validator.hasPath("/table/${Int.MAX_VALUE - 2}"))
        assertTrue(validator.hasPath("/table/16/stringCell"))
        assertFalse(validator.hasPath("/table/16/x"))
    }

    @Test
    fun testSchema() {
        val validator = Validator(exampleValueSchema)
        assertEquals(exampleValueSchema, validator.schema("/"))
        assertEquals(exampleTableRowSchema, validator.schema("/table/?"))
    }

    @Test
    fun testHas() {
        val validator = Validator(exampleValueSchema)
        assertFails { validator.has(ExampleValue(), "/x") } // Invalid path throws
        assertTrue(validator.has(ExampleValue(), "/"))
        assertTrue(validator.has(ExampleValue(), "/nullableString"))
        assertFalse(validator.has(ExampleValue(), "/table/0"))
        assertFalse(validator.has(ExampleValue(), "/nullableTable/0"))
        assertTrue(validator.has(ExampleValue(table = arrayOf(ExampleTableRow())), "/table/0"))
        assertTrue(
            validator.has(ExampleValue(table = arrayOf(ExampleTableRow())), "/table/0/stringCell")
        )
    }

    @Test
    fun testGet() {
        val validator = Validator(exampleValueSchema)
        assertFails { validator.get(ExampleValue(), "/x") } // Invalid path throws
        assertFails { validator.get(ExampleValue(), "/table/0") } // No value throws
        assertEquals(null, validator.get(ExampleValue(), "/nullableString"))
        assertEquals(10, validator.get(ExampleValue(int = 10), "/int"))
        assertEquals(
            "abc",
            validator.get(
                ExampleValue(table = arrayOf(ExampleTableRow(stringCell = "abc"))),
                "/table/0/stringCell"
            )
        )
    }

    @Test
    fun testValidation() {
        val validator = Validator(exampleValueSchema)

        // Invalid value ===========================================================================

        val invalidValue =
            ExampleValue(
                nullableBoolean = false,
                nullableDateRange = arrayOf(9L.toLfDate(), 4L.toLfDate()),
                date = 20L.toLfDate(),
                double = 999999999.0,
                enumName = "XXX",
                nullableEnumOrdinal = 20,
                short = 1,
                table = arrayOf(ExampleTableRow("a", 2), ExampleTableRow("a", 5)),
                tupleStringInt = arrayOf("x".repeat(20), 13)
            )
        val invalidIssuesMap = validator.validationIssues(invalidValue)

        assertMatchingIssues(
            mapOf(
                "/arrayOfStrings" to
                    listOf(
                        Issue(
                            SIZE_OUT_OF_BOUNDS_CODE,
                            data =
                                objectOf(
                                    "type" to SIZE_OUT_OF_BOUNDS_TYPE,
                                    "size" to 0,
                                    "minSize" to 1,
                                    "maxSize" to 3
                                )
                        )
                    ),
                "/nullableArrayOfStrings" to
                    listOf(Issue(IS_REQUIRED_CODE, data = objectOf("type" to IS_REQUIRED_TYPE))),
                "/boolean" to
                    listOf(
                        Issue(
                            DISALLOWED_VALUE_CODE,
                            data =
                                objectOf(
                                    "type" to DISALLOWED_VALUE_TYPE,
                                    "value" to false,
                                    "allowedValues" to listOf(true)
                                )
                        )
                    ),
                "/byte" to
                    listOf(
                        Issue(
                            NUMBER_OUT_OF_BOUNDS_CODE,
                            data =
                                objectOf(
                                    "type" to NUMBER_OUT_OF_BOUNDS_TYPE,
                                    "value" to 0.toByte(),
                                    "min" to 10.toByte(),
                                    "max" to 44.toByte()
                                )
                        )
                    ),
                "/nullableDateRange" to
                    listOf(
                        Issue(
                            INVALID_DATE_RANGE_CODE,
                            data = objectOf("type" to INVALID_DATE_RANGE_TYPE)
                        )
                    ),
                "/date" to
                    listOf(
                        Issue(
                            DATE_OUT_OF_BOUNDS_CODE,
                            data =
                                objectOf(
                                    "type" to DATE_OUT_OF_BOUNDS_TYPE,
                                    "value" to "1970-01-01T00:00:00.020Z",
                                    "maxDate" to "1970-01-01T00:00:00.015Z"
                                )
                        )
                    ),
                "/double" to
                    listOf(
                        Issue(
                            NUMBER_OUT_OF_BOUNDS_CODE,
                            data =
                                objectOf(
                                    "type" to NUMBER_OUT_OF_BOUNDS_TYPE,
                                    "value" to 999999999.0,
                                    "max" to 99999.0
                                )
                        )
                    ),
                "/enumName" to
                    listOf(
                        Issue(
                            DISALLOWED_VALUE_CODE,
                            data =
                                objectOf(
                                    "type" to DISALLOWED_VALUE_TYPE,
                                    "value" to "XXX",
                                    "allowedValues" to listOf("V1", "V2", "V3")
                                )
                        )
                    ),
                "/nullableEnumOrdinal" to
                    listOf(
                        Issue(
                            DISALLOWED_VALUE_CODE,
                            data =
                                objectOf(
                                    "type" to DISALLOWED_VALUE_TYPE,
                                    "value" to 20,
                                    "allowedValues" to listOf(0, 1, 2)
                                )
                        )
                    ),
                "/float" to listOf(Issue("NUMBER_MUST_BE_ODD")),
                "/long" to listOf(Issue("NUMBER_MUST_BE_ODD")),
                "/short" to listOf(Issue("NUMBER_MUST_BE_EVEN", isWarning = true)),
                "/string" to
                    listOf(
                        Issue(
                            LENGTH_OUT_OF_BOUNDS_CODE,
                            data =
                                objectOf(
                                    "type" to LENGTH_OUT_OF_BOUNDS_TYPE,
                                    "length" to 0,
                                    "minLength" to 3
                                )
                        )
                    ),
                "/table" to
                    listOf(
                        Issue(
                            SIZE_OUT_OF_BOUNDS_CODE,
                            data =
                                objectOf(
                                    "type" to SIZE_OUT_OF_BOUNDS_TYPE,
                                    "size" to 2,
                                    "maxSize" to 1
                                )
                        ),
                        Issue(
                            REPEATED_ELEMENTS_CODE,
                            data =
                                objectOf(
                                    "type" to REPEATED_ELEMENTS_TYPE,
                                    "indices" to listOf(0, 1)
                                )
                        )
                    ),
                "/tableSum" to
                    listOf(
                        Issue(
                            MISMATCHED_COMPUTED_CODE,
                            data =
                                objectOf(
                                    "type" to MISMATCHED_COMPUTED_TYPE,
                                    "expected" to 7.toLfLong()
                                )
                        )
                    ),
                "/tupleStringInt/0" to
                    listOf(
                        Issue(
                            LENGTH_OUT_OF_BOUNDS_CODE,
                            data =
                                objectOf(
                                    "type" to LENGTH_OUT_OF_BOUNDS_TYPE,
                                    "length" to 20,
                                    "maxLength" to 18
                                )
                        )
                    ),
                "/tupleStringInt/1" to listOf(Issue("NUMBER_MUST_BE_EVEN", isWarning = true))
            ),
            invalidIssuesMap
        )
        assertTrue(validator.hasIssues(invalidValue))
        assertFalse(validator.isValid(invalidValue))
        assertTrue(invalidIssuesMap.hasIssues())
        assertFalse(invalidIssuesMap.hasNoIssues())
        assertTrue(invalidIssuesMap.hasErrors())
        assertFalse(invalidIssuesMap.hasNoErrors())
        assertTrue(invalidIssuesMap.hasWarnings())
        assertFalse(invalidIssuesMap.hasNoWarnings())

        // Valid value =============================================================================

        val validValue =
            ExampleValue(
                boolean = true,
                arrayOfStrings = arrayOf("x"),
                nullableArrayOfStrings = arrayOf(),
                byte = 20,
                nullableDateRange = arrayOf(15L.toLfDate(), 20L.toLfDate()),
                float = 1f,
                int = 10,
                long = 1.toLfLong(),
                string = "abc"
            )
        val validIssuesMap = validator.validationIssues(validValue)

        assertMatchingIssues(mapOf(), validIssuesMap)
        assertFalse(validator.hasIssues(validValue))
        assertTrue(validator.isValid(validValue))
        assertFalse(validIssuesMap.hasIssues())
        assertTrue(validIssuesMap.hasNoIssues())
        assertFalse(validIssuesMap.hasErrors())
        assertTrue(validIssuesMap.hasNoErrors())
        assertFalse(validIssuesMap.hasWarnings())
        assertTrue(validIssuesMap.hasNoWarnings())

        // Warned value ============================================================================

        val warnedValue = validValue.copy(nullableBoolean = false, short = 1)
        val warnedIssuesMap = validator.validationIssues(warnedValue)

        assertMatchingIssues(
            mapOf("/short" to listOf(Issue("NUMBER_MUST_BE_EVEN", isWarning = true))),
            warnedIssuesMap
        )
        assertTrue(validator.hasIssues(warnedValue))
        assertTrue(validator.isValid(warnedValue))
        assertTrue(warnedIssuesMap.hasIssues())
        assertFalse(warnedIssuesMap.hasNoIssues())
        assertFalse(warnedIssuesMap.hasErrors())
        assertTrue(warnedIssuesMap.hasNoErrors())
        assertTrue(warnedIssuesMap.hasWarnings())
        assertFalse(warnedIssuesMap.hasNoWarnings())
    }
}

package pt.lightweightform.lfkotlin

/** [LfLong] is represented as a [Long] in the JVM. */
public actual typealias LfLong = Long

public actual fun Long.toLfLongImpl(): LfLong = this

public actual fun LfLong.toLongImpl(): Long = this

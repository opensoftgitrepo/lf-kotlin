@file:JvmName("ByteSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

@Suppress("UNCHECKED_CAST")
public actual fun byteSchemaImpl(
    isNullable: Boolean,
    initialValue: Byte?,
    computedInitialValue: InitialValue<Byte?>?,
    computedValue: ComputedValue<Byte?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Byte>?,
    computedAllowedValues: AllowedValues<Byte>?,
    disallowedValueCode: String?,
    min: Byte?,
    computedMin: Bound<Byte>?,
    minCode: String?,
    max: Byte?,
    computedMax: Bound<Byte>?,
    maxCode: String?,
    validations: List<Validation<Byte>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Byte?> =
    NumberSchema(
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        if (min == null && computedMin == null) Byte.MIN_VALUE else min,
        computedMin,
        minCode,
        if (max == null && computedMax == null) Byte.MAX_VALUE else max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Byte?>

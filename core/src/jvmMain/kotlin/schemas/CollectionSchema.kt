package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.Schema

public abstract class CollectionSchema<T>(
    public val elementsSchema: Schema<*>,
    public val minSize: Int?,
    public val minSizeCode: String?,
    public val computedMinSize: Bound<Int>?,
    public val maxSize: Int?,
    public val computedMaxSize: Bound<Int>?,
    public val maxSizeCode: String?
) : Schema<T> {
    public abstract fun size(value: T): Int
}

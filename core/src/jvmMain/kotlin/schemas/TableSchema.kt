@file:JvmName("TableSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

@Suppress("UNCHECKED_CAST")
public actual fun <T : Any> tableSchemaImpl(
    rowsSchema: ClassSchema<T>,
    isNullable: Boolean,
    initialValue: Array<T>?,
    computedInitialValue: InitialValue<Array<T>?>?,
    computedValue: ComputedValue<Array<T>?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Array<T>>?,
    computedAllowedValues: AllowedValues<Array<T>>?,
    disallowedValueCode: String?,
    minSize: Int?,
    computedMinSize: Bound<Int>?,
    minSizeCode: String?,
    maxSize: Int?,
    computedMaxSize: Bound<Int>?,
    maxSizeCode: String?,
    validations: List<Validation<Array<T>>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Array<T>?> =
    ArraySchema(
        rowsSchema,
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        minSize,
        computedMinSize,
        minSizeCode,
        maxSize,
        computedMaxSize,
        maxSizeCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Array<T>?>

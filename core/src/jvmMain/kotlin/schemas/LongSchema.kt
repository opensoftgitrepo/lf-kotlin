@file:JvmName("LongSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.LF_LONG_MAX_VALUE
import pt.lightweightform.lfkotlin.LF_LONG_MIN_VALUE
import pt.lightweightform.lfkotlin.LfLong
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

@Suppress("UNCHECKED_CAST")
public actual fun longSchemaImpl(
    isNullable: Boolean,
    initialValue: LfLong?,
    computedInitialValue: InitialValue<LfLong?>?,
    computedValue: ComputedValue<LfLong?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<LfLong>?,
    computedAllowedValues: AllowedValues<LfLong>?,
    disallowedValueCode: String?,
    min: LfLong?,
    computedMin: Bound<LfLong>?,
    minCode: String?,
    max: LfLong?,
    computedMax: Bound<LfLong>?,
    maxCode: String?,
    validations: List<Validation<LfLong>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<LfLong?> =
    NumberSchema(
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        if (min == null && computedMin == null) LF_LONG_MIN_VALUE else min,
        computedMin,
        minCode,
        if (max == null && computedMax == null) LF_LONG_MAX_VALUE else max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<LfLong?>

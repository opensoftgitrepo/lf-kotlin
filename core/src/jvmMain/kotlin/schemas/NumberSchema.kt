package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

public class NumberSchema<T : Number>(
    override val isNullable: Boolean,
    override val initialValue: T?,
    override val computedInitialValue: InitialValue<T?>?,
    override val computedValue: ComputedValue<T?>?,
    override val mismatchedComputedCode: String?,
    override val isClientOnly: Boolean?,
    override val isRequired: Boolean?,
    override val computedIsRequired: IsRequired?,
    override val isRequiredCode: String?,
    override val allowedValues: List<T>?,
    override val computedAllowedValues: AllowedValues<T>?,
    override val disallowedValueCode: String?,
    public val min: T?,
    public val computedMin: Bound<T>?,
    public val minCode: String?,
    public val max: T?,
    public val computedMax: Bound<T>?,
    public val maxCode: String?,
    override val validations: List<Validation<T>>?,
    override val initialState: Map<String, Any?>?,
    override val extra: Map<String, Any?>?
) : Schema<T> {
    override fun valuesAreEqual(value1: T?, value2: T?): Boolean = value1 == value2
}

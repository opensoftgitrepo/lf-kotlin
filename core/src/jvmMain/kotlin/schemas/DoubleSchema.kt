@file:JvmName("DoubleSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

@Suppress("UNCHECKED_CAST")
public actual fun doubleSchemaImpl(
    isNullable: Boolean,
    representsInteger: Boolean,
    initialValue: Double?,
    computedInitialValue: InitialValue<Double?>?,
    computedValue: ComputedValue<Double?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Double>?,
    computedAllowedValues: AllowedValues<Double>?,
    disallowedValueCode: String?,
    min: Double?,
    computedMin: Bound<Double>?,
    minCode: String?,
    max: Double?,
    computedMax: Bound<Double>?,
    maxCode: String?,
    validations: List<Validation<Double>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Double?> =
    NumberSchema(
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Double?>

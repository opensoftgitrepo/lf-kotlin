@file:JvmName("ArraySchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

public class ArraySchema<T>(
    elementsSchema: Schema<T>,
    override val isNullable: Boolean,
    override val initialValue: Array<T>?,
    override val computedInitialValue: InitialValue<Array<T>?>?,
    override val computedValue: ComputedValue<Array<T>?>?,
    override val mismatchedComputedCode: String?,
    override val isClientOnly: Boolean?,
    override val isRequired: Boolean?,
    override val computedIsRequired: IsRequired?,
    override val isRequiredCode: String?,
    override val allowedValues: List<Array<T>>?,
    override val computedAllowedValues: AllowedValues<Array<T>>?,
    override val disallowedValueCode: String?,
    minSize: Int?,
    computedMinSize: Bound<Int>?,
    minSizeCode: String?,
    maxSize: Int?,
    computedMaxSize: Bound<Int>?,
    maxSizeCode: String?,
    override val validations: List<Validation<Array<T>>>?,
    override val initialState: Map<String, Any?>?,
    override val extra: Map<String, Any?>?
) :
    CollectionSchema<Array<T>>(
        elementsSchema,
        minSize,
        minSizeCode,
        computedMinSize,
        maxSize,
        computedMaxSize,
        maxSizeCode
    ) {
    public override fun size(value: Array<T>): Int = value.size

    override fun valuesAreEqual(value1: Array<T>?, value2: Array<T>?): Boolean {
        if (value1 == null || value2 == null) {
            return value1 === value2
        }
        if (size(value1) != size(value2)) {
            return false
        }
        for ((i, el) in value1.withIndex()) {
            @Suppress("UNCHECKED_CAST")
            if (!(elementsSchema as Schema<T>).valuesAreEqual(el, value2[i])) {
                return false
            }
        }
        return true
    }
}

@Suppress("UNCHECKED_CAST")
public actual fun <T> arraySchemaImpl(
    elementsSchema: Schema<T>,
    isNullable: Boolean,
    initialValue: Array<T>?,
    computedInitialValue: InitialValue<Array<T>?>?,
    computedValue: ComputedValue<Array<T>?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Array<T>>?,
    computedAllowedValues: AllowedValues<Array<T>>?,
    disallowedValueCode: String?,
    minSize: Int?,
    computedMinSize: Bound<Int>?,
    minSizeCode: String?,
    maxSize: Int?,
    computedMaxSize: Bound<Int>?,
    maxSizeCode: String?,
    validations: List<Validation<Array<T>>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Array<T>?> =
    ArraySchema(
        elementsSchema,
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        minSize,
        computedMinSize,
        minSizeCode,
        maxSize,
        computedMaxSize,
        maxSizeCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Array<T>?>

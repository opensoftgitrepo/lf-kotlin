@file:JvmName("TupleSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

public class TupleSchema(
    public val elementsSchemas: List<Schema<Any?>>,
    override val isNullable: Boolean,
    override val initialValue: Array<Any?>?,
    override val computedInitialValue: InitialValue<Array<Any?>?>?,
    override val computedValue: ComputedValue<Array<Any?>?>?,
    override val mismatchedComputedCode: String?,
    override val isClientOnly: Boolean?,
    override val isRequired: Boolean?,
    override val computedIsRequired: IsRequired?,
    override val isRequiredCode: String?,
    override val allowedValues: List<Array<Any?>>?,
    override val computedAllowedValues: AllowedValues<Array<Any?>>?,
    override val disallowedValueCode: String?,
    override val validations: List<Validation<Array<Any?>>>?,
    override val initialState: Map<String, Any?>?,
    override val extra: Map<String, Any?>?
) : Schema<Array<Any?>> {
    override fun valuesAreEqual(value1: Array<Any?>?, value2: Array<Any?>?): Boolean {
        if (value1 == null || value2 == null) {
            return value1 === value2
        }
        for ((i, schema) in elementsSchemas.withIndex()) {
            if (!schema.valuesAreEqual(value1[i], value2[i])) {
                return false
            }
        }
        return true
    }
}

@Suppress("UNCHECKED_CAST")
public actual fun tupleSchemaImpl(
    elementsSchemas: List<Schema<Any?>>,
    isNullable: Boolean,
    initialValue: Array<Any?>?,
    computedInitialValue: InitialValue<Array<Any?>?>?,
    computedValue: ComputedValue<Array<Any?>?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Array<Any?>>?,
    computedAllowedValues: AllowedValues<Array<Any?>>?,
    disallowedValueCode: String?,
    validations: List<Validation<Array<Any?>>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Array<Any?>?> =
    TupleSchema(
        elementsSchemas,
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Array<Any?>?>

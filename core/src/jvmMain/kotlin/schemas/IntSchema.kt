@file:JvmName("IntSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

@Suppress("UNCHECKED_CAST")
public actual fun intSchemaImpl(
    isNullable: Boolean,
    initialValue: Int?,
    computedInitialValue: InitialValue<Int?>?,
    computedValue: ComputedValue<Int?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Int>?,
    computedAllowedValues: AllowedValues<Int>?,
    disallowedValueCode: String?,
    min: Int?,
    computedMin: Bound<Int>?,
    minCode: String?,
    max: Int?,
    computedMax: Bound<Int>?,
    maxCode: String?,
    validations: List<Validation<Int>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Int?> =
    NumberSchema(
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        if (min == null && computedMin == null) Int.MIN_VALUE else min,
        computedMin,
        minCode,
        if (max == null && computedMax == null) Int.MAX_VALUE else max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Int?>

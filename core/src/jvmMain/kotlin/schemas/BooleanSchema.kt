@file:JvmName("BooleanSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

public class BooleanSchema(
    override val isNullable: Boolean,
    override val initialValue: Boolean?,
    override val computedInitialValue: InitialValue<Boolean?>?,
    override val computedValue: ComputedValue<Boolean?>?,
    override val mismatchedComputedCode: String?,
    override val isClientOnly: Boolean?,
    override val isRequired: Boolean?,
    override val computedIsRequired: IsRequired?,
    override val isRequiredCode: String?,
    override val allowedValues: List<Boolean>?,
    override val computedAllowedValues: AllowedValues<Boolean>?,
    override val disallowedValueCode: String?,
    override val validations: List<Validation<Boolean>>?,
    override val initialState: Map<String, Any?>?,
    override val extra: Map<String, Any?>?
) : Schema<Boolean> {
    override fun valuesAreEqual(value1: Boolean?, value2: Boolean?): Boolean = value1 == value2
}

@Suppress("UNCHECKED_CAST")
public actual fun booleanSchemaImpl(
    isNullable: Boolean,
    initialValue: Boolean?,
    computedInitialValue: InitialValue<Boolean?>?,
    computedValue: ComputedValue<Boolean?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Boolean>?,
    computedAllowedValues: AllowedValues<Boolean>?,
    disallowedValueCode: String?,
    validations: List<Validation<Boolean>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Boolean?> =
    BooleanSchema(
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Boolean?>

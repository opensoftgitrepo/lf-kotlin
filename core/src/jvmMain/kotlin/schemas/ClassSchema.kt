@file:JvmName("ClassSchemas")
@file:Suppress("UNCHECKED_CAST")

package pt.lightweightform.lfkotlin.schemas

import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty1
import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

public actual interface ClassSchema<T> : Schema<T> {
    public actual var kClass: KClass<Any>
    public actual var childInfoByName: Map<String, ChildInfo<T>>
    public actual var constructorFunction: ConstructorFunction<T>?
    public val childrenSchemas: Map<KMutableProperty1<T, *>, Schema<*>>
}

public class ClassSchemaImpl<T>(
    override var kClass: KClass<Any>,
    override val childrenSchemas: Map<KMutableProperty1<T, *>, Schema<*>>,
    override var constructorFunction: ConstructorFunction<T>?,
    override val isNullable: Boolean,
    override val initialValue: T?,
    override val computedInitialValue: InitialValue<T?>?,
    override val computedValue: ComputedValue<T?>?,
    override val mismatchedComputedCode: String?,
    override val isClientOnly: Boolean?,
    override val isRequired: Boolean?,
    override val computedIsRequired: IsRequired?,
    override val isRequiredCode: String?,
    override val allowedValues: List<T>?,
    override val computedAllowedValues: AllowedValues<T>?,
    override val disallowedValueCode: String?,
    override val validations: List<Validation<T>>?,
    override val initialState: Map<String, Any?>?,
    override val extra: Map<String, Any?>?
) : ClassSchema<T> {
    override var childInfoByName: Map<String, ChildInfo<T>> = let {
        val map = mutableMapOf<String, ChildInfo<T>>()
        for ((prop, schema) in childrenSchemas) {
            map[prop.name] = ChildInfo(prop, schema)
        }
        map
    }

    override fun valuesAreEqual(value1: T?, value2: T?): Boolean {
        if (value1 == null || value2 == null) {
            return value1 === value2
        }
        for ((prop, schema) in childrenSchemas) {
            if (!(schema as Schema<Any?>).valuesAreEqual(prop.get(value1), prop.get(value2))) {
                return false
            }
        }
        return true
    }
}

public actual fun <T : Any> classSchemaImpl(
    kClass: KClass<T>,
    childrenSchemas: Map<KMutableProperty1<T, *>, Schema<*>>,
    constructorFunction: ConstructorFunction<T>?,
    isNullable: Boolean,
    initialValue: T?,
    computedInitialValue: InitialValue<T?>?,
    computedValue: ComputedValue<T?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<T>?,
    computedAllowedValues: AllowedValues<T>?,
    disallowedValueCode: String?,
    validations: List<Validation<T>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): ClassSchema<T?> =
    ClassSchemaImpl(
        kClass as KClass<Any>,
        childrenSchemas,
        constructorFunction,
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        validations,
        initialState,
        extra
    ) as
        ClassSchema<T?>

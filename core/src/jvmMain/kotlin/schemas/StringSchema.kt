@file:JvmName("StringSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

public class StringSchema(
    override val isNullable: Boolean,
    override val initialValue: String?,
    override val computedInitialValue: InitialValue<String?>?,
    override val computedValue: ComputedValue<String?>?,
    override val mismatchedComputedCode: String?,
    override val isClientOnly: Boolean?,
    override val isRequired: Boolean?,
    override val computedIsRequired: IsRequired?,
    override val isRequiredCode: String?,
    override val allowedValues: List<String>?,
    override val computedAllowedValues: AllowedValues<String>?,
    override val disallowedValueCode: String?,
    public val minLength: Int?,
    public val computedMinLength: Bound<Int>?,
    public val minLengthCode: String?,
    public val maxLength: Int?,
    public val computedMaxLength: Bound<Int>?,
    public val maxLengthCode: String?,
    override val validations: List<Validation<String>>?,
    override val initialState: Map<String, Any?>?,
    override val extra: Map<String, Any?>?
) : Schema<String> {
    override fun valuesAreEqual(value1: String?, value2: String?): Boolean = value1 == value2
}

@Suppress("UNCHECKED_CAST")
public actual fun stringSchemaImpl(
    isNullable: Boolean,
    initialValue: String?,
    computedInitialValue: InitialValue<String?>?,
    computedValue: ComputedValue<String?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<String>?,
    computedAllowedValues: AllowedValues<String>?,
    disallowedValueCode: String?,
    minLength: Int?,
    computedMinLength: Bound<Int>?,
    minLengthCode: String?,
    maxLength: Int?,
    computedMaxLength: Bound<Int>?,
    maxLengthCode: String?,
    validations: List<Validation<String>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<String?> =
    StringSchema(
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        minLength,
        computedMinLength,
        minLengthCode,
        maxLength,
        computedMaxLength,
        maxLengthCode,
        validations,
        initialState,
        extra
    ) as
        Schema<String?>

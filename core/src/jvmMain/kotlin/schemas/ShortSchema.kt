@file:JvmName("ShortSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

@Suppress("UNCHECKED_CAST")
public actual fun shortSchemaImpl(
    isNullable: Boolean,
    initialValue: Short?,
    computedInitialValue: InitialValue<Short?>?,
    computedValue: ComputedValue<Short?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Short>?,
    computedAllowedValues: AllowedValues<Short>?,
    disallowedValueCode: String?,
    min: Short?,
    computedMin: Bound<Short>?,
    minCode: String?,
    max: Short?,
    computedMax: Bound<Short>?,
    maxCode: String?,
    validations: List<Validation<Short>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Short?> =
    NumberSchema(
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        if (min == null && computedMin == null) Short.MIN_VALUE else min,
        computedMin,
        minCode,
        if (max == null && computedMax == null) Short.MAX_VALUE else max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Short?>

@file:JvmName("DateSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.LfDate
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

public class DateSchema(
    override val isNullable: Boolean,
    override val initialValue: LfDate?,
    override val computedInitialValue: InitialValue<LfDate?>?,
    override val computedValue: ComputedValue<LfDate?>?,
    override val mismatchedComputedCode: String?,
    override val isClientOnly: Boolean?,
    override val isRequired: Boolean?,
    override val computedIsRequired: IsRequired?,
    override val isRequiredCode: String?,
    override val allowedValues: List<LfDate>?,
    override val computedAllowedValues: AllowedValues<LfDate>?,
    override val disallowedValueCode: String?,
    public val minDate: LfDate?,
    public val computedMinDate: Bound<LfDate>?,
    public val minDateCode: String?,
    public val maxDate: LfDate?,
    public val computedMaxDate: Bound<LfDate>?,
    public val maxDateCode: String?,
    override val validations: List<Validation<LfDate>>?,
    override val initialState: Map<String, Any?>?,
    override val extra: Map<String, Any?>?
) : Schema<LfDate> {
    override fun valuesAreEqual(value1: LfDate?, value2: LfDate?): Boolean = value1 == value2
}

@Suppress("UNCHECKED_CAST")
public actual fun dateSchemaImpl(
    isNullable: Boolean,
    initialValue: LfDate?,
    computedInitialValue: InitialValue<LfDate?>?,
    computedValue: ComputedValue<LfDate?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<LfDate>?,
    computedAllowedValues: AllowedValues<LfDate>?,
    disallowedValueCode: String?,
    minDate: LfDate?,
    computedMinDate: Bound<LfDate>?,
    minDateCode: String?,
    maxDate: LfDate?,
    computedMaxDate: Bound<LfDate>?,
    maxDateCode: String?,
    validations: List<Validation<LfDate>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<LfDate?> =
    DateSchema(
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        minDate,
        computedMinDate,
        minDateCode,
        maxDate,
        computedMaxDate,
        maxDateCode,
        validations,
        initialState,
        extra
    ) as
        Schema<LfDate?>

@file:JvmName("FloatSchemas")

package pt.lightweightform.lfkotlin.schemas

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.InitialValue
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation

@Suppress("UNCHECKED_CAST")
public actual fun floatSchemaImpl(
    isNullable: Boolean,
    representsInteger: Boolean,
    initialValue: Float?,
    computedInitialValue: InitialValue<Float?>?,
    computedValue: ComputedValue<Float?>?,
    mismatchedComputedCode: String?,
    isClientOnly: Boolean?,
    isRequired: Boolean?,
    computedIsRequired: IsRequired?,
    isRequiredCode: String?,
    allowedValues: List<Float>?,
    computedAllowedValues: AllowedValues<Float>?,
    disallowedValueCode: String?,
    min: Float?,
    computedMin: Bound<Float>?,
    minCode: String?,
    max: Float?,
    computedMax: Bound<Float>?,
    maxCode: String?,
    validations: List<Validation<Float>>?,
    initialState: Map<String, Any?>?,
    extra: Map<String, Any?>?
): Schema<Float?> =
    NumberSchema(
        isNullable,
        initialValue,
        computedInitialValue,
        computedValue,
        mismatchedComputedCode,
        isClientOnly,
        isRequired,
        computedIsRequired,
        isRequiredCode,
        allowedValues,
        computedAllowedValues,
        disallowedValueCode,
        min,
        computedMin,
        minCode,
        max,
        computedMax,
        maxCode,
        validations,
        initialState,
        extra
    ) as
        Schema<Float?>

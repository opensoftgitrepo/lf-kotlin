package pt.lightweightform.lfkotlin

/** Exception at a certain [path]. */
public class PathException(public val path: Path, message: String) :
    Exception("At '$path': $message")

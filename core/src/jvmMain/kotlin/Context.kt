package pt.lightweightform.lfkotlin

import pt.lightweightform.lfkotlin.internal.MutableState
import pt.lightweightform.lfkotlin.internal.lastPathPartInfo

/** Context available for use in validations, computed values, and state properties. */
public actual class Context(
    private val rootSchema: Schema<*>,
    private val rootValue: Any?,
    private val state: MutableState,
    public actual val currentPath: Path,
    public val externalContext: Any?
) {
    public actual fun relativeContext(relativePath: Path): Context =
        Context(
            rootSchema,
            rootValue,
            state,
            resolvePath(currentPath, relativePath),
            externalContext
        )

    @Suppress("UNCHECKED_CAST")
    public actual fun <T> get(relativePath: String): T =
        lastPathPartInfo(rootSchema, rootValue, resolvePath(currentPath, relativePath), true)
            .value as
            T

    @Suppress("UNCHECKED_CAST")
    public actual fun <T> getStateProperty(relativePath: Path, prop: String): T {
        val path = resolvePath(currentPath, relativePath)
        return state.getOrPut(path) { mutableMapOf() }.getOrPut(prop) {
            val schema = lastPathPartInfo(rootSchema, rootValue, path, false).schema
            if (schema.initialState?.contains(prop) == true)
                when (val initialStateProp = schema.initialState!![prop]) {
                    is SyncStateProperty<*> ->
                        initialStateProp.run { relativeContext(path).property() }
                    is StateProperty<*> -> error("Unsupported `StateProperty` implementation")
                    else -> initialStateProp
                }
            else null
        } as
            T
    }
}

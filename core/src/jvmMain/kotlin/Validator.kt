@file:JvmName("Validators")

package pt.lightweightform.lfkotlin

import pt.lightweightform.lfkotlin.internal.MutableIssuesMap
import pt.lightweightform.lfkotlin.internal.MutableState
import pt.lightweightform.lfkotlin.internal.addIssue
import pt.lightweightform.lfkotlin.internal.lastPathPartInfo
import pt.lightweightform.lfkotlin.internal.pathInfoImpl
import pt.lightweightform.lfkotlin.internal.runExternalValidation
import pt.lightweightform.lfkotlin.internal.validate

/**
 * Class responsible for validating a schema on the server-side. Assumes that [schema] is valid
 * according to LF's storage.
 *
 * The validator supports [external validations][externalValidations] which only run when a value
 * has no errors according to the schema.
 */
public class Validator<T>
@JvmOverloads
constructor(
    public val schema: Schema<T>,
    private val externalValidations: Map<Path, Validation<*>>? = null
) {
    /**
     * Returns information about the parts of [value] matching [path] from the root to the most
     * specific part.
     */
    @JvmOverloads
    public fun pathInfo(
        value: T?,
        path: Path,
        fetchValues: Boolean = value != null
    ): List<PathPartInfo> = pathInfoImpl(schema, value, path, fetchValues)

    /** Checks whether [path] is a valid schema path. Throws an error if not. */
    public fun validatePath(path: Path) {
        pathInfo(null, path, false)
    }

    /** Returns whether [path] is a valid schema path. */
    public fun hasPath(path: Path): Boolean =
        try {
            validatePath(path)
            true
        } catch (ex: PathException) {
            false
        }

    /** Last part of [pathInfo]. */
    private fun lastPathPartInfo(
        value: T?,
        path: Path,
        fetchValues: Boolean = value != null
    ): PathPartInfo = lastPathPartInfo(schema, value, path, fetchValues)

    /**
     * Returns the schema associated with the given [path]. Supports wildcard ids (`?`) for
     * collection elements.
     */
    public fun schema(path: Path): Schema<*> = lastPathPartInfo(null, path, false).schema

    /**
     * Returns whether the schema associated with the given [path] is computed. Supports wildcard
     * ids (`?`) for collection elements.
     */
    public fun isComputed(path: Path): Boolean = lastPathPartInfo(null, path, false).isComputed

    /** Returns whether [value] has a value at [path]. */
    public fun has(value: T, path: Path): Boolean {
        validatePath(path)
        return try {
            pathInfo(value, path, true)
            true
        } catch (ex: PathException) {
            false
        }
    }

    /** Returns the part of [value] matching [path]. */
    public fun get(value: T, path: Path): Any? = lastPathPartInfo(value, path, true).value

    /**
     * Returns all found validation issues associated with [value] at the given [path] according to
     * [schema]: mapping value paths to their associated issues.
     *
     * If [extra validations][externalValidations] were provided and no errors were found according
     * to the schema, then the extra validations will run for the value at [path].
     */
    @JvmOverloads
    public fun validationIssues(
        value: T,
        externalContext: Any? = null,
        path: Path = "/"
    ): IssuesMap {
        val issuesMap: MutableIssuesMap = mutableMapOf()
        val state: MutableState = mutableMapOf()
        val info = lastPathPartInfo(value, path, true)

        // Run schema validations
        validate(schema, value, state, info.schema, info.value, info.path, externalContext)
            .forEach { (path, issue) -> issuesMap.addIssue(path, issue) }

        // Run external validations if no errors were found in the schema
        if (issuesMap.hasNoErrors() && externalValidations != null) {
            for ((validationPath, validation) in externalValidations) {
                if (pathsMatch(path, validationPath) || isMatchingSubpath(path, validationPath)) {
                    runExternalValidation(
                        schema,
                        value,
                        state,
                        validationPath,
                        validation,
                        externalContext
                    )
                        .forEach { (path, issue) -> issuesMap.addIssue(path, issue) }
                }
            }
        }

        return issuesMap
    }

    /**
     * Returns whether [value] has any kind of issues at the given [path] according to [schema],
     * i.e. returns `true` if _any_ issues are found, including only warnings.
     */
    @JvmOverloads
    public fun hasIssues(value: T, externalContext: Any? = null, path: Path = "/"): Boolean {
        val info = lastPathPartInfo(value, path, true)
        val state = mutableMapOf<Path, MutableMap<String, Any?>>()
        return validate(schema, value, state, info.schema, info.value, info.path, externalContext)
            .firstOrNull() != null
    }

    /**
     * Returns whether [value] is valid at the given [path] according to [schema], i.e. returns
     * `true` when there are no issues that aren't warnings.
     */
    @JvmOverloads
    public fun isValid(value: T, externalContext: Any? = null, path: Path = "/"): Boolean {
        val info = lastPathPartInfo(value, path, true)
        val state = mutableMapOf<Path, MutableMap<String, Any?>>()
        return validate(schema, value, state, info.schema, info.value, info.path, externalContext)
            .firstOrNull { (_, issue) -> !issue.isWarning } == null
    }
}

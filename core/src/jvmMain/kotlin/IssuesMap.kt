@file:JvmName("IssuesMaps")

package pt.lightweightform.lfkotlin

import pt.lightweightform.lfkotlin.internal.MutableIssuesMap
import pt.lightweightform.lfkotlin.internal.addIssue

/** Mapping of paths to their issues. */
public typealias IssuesMap = Map<Path, List<Issue>>

/** An empty map of issues. */
public fun emptyIssuesMap(): IssuesMap = emptyMap()

/** Whether a map of issues contains issues. */
public fun IssuesMap.hasIssues(): Boolean = values.any { it.isNotEmpty() }

/** Whether a map of issues contains no issues. */
public fun IssuesMap.hasNoIssues(): Boolean = !hasIssues()

/** Whether a map of issues contains errors. */
public fun IssuesMap.hasErrors(): Boolean =
    values.any { issues -> issues.any { issue -> !issue.isWarning } }

/** Whether a map of issues contains no errors. */
public fun IssuesMap.hasNoErrors(): Boolean = !hasErrors()

/** Whether a map of issues contains warning. */
public fun IssuesMap.hasWarnings(): Boolean =
    values.any { issues -> issues.any { issue -> issue.isWarning } }

/** Whether a map of issues contains no errors. */
public fun IssuesMap.hasNoWarnings(): Boolean = !hasWarnings()

/** Returns a mapping of paths to their errors (warnings are excluded). */
public fun IssuesMap.errors(): IssuesMap {
    val errorsMap: MutableIssuesMap = mutableMapOf()
    for ((path, issues) in this) {
        for (issue in issues) {
            if (!issue.isWarning) {
                errorsMap.addIssue(path, issue)
            }
        }
    }
    return errorsMap
}

/** Returns a mapping of paths to their warnings (errors are excluded). */
public fun IssuesMap.warnings(): IssuesMap {
    val warningsMap: MutableIssuesMap = mutableMapOf()
    for ((path, issues) in this) {
        for (issue in issues) {
            if (issue.isWarning) {
                warningsMap.addIssue(path, issue)
            }
        }
    }
    return warningsMap
}

/** Whether two maps of issues equal one another. */
public fun IssuesMap.equal(other: IssuesMap): Boolean {
    val relevant = filterValues { it.isNotEmpty() }
    val relevantOther = other.filterValues { it.isNotEmpty() }
    return relevant.size == relevantOther.size &&
        relevant.entries.all { (path, issues) ->
            val otherIssues = relevantOther[path]?.toHashSet()
            otherIssues != null && issues.toHashSet() == otherIssues
        }
}

/** Whether this map of issues contains the [other] map of issues. */
public operator fun IssuesMap.contains(other: IssuesMap): Boolean =
    other.all { (otherPath, otherIssues) ->
        if (otherIssues.isEmpty()) true
        else {
            val issues = this[otherPath]?.toHashSet()
            issues != null && otherIssues.all { it in issues }
        }
    }

/**
 * Returns the map of issues that results from removing all issues in the [other] map of issues from
 * this map of issues.
 */
public operator fun IssuesMap.minus(other: IssuesMap): IssuesMap {
    val diffMap = mutableMapOf<Path, MutableList<Issue>>()
    for ((path, issues) in this) {
        val diffIssues by lazy { mutableListOf<Issue>().also { diffMap[path] = it } }
        val otherIssues = other[path]?.toHashSet() ?: emptySet()
        for (issue in issues) {
            if (issue !in otherIssues) {
                diffIssues += issue
            }
        }
    }
    return diffMap
}

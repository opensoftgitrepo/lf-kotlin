package pt.lightweightform.lfkotlin.util

import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.Context
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.SyncBound
import pt.lightweightform.lfkotlin.SyncIsRequired

public actual fun <T> isRequiredToBound(isRequired: IsRequired, boundValue: T): Bound<T> =
    when (isRequired) {
        is SyncIsRequired -> IsRequiredBound(isRequired, boundValue)
        else -> error("Unsupported `IsRequired` implementation")
    }

private class IsRequiredBound<T>(
    private val isRequired: SyncIsRequired,
    private val boundValue: T
) : SyncBound<T> {
    override fun Context.bound(): T? = isRequired.run { if (isRequired()) boundValue else null }
}

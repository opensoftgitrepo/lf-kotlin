@file:JvmName("JsonObjects")

package pt.lightweightform.lfkotlin

public actual fun objectOf(vararg pairs: Pair<String, Any?>): Any {
    val obj = mutableMapOf<String, Any?>()
    for ((key, value) in pairs) {
        when (value) {
            null -> obj[key] = null as Boolean?
            is Boolean -> obj[key] = value
            is Number -> obj[key] = value
            is String -> obj[key] = value
            is List<*> -> obj[key] = jsonArrayOf(value)
            is Map<*, *> -> obj[key] = value
            // TODO: Better way of serializing non-JSON values
            else -> obj[key] = value.toString()
        }
    }
    return obj
}

private fun jsonArrayOf(value: List<*>): List<*> {
    val list = mutableListOf<Any?>()
    for (el in value) {
        when (el) {
            null -> list.add(null as Boolean?)
            is Boolean -> list.add(el)
            is Number -> list.add(el)
            is String -> list.add(el)
            is List<*> -> list.add(jsonArrayOf(el))
            is Map<*, *> -> list.add(el)
            // TODO: Better way of serializing non-JSON values
            else -> list.add(el.toString())
        }
    }
    return list
}

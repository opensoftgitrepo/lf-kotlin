package pt.lightweightform.lfkotlin

/** Part of a path info. */
public data class PathPartInfo(
    val path: Path,
    val id: String?,
    val schema: Schema<*>,
    val isComputed: Boolean,
    val value: Any?
)

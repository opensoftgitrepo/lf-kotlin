package pt.lightweightform.lfkotlin.test

import pt.lightweightform.lfkotlin.Issue
import pt.lightweightform.lfkotlin.IssuesMap
import pt.lightweightform.lfkotlin.Path

/** Formats a [prefix] to be added to an assert message. */
private fun messagePrefix(prefix: String?) = if (prefix == null) "" else "$prefix. "

/** Whether an [expectedIssue] matches the [actualIssue]. */
private fun issuesMatch(expectedIssue: Issue, actualIssue: Issue): Boolean =
    expectedIssue.code == actualIssue.code &&
        (expectedIssue.data == null ||
            (actualIssue.data != null &&
                if (expectedIssue.data is Map<*, *> && actualIssue.data is Map<*, *>)
                    expectedIssue.data.all { (key, value) ->
                        key in actualIssue.data && actualIssue.data[key] == value
                    }
                else expectedIssue.data == actualIssue.data))

/**
 * Asserts that the [expectedIssuesMap] matches the [actualIssuesMap]. The order in which the issues
 * appear is not relevant.
 *
 * Matching issues are those with the same path, code, and where the `data` of the actual issue
 * "contains" the `data` of the expected issue (if no `data` is provided in an expected issue, it
 * will always be "contained" by that of an actual issue).
 */
public fun assertMatchingIssues(
    expectedIssuesMap: IssuesMap,
    actualIssuesMap: IssuesMap,
    message: String? = null
) {
    for ((path, expectedIssues) in expectedIssuesMap) {
        val actualIssues = actualIssuesMap[path] ?: emptyList()
        for (expectedIssue in expectedIssues) {
            if (!actualIssues.any { actualIssue -> issuesMatch(expectedIssue, actualIssue) }) {
                throw AssertionError(
                    messagePrefix(message) +
                        "Expected issue <$expectedIssue> in path <$path> does not match any " +
                        "actual issue.\n" +
                        "Expected issues: <$expectedIssuesMap>, actual issues: <$actualIssuesMap>."
                )
            }
        }
    }
    for ((path, actualIssues) in actualIssuesMap) {
        val expectedIssues = expectedIssuesMap[path] ?: emptyList()
        for (actualIssue in actualIssues) {
            if (!expectedIssues.any { expectedIssue -> issuesMatch(expectedIssue, actualIssue) }) {
                throw AssertionError(
                    messagePrefix(message) +
                        "Actual issue <$actualIssue> in path <$path> does not match any expected " +
                        "issue.\n" +
                        "Expected issues: <$expectedIssuesMap>, actual issues: <$actualIssuesMap>."
                )
            }
        }
    }
}

/**
 * Asserts that the [actualIssuesMap] contains all matching issues in [expectedIssuesMap]. The order
 * in which the issues appear is not relevant.
 *
 * Matching issues are those with the same path, code, and where the `data` of the actual issue
 * "contains" the `data` of the expected issue (if no `data` is provided in an expected issue, it
 * will always be "contained" by that of an actual issue).
 */
public fun assertContainsMatchingIssues(
    expectedIssuesMap: IssuesMap,
    actualIssuesMap: IssuesMap,
    message: String? = null
) {
    for ((path, expectedIssues) in expectedIssuesMap) {
        val actualIssues = actualIssuesMap[path] ?: emptyList()
        for (expectedIssue in expectedIssues) {
            if (!actualIssues.any { actualIssue -> issuesMatch(expectedIssue, actualIssue) }) {
                throw AssertionError(
                    messagePrefix(message) +
                        "Expected actual issues to contain expected issues.\n" +
                        "Expected issue <$expectedIssue> in path <$path> does not match any " +
                        "actual issue.\n" +
                        "Expected issues: <$expectedIssues>, actual issues: <$actualIssues>."
                )
            }
        }
    }
}

/**
 * Asserts that [actualIssuesMap] contains a matching [expectedIssue] in path [expectedIssuePath].
 *
 * Matching issues are those with the same path, code, and severity and where the `data` of the
 * actual issue "contains" the `data` of the expected issue (if no `data` is provided in an expected
 * issue, it will always be "contained" by that of an actual issue).
 */
public fun assertContainsMatchingIssue(
    expectedIssuePath: Path,
    expectedIssue: Issue,
    actualIssuesMap: IssuesMap,
    message: String? = null
) {
    val actualIssues = actualIssuesMap[expectedIssuePath] ?: emptyList()
    for (actualIssue in actualIssues) {
        if (issuesMatch(expectedIssue, actualIssue)) {
            return
        }
    }

    throw AssertionError(
        messagePrefix(message) +
            "Expected actual issues to contain expected issue <$expectedIssue> in path " +
            "<$expectedIssuePath>.\n" +
            "Actual issues: <$actualIssuesMap>."
    )
}

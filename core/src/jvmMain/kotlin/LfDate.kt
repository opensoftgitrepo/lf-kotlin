package pt.lightweightform.lfkotlin

import java.time.Instant
import java.time.format.DateTimeFormatter

/** [LfDate] is represented as an [Instant] in the JVM. */
public actual typealias LfDate = Instant

public actual fun Long.toLfDate(): LfDate = Instant.ofEpochMilli(this)

public actual fun LfDate.toEpochMilliseconds(): Long = this.toEpochMilli()

public actual fun LfDate.toISOString(): String = DateTimeFormatter.ISO_INSTANT.format(this)

package pt.lightweightform.lfkotlin.internal

import pt.lightweightform.lfkotlin.Path

/** State stored by the validation context. */
internal typealias MutableState = MutableMap<Path, MutableMap<String, Any?>>

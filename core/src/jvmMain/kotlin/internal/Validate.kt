package pt.lightweightform.lfkotlin.internal

import pt.lightweightform.lfkotlin.DATE_OUT_OF_BOUNDS_CODE
import pt.lightweightform.lfkotlin.DATE_OUT_OF_BOUNDS_TYPE
import pt.lightweightform.lfkotlin.DISALLOWED_VALUE_CODE
import pt.lightweightform.lfkotlin.DISALLOWED_VALUE_TYPE
import pt.lightweightform.lfkotlin.IS_REQUIRED_CODE
import pt.lightweightform.lfkotlin.IS_REQUIRED_TYPE
import pt.lightweightform.lfkotlin.Issue
import pt.lightweightform.lfkotlin.LENGTH_OUT_OF_BOUNDS_CODE
import pt.lightweightform.lfkotlin.LENGTH_OUT_OF_BOUNDS_TYPE
import pt.lightweightform.lfkotlin.LfDate
import pt.lightweightform.lfkotlin.MISMATCHED_COMPUTED_CODE
import pt.lightweightform.lfkotlin.MISMATCHED_COMPUTED_TYPE
import pt.lightweightform.lfkotlin.NUMBER_OUT_OF_BOUNDS_CODE
import pt.lightweightform.lfkotlin.NUMBER_OUT_OF_BOUNDS_TYPE
import pt.lightweightform.lfkotlin.Path
import pt.lightweightform.lfkotlin.PathException
import pt.lightweightform.lfkotlin.SIZE_OUT_OF_BOUNDS_CODE
import pt.lightweightform.lfkotlin.SIZE_OUT_OF_BOUNDS_TYPE
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.appendToPath
import pt.lightweightform.lfkotlin.objectOf
import pt.lightweightform.lfkotlin.schemas.ArraySchema
import pt.lightweightform.lfkotlin.schemas.BooleanSchema
import pt.lightweightform.lfkotlin.schemas.ClassSchema
import pt.lightweightform.lfkotlin.schemas.CollectionSchema
import pt.lightweightform.lfkotlin.schemas.DateSchema
import pt.lightweightform.lfkotlin.schemas.NumberSchema
import pt.lightweightform.lfkotlin.schemas.StringSchema
import pt.lightweightform.lfkotlin.schemas.TupleSchema
import pt.lightweightform.lfkotlin.toISOString

/** Validates [value] according to [schema] by returning a sequence over all found issues. */
internal fun validate(
    rootSchema: Schema<*>,
    rootValue: Any?,
    state: MutableState,
    schema: Schema<*> = rootSchema,
    value: Any? = rootValue,
    path: Path = "/",
    externalContext: Any?
): Sequence<Pair<String, Issue>> = sequence {
    val isComputed = schema.computedValue != null

    // Ignore all values of client-only schemas (computed values are client-only by default)
    if (schema.isClientOnly == true || (isComputed && schema.isClientOnly == null)) {
        return@sequence
    }

    // Validate that the computed value matches the value
    if (isComputed) {
        val computed =
            runComputedValue(
                rootSchema,
                rootValue,
                state,
                path,
                externalContext,
                schema.computedValue!!
            )
        @Suppress("UNCHECKED_CAST")
        if (!(schema as Schema<Any?>).valuesAreEqual(value, computed)) {
            yield(
                path to
                    Issue(
                        schema.mismatchedComputedCode ?: MISMATCHED_COMPUTED_CODE,
                        data = objectOf("type" to MISMATCHED_COMPUTED_TYPE, "expected" to computed)
                    )
            )
        }
    }

    // Validate `null` values
    if (value == null) {
        if (!schema.isNullable) {
            throw PathException(path, "Received null value for non-nullable schema.")
        }
        val isRequired =
            when {
                schema.isRequired != null -> schema.isRequired!!
                schema.computedIsRequired != null ->
                    runIsRequired(
                        rootSchema,
                        rootValue,
                        state,
                        path,
                        externalContext,
                        schema.computedIsRequired!!
                    )
                else -> false
            }
        if (isRequired) {
            yield(
                path to
                    Issue(
                        schema.isRequiredCode ?: IS_REQUIRED_CODE,
                        data = mapOf("type" to IS_REQUIRED_TYPE)
                    )
            )
        }
        return@sequence
    }

    // Validate "allowed values"
    val allowedValues =
        when {
            schema.allowedValues != null -> schema.allowedValues
            schema.computedAllowedValues != null ->
                runAllowedValues(
                    rootSchema,
                    rootValue,
                    state,
                    path,
                    externalContext,
                    schema.computedAllowedValues!!
                )
            else -> null
        }
    if (allowedValues != null) {
        val isAllowed =
            allowedValues.find { allowed ->
                @Suppress("UNCHECKED_CAST") (schema as Schema<Any?>).valuesAreEqual(value, allowed)
            } != null
        if (!isAllowed) {
            yield(
                path to
                    Issue(
                        schema.disallowedValueCode ?: DISALLOWED_VALUE_CODE,
                        data =
                            objectOf(
                                "type" to DISALLOWED_VALUE_TYPE,
                                "value" to value,
                                "allowedValues" to allowedValues
                            )
                    )
            )
        }
    }

    @Suppress("UNCHECKED_CAST")
    when (schema) {
        // Validate collections
        is CollectionSchema<*> -> {
            schema as CollectionSchema<Any?>
            val size = schema.size(value)
            val minSize =
                when {
                    schema.minSize != null -> schema.minSize
                    schema.computedMinSize != null ->
                        runBound(
                            rootSchema,
                            rootValue,
                            state,
                            path,
                            externalContext,
                            schema.computedMinSize
                        )
                    else -> null
                }
            val maxSize =
                when {
                    schema.maxSize != null -> schema.maxSize
                    schema.computedMaxSize != null ->
                        runBound(
                            rootSchema,
                            rootValue,
                            state,
                            path,
                            externalContext,
                            schema.computedMaxSize
                        )
                    else -> null
                }
            val code =
                if (minSize != null && size < minSize) schema.minSizeCode ?: SIZE_OUT_OF_BOUNDS_CODE
                else if (maxSize != null && size > maxSize)
                    schema.maxSizeCode ?: SIZE_OUT_OF_BOUNDS_CODE
                else null
            if (code != null) {
                yield(
                    path to
                        Issue(
                            code,
                            data =
                                buildMap<String, Any?> {
                                    put("type", SIZE_OUT_OF_BOUNDS_TYPE)
                                    put("size", size)
                                    if (minSize != null) put("minSize", minSize)
                                    if (maxSize != null) put("maxSize", maxSize)
                                }
                        )
                )
            }

            // Validate collection children (children of computed schemas shouldn't be validated)
            if (!isComputed) {
                when (schema) {
                    is ArraySchema<*> -> {
                        value as Array<*>
                        for ((i, el) in value.withIndex()) {
                            yieldAll(
                                validate(
                                    rootSchema,
                                    rootValue,
                                    state,
                                    schema.elementsSchema,
                                    el,
                                    appendToPath(path, "$i"),
                                    externalContext
                                )
                            )
                        }
                    }
                    else -> error("Invalid schema")
                }
            }
        }
        is DateSchema -> {
            value as LfDate
            val minDate =
                when {
                    schema.minDate != null -> schema.minDate
                    schema.computedMinDate != null ->
                        runBound(
                            rootSchema,
                            rootValue,
                            state,
                            path,
                            externalContext,
                            schema.computedMinDate
                        )
                    else -> null
                }
            val maxDate =
                when {
                    schema.maxDate != null -> schema.maxDate
                    schema.computedMaxDate != null ->
                        runBound(
                            rootSchema,
                            rootValue,
                            state,
                            path,
                            externalContext,
                            schema.computedMaxDate
                        )
                    else -> null
                }
            val code =
                if (minDate != null && value < minDate)
                    schema.minDateCode ?: DATE_OUT_OF_BOUNDS_CODE
                else if (maxDate != null && value > maxDate)
                    schema.maxDateCode ?: DATE_OUT_OF_BOUNDS_CODE
                else null
            if (code != null) {
                yield(
                    path to
                        Issue(
                            code,
                            data =
                                buildMap<String, Any?> {
                                    put("type", DATE_OUT_OF_BOUNDS_TYPE)
                                    put("value", value.toString())
                                    if (minDate != null) put("minDate", minDate.toISOString())
                                    if (maxDate != null) put("maxDate", maxDate.toISOString())
                                }
                        )
                )
            }
        }
        is ClassSchema<*> -> {
            // Validate class children (children of computed schemas shouldn't be validated)
            if (!isComputed) {
                schema as ClassSchema<Any>
                for ((prop, childSchema) in schema.childrenSchemas) {
                    yieldAll(
                        validate(
                            rootSchema,
                            rootValue,
                            state,
                            childSchema,
                            prop.get(value),
                            appendToPath(path, prop.name),
                            externalContext
                        )
                    )
                }
            }
        }
        is NumberSchema<*> -> {
            value as Comparable<Number>
            val min =
                when {
                    schema.min != null -> schema.min
                    schema.computedMin != null ->
                        runBound(
                            rootSchema,
                            rootValue,
                            state,
                            path,
                            externalContext,
                            schema.computedMin
                        )
                    else -> null
                }
            val max =
                when {
                    schema.max != null -> schema.max
                    schema.computedMax != null ->
                        runBound(
                            rootSchema,
                            rootValue,
                            state,
                            path,
                            externalContext,
                            schema.computedMax
                        )
                    else -> null
                }
            val code =
                if (min != null && value < min) schema.minCode ?: NUMBER_OUT_OF_BOUNDS_CODE
                else if (max != null && value > max) schema.maxCode ?: NUMBER_OUT_OF_BOUNDS_CODE
                else null
            if (code != null) {
                yield(
                    path to
                        Issue(
                            code,
                            data =
                                buildMap<String, Any?> {
                                    put("type", NUMBER_OUT_OF_BOUNDS_TYPE)
                                    put("value", value as Number)
                                    if (min != null) put("min", min)
                                    if (max != null) put("max", max)
                                }
                        )
                )
            }
        }
        is StringSchema -> {
            value as String
            val length = value.length
            val minLength =
                when {
                    schema.minLength != null -> schema.minLength
                    schema.computedMinLength != null ->
                        runBound(
                            rootSchema,
                            rootValue,
                            state,
                            path,
                            externalContext,
                            schema.computedMinLength
                        )
                    else -> null
                }
            val maxLength =
                when {
                    schema.maxLength != null -> schema.maxLength
                    schema.computedMaxLength != null ->
                        runBound(
                            rootSchema,
                            rootValue,
                            state,
                            path,
                            externalContext,
                            schema.computedMaxLength
                        )
                    else -> null
                }
            val code =
                if (minLength != null && length < minLength)
                    schema.minLengthCode ?: LENGTH_OUT_OF_BOUNDS_CODE
                else if (maxLength != null && length > maxLength)
                    schema.maxLengthCode ?: LENGTH_OUT_OF_BOUNDS_CODE
                else null
            if (code != null) {
                yield(
                    path to
                        Issue(
                            code,
                            data =
                                buildMap<String, Any?> {
                                    put("type", LENGTH_OUT_OF_BOUNDS_TYPE)
                                    put("length", length)
                                    if (minLength != null) put("minLength", minLength)
                                    if (maxLength != null) put("maxLength", maxLength)
                                }
                        )
                )
            }
        }
        is TupleSchema -> {
            // Validate tuple children (children of computed schemas shouldn't be validated)
            if (!isComputed) {
                value as Array<*>
                for ((i, childSchema) in schema.elementsSchemas.withIndex()) {
                    yieldAll(
                        validate(
                            rootSchema,
                            rootValue,
                            state,
                            childSchema,
                            value[i],
                            appendToPath(path, "$i"),
                            externalContext
                        )
                    )
                }
            }
        }
        !is BooleanSchema -> error("Unsupported `Schema` implementation")
    }

    // Run custom validations
    for (validation in schema.validations ?: emptyList()) {
        runValidation(rootSchema, rootValue, state, path, externalContext, validation).forEach {
            issue ->
            yield(path to issue)
        }
    }
}

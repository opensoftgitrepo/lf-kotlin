package pt.lightweightform.lfkotlin.internal

import pt.lightweightform.lfkotlin.PATH_ID_PLACEHOLDER
import pt.lightweightform.lfkotlin.Path
import pt.lightweightform.lfkotlin.PathException
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.isValidPathIndex
import pt.lightweightform.lfkotlin.schemas.ArraySchema
import pt.lightweightform.lfkotlin.schemas.ClassSchema
import pt.lightweightform.lfkotlin.schemas.TupleSchema

/** Child schema with id [id] of a [parentSchema] with path [parentPath]. */
internal fun childSchema(parentSchema: Schema<*>, parentPath: Path, id: String): Schema<*> {
    if (parentSchema is ArraySchema<*>) {
        // Only integers are valid identifiers for `ListSchema` elements (the
        // placeholder is also valid here)
        if (!isValidPathIndex(id, true)) {
            throw PathException(parentPath, "Invalid list index '$id'.")
        }
        return parentSchema.elementsSchema
    }
    /*if (parentSchema is MapSchema<*>) {
        // A map key may not be `'.'` or `'..'` or contain a forward slash
        if (!isValidPathId(id)) {
            throw PathException(parentPath, "Invalid map key '$id'.")
        }
        return parentSchema.valuesSchema
    }*/
    if (parentSchema is ClassSchema<*>) {
        return parentSchema.childInfoByName[id]?.schema
            ?: throw PathException(parentPath, "Invalid class child '$id'.")
    }
    if (parentSchema is TupleSchema) {
        // Only valid integer indices are valid identifiers
        if (!isValidPathIndex(id) || id.toInt() >= parentSchema.elementsSchemas.size) {
            throw PathException(parentPath, "Invalid tuple element '$id'.")
        }
        return parentSchema.elementsSchemas[id.toInt()]
    }
    // Schema is not "complex", so it has no children schemas
    throw PathException(parentPath, "Schema has no child '$id'.")
}

/** Child with id [id] of a value [parentValue] with schema [parentSchema] and path [parentPath]. */
internal fun childValue(
    parentSchema: Schema<*>,
    parentPath: Path,
    parentValue: Any?,
    id: String
): Any? {
    if (parentValue == null) {
        throw PathException(parentPath, "Cannot access '$id' of null.")
    }
    if (parentSchema is ArraySchema<*>) {
        if (id == PATH_ID_PLACEHOLDER) {
            throw PathException(parentPath, "Value paths may not contain placeholders.")
        }
        parentValue as Array<*>
        val maxId = parentValue.size - 1
        if (id.toInt() > maxId) {
            throw PathException(
                parentPath,
                "Array index '$id' is out of bounds (greater than '$maxId')."
            )
        }
        return parentValue[id.toInt()]
    }
    /*if (parentSchema is MapSchema<*>) {
        parentValue as Map<String, *>
        if (id !in parentValue) {
            throw PathException(parentPath, "Map has no value with key '$id'.");
        }
        return parentValue[id]
    }*/
    if (parentSchema is ClassSchema<*>) {
        @Suppress("UNCHECKED_CAST") (parentSchema as ClassSchema<Any>)
        return parentSchema.childInfoByName[id]?.prop?.get(parentValue)
    }
    if (parentSchema is TupleSchema) {
        parentValue as Array<*>
        return parentValue[id.toInt()]
    }
    throw PathException(parentPath, "Value has no child '$id'.")
}

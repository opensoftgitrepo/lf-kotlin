package pt.lightweightform.lfkotlin.internal

import pt.lightweightform.lfkotlin.Path
import pt.lightweightform.lfkotlin.PathException
import pt.lightweightform.lfkotlin.PathPartInfo
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.appendToPath
import pt.lightweightform.lfkotlin.resolvePathToList

/** Returns a list of information about each part of a path. */
internal fun pathInfoImpl(
    rootSchema: Schema<*>,
    rootValue: Any?,
    path: Path,
    fetchValues: Boolean
): List<PathPartInfo> =
    resolvePathToList("/", path).fold(
        mutableListOf(
            PathPartInfo("/", null, rootSchema, false, if (fetchValues) rootValue else null)
        )
    ) { pathInfo, id ->
        val prev = pathInfo.last()
        val childSchema = childSchema(prev.schema, prev.path, id)
        if (childSchema.isClientOnly == true ||
                (childSchema.isClientOnly == null && childSchema.computedValue != null)
        ) {
            throw PathException(
                prev.path,
                "Child schema with id '$id' is client-only and cannot be accessed on the server side."
            )
        }
        pathInfo +=
            PathPartInfo(
                appendToPath(prev.path, id),
                id,
                childSchema,
                prev.isComputed || childSchema.computedValue != null,
                if (fetchValues) childValue(prev.schema, prev.path, prev.value, id) else null
            )
        pathInfo
    }

/**
 * Returns the last part of the "path information" (the last element of the list returned by
 * [pathInfoImpl]).
 */
internal fun lastPathPartInfo(
    rootSchema: Schema<*>,
    rootValue: Any?,
    path: Path,
    fetchValues: Boolean
): PathPartInfo = pathInfoImpl(rootSchema, rootValue, path, fetchValues).last()

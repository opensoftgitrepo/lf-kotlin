package pt.lightweightform.lfkotlin.internal

import pt.lightweightform.lfkotlin.AllowedValues
import pt.lightweightform.lfkotlin.Bound
import pt.lightweightform.lfkotlin.ComputedValue
import pt.lightweightform.lfkotlin.Context
import pt.lightweightform.lfkotlin.IsRequired
import pt.lightweightform.lfkotlin.Issue
import pt.lightweightform.lfkotlin.Path
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.SyncAllowedValues
import pt.lightweightform.lfkotlin.SyncBound
import pt.lightweightform.lfkotlin.SyncIsRequired
import pt.lightweightform.lfkotlin.SyncValidation
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.resolvePath

/** Runs a computed value. */
internal fun runComputedValue(
    rootSchema: Schema<*>,
    rootValue: Any?,
    state: MutableState,
    path: Path,
    externalContext: Any?,
    computedValue: ComputedValue<*>
): Any? {
    val ctx = Context(rootSchema, rootValue, state, resolvePath(path, ".."), externalContext)
    return computedValue.run { ctx.compute() }
}

/** Runs an "is required" validation. */
internal fun runIsRequired(
    rootSchema: Schema<*>,
    rootValue: Any?,
    state: MutableState,
    path: Path,
    externalContext: Any?,
    isRequired: IsRequired
): Boolean {
    val ctx = Context(rootSchema, rootValue, state, resolvePath(path, ".."), externalContext)
    return when (isRequired) {
        is SyncIsRequired -> isRequired.run { ctx.isRequired() }
        else -> error("Unsupported `IsRequired` implementation")
    }
}

/** Runs an "allowed values" validation. */
internal fun runAllowedValues(
    rootSchema: Schema<*>,
    rootValue: Any?,
    state: MutableState,
    path: Path,
    externalContext: Any?,
    allowedValues: AllowedValues<*>
): List<Any?>? {
    val ctx = Context(rootSchema, rootValue, state, resolvePath(path, ".."), externalContext)
    return when (allowedValues) {
        is SyncAllowedValues<*> ->
            (allowedValues as SyncAllowedValues<Any?>).run { ctx.allowedValues() }
        else -> error("Unsupported `AllowedValues` implementation")
    }
}

/** Runs a "bound" validation. */
internal fun <T> runBound(
    rootSchema: Schema<*>,
    rootValue: Any?,
    state: MutableState,
    path: Path,
    externalContext: Any?,
    bound: Bound<T>
): T? {
    val ctx = Context(rootSchema, rootValue, state, resolvePath(path, ".."), externalContext)
    return when (bound) {
        is SyncBound<*> -> (bound as SyncBound<T>).run { ctx.bound() }
        else -> error("Unsupported `Bound` implementation")
    }
}

/** Runs a "regular" validation. */
internal fun runValidation(
    rootSchema: Schema<*>,
    rootValue: Any?,
    state: MutableState,
    path: Path,
    externalContext: Any?,
    validation: Validation<*>
): Iterable<Issue> {
    val ctx = Context(rootSchema, rootValue, state, path, externalContext)
    @Suppress("UNCHECKED_CAST")
    return when (validation) {
        is SyncValidation<*> ->
            (validation as SyncValidation<Any?>).run { ctx.validate(ctx.get(".")) }
        else -> error("Unsupported `Validation` implementation")
    }
}

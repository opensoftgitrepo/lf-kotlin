package pt.lightweightform.lfkotlin.internal

import pt.lightweightform.lfkotlin.Issue
import pt.lightweightform.lfkotlin.PATH_ID_PLACEHOLDER
import pt.lightweightform.lfkotlin.Path
import pt.lightweightform.lfkotlin.Schema
import pt.lightweightform.lfkotlin.Validation
import pt.lightweightform.lfkotlin.appendToPath
import pt.lightweightform.lfkotlin.resolvePathToList
import pt.lightweightform.lfkotlin.schemas.ArraySchema
import pt.lightweightform.lfkotlin.schemas.ClassSchema
import pt.lightweightform.lfkotlin.schemas.TupleSchema

/** Runs an external [validation] with a path [validationPath] (which may contain placeholders). */
internal fun runExternalValidation(
    rootSchema: Schema<*>,
    rootValue: Any?,
    state: MutableState,
    validationPath: Path,
    validation: Validation<*>,
    externalContext: Any?
): Sequence<Pair<Path, Issue>> = sequence {
    for (path in
        allPathsMatchingValidationPath(
            rootSchema,
            rootValue,
            resolvePathToList("/", validationPath)
        )) {
        runValidation(rootSchema, rootValue, state, path, externalContext, validation).forEach {
            issue ->
            yield(path to issue)
        }
    }
}

/**
 * Returns all paths matching the validation path in list form [validationPathList] (which may
 * contain placeholders).
 */
private fun allPathsMatchingValidationPath(
    schema: Schema<*>,
    value: Any?,
    validationPathList: List<String>,
    curPath: String = "/",
    curIndex: Int = -1
): Sequence<Path> = sequence {
    if (curIndex == validationPathList.size - 1) {
        yield(curPath)
        return@sequence
    }

    if (value == null) {
        return@sequence
    }

    val nextIndex = curIndex + 1
    val nextId = validationPathList[nextIndex]

    if (nextId != PATH_ID_PLACEHOLDER) {
        yieldAll(
            allPathsMatchingValidationPath(
                childSchema(schema, curPath, nextId),
                childValue(schema, curPath, value, nextId),
                validationPathList,
                appendToPath(curPath, nextId),
                nextIndex
            )
        )
    } else {
        when (schema) {
            is ArraySchema<*> -> {
                for ((i, el) in (value as Array<*>).withIndex()) {
                    yieldAll(
                        allPathsMatchingValidationPath(
                            schema.elementsSchema,
                            el,
                            validationPathList,
                            appendToPath(curPath, i.toString()),
                            nextIndex
                        )
                    )
                }
            }
            is ClassSchema<*> -> {
                @Suppress("UNCHECKED_CAST")
                for ((name, info) in (schema as ClassSchema<Any>).childInfoByName) {
                    yieldAll(
                        allPathsMatchingValidationPath(
                            info.schema,
                            info.prop.get(value),
                            validationPathList,
                            appendToPath(curPath, name),
                            nextIndex
                        )
                    )
                }
            }
            is TupleSchema -> {
                for ((i, childSchema) in schema.elementsSchemas.withIndex()) {
                    yieldAll(
                        allPathsMatchingValidationPath(
                            childSchema,
                            (value as Array<*>)[i],
                            validationPathList,
                            appendToPath(curPath, i.toString()),
                            nextIndex
                        )
                    )
                }
            }
        }
    }
}

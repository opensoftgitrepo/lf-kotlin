package pt.lightweightform.lfkotlin.internal

import pt.lightweightform.lfkotlin.Issue
import pt.lightweightform.lfkotlin.Path

/** Issues map allowing mutability. */
internal typealias MutableIssuesMap = MutableMap<Path, MutableList<Issue>>

/** Adds a issue to an issues map. */
internal fun MutableIssuesMap.addIssue(path: Path, issue: Issue) =
    this.getOrPut(path) { mutableListOf() }.add(issue)

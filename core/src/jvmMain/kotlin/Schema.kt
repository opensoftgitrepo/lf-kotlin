package pt.lightweightform.lfkotlin

public actual interface Schema<T> {
    public val isNullable: Boolean
    public val initialValue: T?
    public val computedInitialValue: InitialValue<T?>?
    public val computedValue: ComputedValue<T?>?
    public val mismatchedComputedCode: String?
    public val isClientOnly: Boolean?
    public val isRequired: Boolean?
    public val computedIsRequired: IsRequired?
    public val isRequiredCode: String?
    public val allowedValues: List<T>?
    public val computedAllowedValues: AllowedValues<T>?
    public val disallowedValueCode: String?
    public val validations: List<Validation<T>>?
    public val initialState: Map<String, Any?>?
    public val extra: Map<String, Any?>?

    /** Whether two values of this schema are equal. */
    public fun valuesAreEqual(value1: T?, value2: T?): Boolean
}

pluginManagement {
    plugins {
        kotlin("multiplatform") version "${extra["kotlinVersion"]}"
        id("org.springframework.boot") version "${extra["springframeworkBootVersion"]}"
        id("io.spring.dependency-management") version
            "${extra["springDependencyManagementVersion"]}"
        id("com.github.node-gradle.node") version "${extra["nodeGradleVersion"]}"
        id("com.diffplug.spotless") version "${extra["spotlessVersion"]}"
        id("org.jetbrains.dokka") version "${extra["dokkaVersion"]}"
        id("io.github.gradle-nexus.publish-plugin") version "${extra["nexusPublishPluginVersion"]}"
        id("signing")
        id("maven-publish")
    }

    repositories {
        gradlePluginPortal()
        jcenter()
    }
}

rootProject.name = "LF-Kotlin"

include(
    // Core
    "core",
    // Demo
    "demo:common",
    "demo:server",
    "demo:client"
)

project(":core").name = "lf-kotlin"
